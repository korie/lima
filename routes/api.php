<?php

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */


Route::get('outbox', 'SMS\ProcessSMS@getOutbox');
Route::get('inbox', 'SMS\ProcessSMS@getInbox');

Route::post('sender', 'API\SenderController@sender');
Route::get('inbox', 'SMS\ProcessSMS@getInbox');
Route::get('re', 'SMS\ProcessSMS@me');
Route::get('send_products', 'SMS\ProcessSMS@send_products');
Route::get('send_orders', 'SMS\ProcessSMS@send_orders');
Route::get('counties', 'CountyController@index');

Route::post('sender', 'SenderController@sender');

Route::post('register_corp', 'RegisterCorpController@register');
Route::post('app_login', 'RegisterCorpController@login');

Route::post('my_farmers', 'FarmerController@my_farmers');
Route::post('unattached_farmers', 'FarmerController@unattached_farmers');
Route::post('farmer', 'FarmerController@getFarmer');

Route::post('attach_farmer', 'FarmerController@attach_farmer');
Route::post('unattach_farmer', 'FarmerController@unattach_farmer');

Route::post('register_farmer', 'RegisterCorpController@register_farmer');

Route::post('corp_orders', 'OrderController@corp_orders');
Route::post('corp_orders_status', 'OrderController@corp_order_status');

Route::post('vet_request', 'VetRequestController@vet_request');
Route::post('vet_status_update', 'VetRequestController@vet_status_update');

Route::post('transfer', 'TransferController@transfer');
Route::post('farmer_transfer', 'TransferController@farmer_transfer');

Route::post('surveys', 'OrderController@surveys');
Route::get('information_center', 'OrderController@information_center');

Route::post('reset_password', 'RegisterCorpController@password_reset');

//ussd endpoint
Route::post('ussd', ['uses' => 'Ussd@ussd']);


