<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are aded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('auth.login');
    return view('auth.log');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'UserController@users');
Route::post('/adduser', ['uses'=>'UserController@adduser', 'as'=>'adduser']);
Route::post('/adduserlevel', ['uses'=>'UserController@adduserlevel', 'as'=>'adduserlevel']);
Route::post('/edituser', ['uses'=>'UserController@edituser', 'as'=>'edituser']);
Route::post('/deleteuser', ['uses'=>'UserController@deleteuser', 'as'=>'deleteuser']);
Route::post('/get_subcounties', ['uses'=>'UserController@get_subcounties', 'as'=>'get_subcounties']);

Route::get('/sa_person', 'SA_PERSONS_CONTROLLER@SA_persons');
Route::post('/add_sa_person', ['uses'=>'SA_PERSONS_CONTROLLER@add_sa_person', 'as' => 'add_sa_person']);

Route::get('/corp', 'CorpController@Corp');
Route::post('/addcorp', ['uses'=>'CorpController@addcorp', 'as' => 'addcorp']);
Route::post('/editcorp', ['uses'=>'CorpController@editcorp', 'as'=>'editcorp']);
Route::post('/deletecorp', ['uses'=>'CorpController@deletecorp', 'as'=>'deletecorp']);

Route::get('/healthofficer', 'HealthOfficerController@healthofficer');
Route::post('/addhealthofficer', ['uses'=>'HealthOfficerController@addhealthofficer', 'as' => 'addhealthofficer']);
Route::post('/edithealthofficer', ['uses'=>'HealthOfficerController@edithealthofficer', 'as'=>'edithealthofficer']);
Route::post('/deletehealthofficer', ['uses'=>'HealthOfficerController@deletehealthofficer', 'as'=>'deletehealthofficer']);

Route::get('/supplier', 'SupplierController@supplier');
Route::post('/addsupplieragent', ['uses'=>'SupplierController@addsupplieragent', 'as' => 'addsupplieragent']);
Route::post('/addsupplier', ['uses'=>'SupplierController@addsupplier', 'as' => 'addsupplier']);
Route::post('/editsupplier', ['uses'=>'SupplierController@editsupplier', 'as'=>'editsupplier']);
Route::post('/deletesupplier', ['uses'=>'SupplierController@deletesupplier', 'as'=>'deletesupplier']);

Route::get('/myproducts', ['uses' => 'ProductsController@products', 'as' => 'myproducts']);
Route::get('/products', ['uses' => 'ProductsController@getproducts', 'as' => 'products']);
Route::post('/addproducts', ['uses'=>'ProductsController@addproducts', 'as'=>'addproducts']);
Route::post('/addproductssupplier', ['uses'=>'ProductsController@add_products_supplier', 'as'=>'addproductssupplier']);
Route::post('/editproducts', ['uses'=>'ProductsController@editproducts', 'as'=>'editproducts']);
Route::post('/deleteproducts', ['uses'=>'ProductsController@deleteproducts', 'as'=>'deleteproducts']);
Route::get('/getproducts', ['uses' => 'ProductsController@getproducts', 'as' => 'getproducts']);

Route::get('/informationcenter', ['uses' => 'InformationCenterController@informationcenter', 'as' => 'informationcenter']);
// Route::get('/products', ['uses' => 'InformationCenterController@getproducts', 'as' => 'products']);
Route::post('/addinfor', ['uses'=>'InformationCenterController@addinfor', 'as'=>'addinfor']);

Route::get('/broadcast', ['uses' => 'MessagingController@broadcast', 'as' => 'broadcast']);
Route::post('/addbroadcast', ['uses'=>'MessagingController@addbroadcast', 'as'=>'addbroadcast']);
Route::post('/editbroadcast', ['uses'=>'MessagingController@editbroadcast', 'as'=>'editbroadcast']);
Route::post('/deletebroadcast', ['uses'=>'MessagingController@deletebroadcast', 'as'=>'deletebroadcast']);

//stock routes
Route::get('/stocks', 'StockController@index');
Route::post('/addstock', ['uses'=>'StockController@addstock', 'as'=>'addstock']);
Route::post('/editstock', ['uses'=>'StockController@editstock', 'as'=>'editstock']);
Route::get('/stockbalance', ['uses'=>'StockController@stockbalance', 'as'=>'stockbalance']);
Route::get('/stockmovement', ['uses'=>'StockController@movement', 'as'=>'stock-movement']);

//orders routes
Route::get('/adminorders', 'Admin_ordersController@admin_orders');
Route::get('/orders', 'OrderController@index');
Route::get('/orderssuppliers', 'OrderController@orderssuppliers');
Route::post('/addorder', ['uses'=>'OrderController@addstock', 'as'=>'addorder']);
Route::post('/editorder', ['uses'=>'OrderController@editorder', 'as'=>'editorder']);
//Route::get('vieworderdetails', 'OrderController@vieworderdetails');
Route::get('/vieworderdetails/{id}', 'OrderController@vieworderdetails');
//Route::post('/vieworderdetails', 'OrderController@vieworderdetails');
Route::post('/updateorder', ['uses'=>'OrderController@updateorder', 'as'=>'updateorder']);
Route::post('/addsaorder', ['uses'=>'OrderController@addsaorder', 'as'=>'addsaorder']);
Route::post('/disapproveorder', ['uses'=>'OrderController@disapproveorder', 'as'=>'disapproveorder']);


Route::get('/report', 'ReportController@report');
Route::get('/farmers', 'FarmerController@farmers');

Route::get('/agent', ['uses' => 'AgentController@agent', 'as' => 'agent']);
Route::get('/addagent', ['uses'=>'AgentController@addagent', 'as'=>'addagent']);
Route::post('/editagent', ['uses'=>'AgentController@editagent', 'as'=>'editagent']);
Route::post('/deleteagent', ['uses'=>'AgentController@deleteagent', 'as'=>'deleteagent']);
Route::get('/corps', 'CorpController@corp');

Route::get('/process_inbox', 'InboxController@process');

// Route::get('/ordertest', 'InboxController@order');