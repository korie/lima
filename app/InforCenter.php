<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InforCenter extends Model
{
    //
    public $table = 'information_center';
    protected $primaryKey = 'infor_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'infor_name', 'infor_link', 'validity', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function client() {
        return $this->belongsTo('App\Person','source','person_id');
    }
}
