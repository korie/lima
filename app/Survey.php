<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    //
    public $table = 'surveys';
    protected $primaryKey = 'survey_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_name', 'survey_link', 'validity', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function client() {
        return $this->belongsTo('App\Person','source','person_id');
    }
    
}
