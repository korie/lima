<?php
	
	namespace App;
	
	use Illuminate\Database\Eloquent\Model;
	
	class FarmerCorp extends Model
	{
		public $table = 'farmer_corp';
		protected $primaryKey = 'farmer_corp_id';
		public $timestamps = false;
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'farmer_id', 'corp_id', 'vet_id',
		];
		
		public function farmer()
		{
			return $this->belongsTo('App\Person', 'farmer_id', 'person_id');
		}
		
		public function corp()
		{
			return $this->belongsTo('App\Person', 'corp_id', 'person_id');
		}
		
		public function vet()
		{
			return $this->belongsTo('App\Person', 'vet_id', 'person_id');
		}
		
		public function farmer_details()
		{
			return $this->belongsTo('App\Farmer', 'farmer_id', 'person_id');
		}
		
		public function person()
		{
			return $this->belongsTo(Person::class, 'farmer_id');
		}
		
		public function apiFarmer()
		{
			return $this->belongsTo(Farmer::class, 'farmer_id', 'person_id');
		}
		
		
	}
