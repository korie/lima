<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierProduct extends Model
{
    public $table = 'supplier_products';
    protected $primaryKey = 'supplier_product_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_product_id', 'supplier_id', 'product_id',
    ];


    public function product(){
        return $this->belongsTo('App\Products','product_id','product_id');
    }

}
