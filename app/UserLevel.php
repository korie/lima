<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    public $table = 'user_level';
    protected $primarykey = 'user_level_id';


    protected $fillable = [
        'user_level_name', 'created_at', 'updated_at', 
    ];
}
