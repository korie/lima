<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorpVetTransfer extends Model
{
    public $table = 'corp_vet_transfer';
    protected $primaryKey = 'transfer_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'person_id', 'registered_farmers', 'subcounty_from', 'subcounty_to', 'transfer_reason', 'transfer_date', 
    ];
    
    // public function corp_vet_transfer(){
    //     return $this->belongsTo('App\Person','person_id','person_id');
    // }
    // public function subcounty(){
    //     return $this->belongsTo('App\Subcounty','subcounty_from','subcounty_id');
    // }
    
}
