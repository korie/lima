<?php
	
	namespace App;
	
	use Illuminate\Database\Eloquent\Model;
	
	class Corp extends Model
	{
		public $table = 'corps';
		protected $primaryKey = 'corp_id';
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'corp_id', 'person_id', 'is_vet', 'vet_reg_number', 'create_at', 'created_by', 'updated_at', 'updated_by',
		];
		
		protected function getCreatedAtAttribute($value)
		{
			return $value;
		}
		
		public function person()
		{
			return $this->belongsTo('App\Person', 'person_id', 'person_id');
		}
		
	}
