<?php
	
	namespace App;
	
	use Illuminate\Database\Eloquent\Model;
	
	class Order extends Model
	{
		public $table = 'orders';
		protected $primaryKey = 'order_id';
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'order_id', 'source', 'order_to', 'status', 'is_deleted', 'created_at', 'created_by', 'updated_at', 'updated_by',
		];
		
		protected function getCreatedAtAttribute($value)
		{
			return $value;
		}
		
		public function person()
		{
			return $this->belongsTo('App\Person', 'source', 'person_id');
		}
		
		
		public function client()
		{
			return $this->belongsTo('App\Person', 'source', 'person_id');
		}
		
		public function seller()
		{
			return $this->belongsTo('App\Person', 'order_to', 'person_id');
		}
		
		public function detail()
		{
			return $this->belongsTo('App\OrderDetail', 'order_id', 'order_id');
		}
		
	}
