<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Broadcast extends Model
{
    //
    public $table = 'broadcasts';
    protected $primaryKey = 'broadcast_id';
    public $timestamps = false;


    protected $fillable = [
        'broadcast_name', 'broadcast_content', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function creator(){
        return $this->belongsTo('App\Person','created_by','person_id');
    }
}
