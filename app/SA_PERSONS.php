<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SA_PERSONS extends Model
{
    public $table = 'sa_persons';
    protected $primaryKey = 'sa_person_id';
    //use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sa_person_id', 'person_id', 'sa_id', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [
        //'password', 'remember_token',
   
   public function person(){
       return $this->belongsTo('App\Person', 'person_id', 'person_id');
   }
   public function creator(){
    return $this->belongsTo('App\Person','created_by','person_id');
    
}
  public  function sas(){
      return $this->belongsTo('App\SAS', 'sa_id', 'sa_id' );
  }
}  

