<?php
	
	namespace App;
	
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Illuminate\Notifications\Notifiable;
	
	class User extends Authenticatable
	{
		public $table = 'users';
		protected $primaryKey = 'user_id';
		use Notifiable;
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'person_id', 'email', 'password', 'user_level', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by', 'remember_token',
		];
		
		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
				'password', 'remember_token',
		];
		
		public function person()
		{
			return $this->belongsTo('App\Person', 'person_id', 'person_id');
		}
		
		
		public function creator()
		{
			return $this->belongsTo('App\Person', 'created_by', 'person_id');
		}
		
		
		public function level()
		{
			return $this->belongsTo('App\UserLevel', 'user_level', 'user_level_id');
		}
	}
