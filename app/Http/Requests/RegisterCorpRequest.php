<?php
	
	namespace App\Http\Requests;
	
	use Illuminate\Foundation\Http\FormRequest;
	
	class RegisterCorpRequest extends FormRequest
	{
		/**
		 * Determine if the user is authorized to make this request.
		 *
		 * @return bool
		 */
		public function authorize()
		{
			return true;
		}
		
		/**
		 * Get the validation rules that apply to the request.
		 *
		 * @return array
		 */
		public function rules()
		{
			$nameRegex = 'regex:/^[a-zA-Z]*$/';
			return [
					'first_name' => ['required', 'string', 'max:50', $nameRegex],
					'last_name' => ['required', 'string', 'max:50', $nameRegex],
					'id_number' => 'required|string|unique:persons,id_number',
					'phone_no' => ['required', 'string', 'regex:/^(\+254|254|0)[1-9]\d{8}$/', 'unique:persons,phone_no'],
					'is_vet' => 'required|in:No,Both,Yes',
					'email' => 'required|string|email|max:255|unique:users,email',
					'password' => 'required|string|min:4',
					'sub_county_id' => 'required|exists:sub_county,id'
			];
		}
		
		public function messages()
		{
			return [
					'first_name.required' => 'Your first name is required',
					'first_name.max' => 'Your first name can only be so long. 190 characters maximum',
					'last_name.required' => 'Your last name is required',
					'last_name.max' => 'Your last name can only be so long. 190 characters maximum',
					'id_number.required' => 'Your ID Number is required',
					'id_number.int' => 'ID Number can only be a number',
					'id_number.max' => 'Your ID Number is too long',
					'id_number.unique' => 'Another account exists with the same ID number',
					
					'phone_no.required' => 'Your mobile number is required',
					'phone_no.regex' => 'Use a valid Kenyan mobile number',
					'phone_no.unique' => 'Another account exists with the same phone number',
					
					'is_vet.required' => 'Marital status is required',
					'is_vet.in' => 'Is Vet can only be Yes, No, Both',
					
					'email.required' => 'Your email address is required',
					'email.email' => 'A valid email is required',
					'email.max' => 'Your email can only be so long. 190 characters maximum',
					'email.unique' => 'Another account exists with the same email',
					
					'password.required' => 'Your account password is required',
					'password.min' => 'Your password is too short. Use minimum 4 characters',
					'password.max' => 'Your password is too short. Use minimum 4 characters',
					
					'sub_county_id.required' => 'Sub County is required',
					'exists.integer' => 'Invalid Sub county',
			
			];
		}
	}
