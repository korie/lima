<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransferCorpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    return [
			    'sub_county_id' => ['required', 'string'],
			    'person_id' => ['required', 'string'],
			    'reason' => ['required', 'string'],
	    ];
    }
    
    public function messages()
    {
	    return [
			    'sub_county_id.required' => "Sub county id is required",
			    'person_id.required' => "Person id is required",
			    'reason.required' => "Reason for transfer is required",
	    ];
    }
}
