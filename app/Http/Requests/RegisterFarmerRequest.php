<?php
	
	namespace App\Http\Requests;
	
	use Illuminate\Foundation\Http\FormRequest;
	
	class RegisterFarmerRequest extends FormRequest
	{
		/**
		 * Determine if the user is authorized to make this request.
		 *
		 * @return bool
		 */
		public function authorize()
		{
			return true;
		}
		
		/**
		 * Get the validation rules that apply to the request.
		 *
		 * @return array
		 */
		public function rules()
		{
			$nameRegex = 'regex:/^[a-zA-Z]*$/';
			return [
					'first_name' => ['required', 'string', 'max:50', $nameRegex],
					'last_name' => ['required', 'string', 'max:50', $nameRegex],
					'id_number' => 'required|string|unique:persons,id_number',
					'phone_no' => ['required', 'string', 'regex:/^(\+254|254|0)[1-9]\d{8}$/', 'unique:persons,phone_no'],
					'location' => 'required|string|max:255',
					'no_of_livestock' => 'nullable|integer',
					'sub_county_id' => 'required|exists:sub_county,id',
					'person_id' => 'required|string',
			];
		}
		
		public function messages()
		{
			return [
					'first_name.required' => 'Farmer\'s first name is required',
					'first_name.max' => 'Farmer\'s first name can only be so long. 50 characters maximum',
					'last_name.required' => 'Farmer\'s last name is required',
					'last_name.max' => 'Farmer\'s last name can only be so long. 50 characters maximum',
					
					'id_number.required' => 'Farmer\'s ID number is required',
					'id_number.int' => 'ID number can only be a number',
					'id_number.max' => 'Farmer\'s ID number is too long',
					'id_number.unique' => 'Another farmer exists with the same Id',
					
					'phone_no.required' => 'Farmer\'s mobile number is required',
					'phone_no.regex' => 'Use a valid Kenyan mobile number',
					'phone_no.unique' => 'Another account exists with the same phone number',
					
					'sub_county_id.required' => 'Sub County is required',
					'sub_county_id.exists' => 'Invalid Sub county',
					
					'location.required' => 'Farmer location is required',
					'location.max' => 'Farmer\'s location can only be so long. 255 characters maximum',
					
					'person_id.required' => 'Person Id is required',
			
			];
		}
	}
