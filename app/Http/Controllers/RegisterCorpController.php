<?php

namespace App\Http\Controllers;

use App\Corp;
use App\Farmer;
use App\FarmerCorp;
use App\Http\Controllers\SMS\ProcessSMS;
use App\Http\Requests\RegisterCorpRequest;
use App\Http\Requests\RegisterFarmerRequest;
use App\Person;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterCorpController extends Controller
{

    public function register(RegisterCorpRequest $request)
    {
        $fname = request('first_name');
        $lname = request('last_name');
        $id = request('id_number');
        $phone = trimSpace(request('phone_no'));
        $vet_number = request('vet_reg_number');
        $vet = request('is_vet');
        $email = request('email');
        $pwd = request('password');
        $sc = request('sub_county_id');


        $date = date('Y-m-d H:i:s', time());
        $person = new Person;
        $person->first_name = $fname;
        $person->last_name = $lname;
        $person->id_number = $id;
        $person->phone_no = $phone;
        $person->sub_county_id = $sc;
        $person->created_by = 1;
        $person->updated_by = 1;
        $person->save();

        $corp = new Corp();
        $corp->person_id = $person->person_id;
        $corp->is_vet = $vet;
        $corp->vet_reg_number = $vet_number;
        $corp->created_by = 1;
        $corp->updated_by = 1;

        $user = new User();
        $user->person_id = $person->person_id;
        $user->email = $email;
        $user->password = bcrypt($pwd);
        $user->user_level = 3;
        $user->status = 'Active';
        $user->created_by = 1;
        $user->updated_by = 1;

        $person->corpAccount()->save($corp);
        $person->userAccount()->save($user);

        $myarr = array();
        $myarr["person_id"] = $corp->person_id;
        $myarr["first_name"] = $person->first_name;
        $myarr["last_name"] = $person->last_name;
        $myarr["id_number"] = $person->id_number;
        $myarr["phone_no"] = $person->phone_no;
        $myarr["vet_reg_number"] = $corp->vet_reg_number;
        $myarr["sub_county_id"] = $person->sub_county_id;
        $myarr["is_vet"] = $corp->is_vet;
        $myarr["email"] = $user->email;
        $myarr["user_level"] = $user->user_level;
        $myarr["status"] = $user->status;
        return response()->json($myarr);
    }

    public function register_farmer(RegisterFarmerRequest $request)
    {
        $params['person_id'] = request('person_id');
        $params['first_name'] = request('first_name');
        $params['last_name'] = request('last_name');
        $params['id_number'] = request('id_number');
        $params['phone_no'] = request('phone_no');
        $params['sub_county_id'] = request('sub_county_id');
        $params['location'] = request('location');
        $params['no_of_livestock'] = request('no_of_livestock');

        return $this->reg_farmer($params);
    }

    public function reg_farmer($params)
    {
        $fname = $params['first_name'];
        $lname = $params['last_name'];
        $id = $params['id_number'];
        $phone = trimSpace($params['phone_no']);
        $sc = $params['sub_county_id'];
        $lc = $params['location'];
        $nol = $params['no_of_livestock'];
        $pid = $params['person_id'];

        $corp = Corp::where('person_id', $pid)->first();

        try {

            if ($corp == null) {
                return response()->json(['messages' => ['You are not authorised to add farmers']], 401);
            } else {


                $farmer = Person::where('id_number', $id)->orWhere('phone_no', $phone)->first();

                if ($farmer == null) {
                    $date = date('Y-m-d H:i:s', time());

                    $person = new Person();
                    $person->first_name = $fname;
                    $person->last_name = $lname;
                    $person->id_number = $id;
                    $person->phone_no = $phone;
                    $person->sub_county_id = $sc;
                    $person->created_by = $pid;
                    $person->updated_by = $pid;
                    $person->updated_at = $date;

                    if ($person->save()) {

                        $farm = new Farmer;

                        $farm->person_id = $person->person_id;
                        $farm->location = $lc;
                        $farm->no_of_livestock = $nol;
                        $farm->created_by = $pid;
                        $farm->updated_by = $pid;
                        $farm->updated_at = $date;

                        if ($farm->save()) {

                            $farmercorp = new FarmerCorp;
                            $farmercorp->farmer_id = $person->person_id;

                            if ($corp->is_vet == 'No') {

                                $farmercorp->corp_id = $corp->person_id;
                            } else if ($corp->is_vet == 'Yes') {
                                $farmercorp->vet_id = $corp->person_id;
                            } else if ($corp->is_vet == 'Both') {

                                $farmercorp->corp_id = $corp->person_id;
                                $farmercorp->vet_id = $corp->person_id;
                            }
                            $farmercorp->save();
                        }
                        return response()->json(['messages' => ['Farmer Added Successfully']]);
                    }

                } else {

                    if ($farmer->status == 'Active') {

                        $fid = $farmer->person_id;

                        $fc = FarmerCorp::where('farmer_id', $fid)->first();

                        if ($fc == null) {

                            $farmercorp = new FarmerCorp;
                            $farmercorp->farmer_id = $fid;

                            if ($corp->is_vet == 'No') {

                                $farmercorp->corp_id = $corp->person_id;
                                $farmercorp->vet_id = '';

                            } else if ($corp->is_vet == 'Yes') {
                                $farmercorp->vet_id = $corp->person_id;

                            } else if ($corp->is_vet == 'Both') {

                                $farmercorp->corp_id = $corp->person_id;
                                $farmercorp->vet_id = $corp->person_id;

                            }

                            $farmercorp->save();


                        } else {

                            if ($corp->is_vet == 'No') {

                                if ($fc->corp_id == "") {
                                    $fc->corp_id = $corp->person_id;
                                } else {
                                    return response()->json(['messages' =>
                                        ['This farmer is already attached to a CORP']]);
                                }
                            } else if ($corp->is_vet == 'Yes') {

                                if ($fc->vet_id == "") {
                                    $fc->vet_id = $corp->person_id;
                                } else {
                                    return response()->json(['messages' =>
                                        ['This farmer is already attached to a VET']]);
                                }

                            } else if ($corp->is_vet == 'Both') {

                                if ($fc->corp_id == "" && $fc->vet_id == "") {
                                    $fc->corp_id = $corp->person_id;
                                    $fc->vet_id = $corp->person_id;
                                } else if ($fc->corp_id == "" && $fc->vet_id != "") {
                                    $fc->corp_id = $corp->person_id;

                                    $fc->save();

                                    return response()->json(['messages' =>
                                        ['This farmer is already attached to a VET, you have been attached to him as a CORP only']]);
                                } else if ($fc->corp_id != "" && $fc->vet_id == "") {
                                    $fc->vet_id = $corp->person_id;
                                    $fc->save();

                                    //return response("This farmer is already attached to a CORP, you have been attached to him as a VET only");
                                    return response()->json(['messages' =>
                                        ['This farmer is already attached to a CORP, you have been attached to him as a VET only']]);

                                } else if ($fc->corp_id != "" && $fc->vet_id != "") {
                                    return response()->json(['messages' =>
                                        ['Sorry. This farmer is already attached to a VET and a CORP']]);
                                }
                            }
                            $fc->save();
                        }
                        return response()->json(['messages' => ['Farmer Added Successfully']]);
                    } else {
                        return response()->json(['messages' => ['This Farmer is not active in the system']], 500);
                    }
                }
            }

        } catch (Exception $e) {

            return response()->json(['messages' => [$e->getMessage()]], 500);

        }
    }

    public function login(Request $request)
    {

        $identity = $request->get('email');

        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $column = 'email';
        } else {
            $column = 'phone_no';
        }

        if (Auth::attempt([$column => $identity, 'password' => request('password')])) {
            $user = auth()->user();
            $corp = Corp::where('person_id', $user->person_id)->first();
            $myarr = array();
            $myarr["person_id"] = $user->person_id;
            $myarr["first_name"] = $user->person->first_name;
            $myarr["last_name"] = $user->person->last_name;
            $myarr["phone_no"] = $user->person->phone_no;
            $myarr["id_number"] = $user->person->id_number;
            $myarr["sub_county_id"] = $user->person->sub_county_id;
            $myarr["is_vet"] = $corp->is_vet;
            $myarr["user_level"] = $user->user_level;
            $myarr["status"] = $user->status;
            $myarr["email"] = $user->email;
            return response()->json($myarr);
        } else {
            return response()->json(['messages' => ['Failed to log in with those credentials']], 401);
        }
    }

    public function password_reset(Request $request)
    {

        $corp = Person::where('phone_no', trimSpace($request->input('phone_no')))->first();

        if ($corp) {

            $pwd = generateRandomChars(6);

            $newpassword = bcrypt($pwd);
            $phone = $corp->phone_no;
            $msg = "Dear " . $corp->first_name . ", your Lima password reset request was successful.Your new password is " . $pwd . " .Login and change this password";

            $update_user = User::where('person_id', $corp->person_id)->update(['password' => $newpassword]);

            if ($update_user) {
                //send sms with the new password as the response
                $sms_process = new ProcessSMS();
                $send_msg = $sms_process->getOutbox($phone, $msg);

                // echo json_encode($send_msg);

                return response()->json(['messages' => ['You will receive SMS reply with password']]);
            }

        } else {
            return response()->json(['messages' => ['That mobile number does not exist in the system']], 401);
        }
    }
}