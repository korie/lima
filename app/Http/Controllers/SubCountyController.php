<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SubCounty;

class SubCountyController extends Controller
{
    public function index()
    {

        $result = [];
        $subcounties = SubCounty::all();
        $result["subCounties"] = $subcounties;

        return $result;
       
    }
}
