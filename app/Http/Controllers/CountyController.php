<?php

namespace App\Http\Controllers;

use App\County;

class CountyController extends Controller
{
    public function index()
    {
        $counties = County::with('subCounties')->get();
        return $counties;
    }

   
}
