<?php

namespace App\Http\Controllers;

//use App\County;
use App\Http\Controllers\Controller;
use App\Person;
use App\Products;
use App\Stock;
use App\User;
use App\SupplierProduct;
//use App\UserLevel;
use Auth;
use Illuminate\Http\Request;
use App\StockMovement;

class StockController extends Controller
{

    public function index()
    {


        $stocks = Stock::where('person_id', Auth::user()->person_id)->get();
        $products = SupplierProduct::with('product')->where('supplier_id', Auth::user()->person_id)->get();

        $data = array(
            'products' => $products,
            'stocks' => $stocks
        );
        return view('stock')->with($data);

    }
    public function stockbalance()
    {
        
        $products = Products::where('person_id',Auth::user()->person_id)->get();
        
 

        $data = array(
            
            'products' => $products
        );
        return view('stock_balance')->with($data);

    }
    public function addstock(Request $request)
    {

        try {

            if ($request->stock_code == '') {
                return response(['status' => 'error', 'details' => "Please enter the Stock Code"]);
            }
            if ($request->quantity == '') {
                return response(['status' => 'error', 'details' => "Please enter Quantity"]);
            } else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());
                $user = User::find(Auth::user()->user_id);
             
                 $product = Products::find($request->input('product_id'));
                 $stock_balance = $product->balance;
                //add the stock
                $stock = new Stock;
                $stock->stock_code = $request->input('stock_code');
                $stock->stock_in = $request->input('quantity');
                $stock->stock_out = 0;
                $stock->product_id = $request->input('product_id');
                $stock->person_id = $user->person->person_id;
                $stock->created_by = $user->person->person_id;
                $stock->created_at = $date;
                $stock->updated_by = $user->person->person_id;
                $stock->updated_at = $date;
                $stock->save();
            

                $stocks = Stock::where('person_id', Auth::user()->person_id)->get();
                $products = SupplierProduct::with('product')->where('supplier_id', Auth::user()->person_id)->get();
   

                $data = array(
                    'products' => $products,
                    'stocks' => $stocks
                );
                return response(['status' => 'success', 'details' => $data]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }


    public function movement(){

        $movements = StockMovement::with('receiver')->where('seller_id', Auth::user()->person_id)->get();

        return view('stock_movement')->with('movements', $movements);


    }

    public function edituser(Request $request)
    {
        try {

            if ($request->first_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the First name"]);
            } else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $user = User::find($request->input('id'));
                $user->email = $request->input('email');
                $user->user_level = $request->input('user_level_id');
                $user->status = $request->input('status');
                $user->updated_at = $date;
                $user->updated_by = Auth::user()->user_id;
                $user->save();

                $pid = $user->person_id;

                $pno = $request->input('phone_no');
                $str = substr($pno, 1);
                $fullno = "+254" . $str;

                $person = Person::find($pid);
                $person->first_name = $request->input('first_name');
                $person->last_name = $request->input('last_name');
                $person->id_number = $request->input('id_num');
                $person->phone_no = $fullno;
                $person->sub_county_id = $request->input('sub_county_id');
                $person->updated_at = $date;
                $person->updated_by = Auth::user()->user_id;
                $person->save();

                $counties = County::all();
                $subcounties = SubCounty::all();
                $userlevels = UserLevel::all();

                $users = User::with('person.sub_county.county')->with('creator')->with('level')->get();

                $data = array(
                    'users' => $users,
                    'counties' => $counties,
                    'subcounties' => $subcounties,
                    'userlevels' => $userlevels,
                );
                return response(['status' => 'success', 'details' => $data]);

            }
        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function deleteuser(Request $request)
    {
        try {
            date_default_timezone_set('UTC');
            $date = date('Y-m-d H:i:s', time());

            $user = User::find($request->input('user_id'));
            $user->status = 'Deleted';
            $user->updated_at = $date;
            $user->updated_by = Auth::user()->user_id;
            $user->save();
            $counties = County::all();
            $subcounties = SubCounty::all();
            $userlevels = UserLevel::all();

            $users = User::with('person.sub_county.county')->with('creator')->with('level')->get();

            $data = array(
                'users' => $users,
                'counties' => $counties,
                'subcounties' => $subcounties,
                'userlevels' => $userlevels,
            );
            return response(['status' => 'success', 'details' => $data]);

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }
    }

    public function get_subcounties(Request $request)
    {

        $county_id = $request->county_id;

        $subcounties = SubCounty::where('county_id', $county_id)->get();

        return $subcounties;

    }

}
