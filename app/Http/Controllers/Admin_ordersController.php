<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Products;
use App\Order;
use App\OrderDetail;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Admin_ordersController extends Controller
{
    //
    public function admin_orders()
    {

        $orderdetails = OrderDetail::with('order.client')->with('order.seller')->with('product')->get();

        return view('admin_orders')->with('order_details', $orderdetails);

    }
}
