<?php

//namespace App\Http\Controllers\SMS;

namespace App\Http\Controllers;

use App\Farmer;
use App\Ussd_session;
use App\Person;
use App\Products;
use App\Outbox;
use App\SA_Products;

class Ussd extends Controller {

    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    function ussd() {
        // Reads the variables sent via POST
// );
        $Start = array(
            'sessionId' => request('sessionId'),
            'serviceCode' => request('serviceCode'),
            'phoneNumber' => request('phoneNumber'),
//            'networkCode' => request('networkCode'),
            'text' => request('text'),
        );

        $this->Anza($Start);
//        $this->test($Start);
//        echo 'Heres';
    }

    function anza($Start) {
        $level = 0; //Default level for a new session.

        $session_id = $Start['sessionId'];
        $texts = $Start['text'];
        $phoneNumber = $Start['phoneNumber'];
        $session_id = $Start['sessionId'];

        $textArray = explode('*', $texts); //Pick the last item keyed in by client.
        $input = trim(end($textArray));

        $sesexists = Ussd_session::where('session_id', $session_id)->first();

        if ($sesexists != null) {//if session exists.
            $ses_id = $sesexists->session_id;
            $level = $sesexists->level;
//            echo 'session ipo';
        } else {//if session doesn't exist.
//            echo 'session not found, insert new session';
            $wal_level = new Ussd_session;
            $wal_level->level = $level;
            $wal_level->phoneNumber = $phoneNumber;
            $wal_level->session_id = $session_id;
            $wal_level->level = 1; //Starts a new session
            $wal_level->save();
        }
        switch ($level) {
            //Level 0, Display main/home menu ';
            case "0":
                //   echo 'Level 0, Display main/home menu ';
                switch ($input) {
                    case "":
//                        echo 'Level 0, Display main menu ';
                        $text = 0;
                        $key = 1;
                        $this->response($text, $key);
                        break;

                    default:
                        break;
                }
                break;
            case "1": //Check if id exists as a farmer.
//                echo 'Level 1, Display menu one';
                //Check if farmer id exists- if exist proceed to menu one else request for a valid id number.
                $inputs = preg_replace('~\D~', '', $input);
                if (!empty($inputs)) {//Check if the Input is not empty
                    $id = $this->getid($inputs);
                    if ($id !== null) {
                        $farmer = $this->getfarmer($inputs);
                        foreach ($farmer as $tem) {
                            $pid = $tem['person_id'];
                            $farmer_id = $tem['farmerAccount']['farmer_id'];
                            $pname = $tem['first_name'];
                            $phone_number = $tem['phone_n0'];

                            if (!empty($farmer_id)) {
                                $level = 2;
                                $this->save_ids($session_id, $level, $pid, $farmer_id, $pname, $phone_number);

//                            print_r($tem);
//                            print_r($farmer_id);
//                    echo 'Found, display menu_one.<br>';
                                $text = 1;
                                $key = 1;
                                $this->response($text, $key);
                            } else {

                                $text = 0.11;
                                $key = 1;
                                $this->response($text, $key);
                            }
                        }
                    } else {//No farmer with that id.<br>"
//                    $this->menu_invalid(); 
//                        echo 'hamna';
                        $text = 0.12;
                        $key = 1;
                        $this->response($text, $key);
                    }
                } else {
                    $text = 0.1;
                    $key = 1;
                    $this->response($text, $key);
                }

                break;
            //level 2-Display vet and list of products menu 
            case "2":
//                echo 'Level 2, Display menu two';
                //level 2-Capture vet and display products menu 
                switch ($input) {
                    //invalid input.
                    case "":
                        $text = 0.13;
                        $key = 1; //Invalid input
                        $this->response($text, $key);
                        break;
                    //Vet Services.
                    case "1":
                        $text = 1.1;
                        $key = 1;
                        $this->response($text, $key);

                        //save vet request.
                        $this->save_service($session_id, $input);

                        $level = 3;
                        $this->update_level($session_id, $level);


                        break;
                    //Products menu.
                    case "2":
                        //System product counter
                        $pcounter = 0;
                        //Display products.
                        $products = $this->getperson($session_id);
//                        print_r($products);
                        $texts = array('CON');
                        foreach ($products as $values) {
                            $pid = $values['product_id'];
                            $pname = $values['product']['product_name'];
                            //Sytstem product ID
                            $s_pid = $pcounter + 1;

                            $texts[] = $s_pid . ' ' . $pname . "\n";
                        }
//                   print_r(implode("",$text));
                        $text = implode(" ", $texts);


                        $s_pid = 0;
                        //Display products.
                        $products = $this->getproducts();
//                        print_r($products);         
                        $text = implode(" ", $products);
                        $key = '';
                        $this->response($text, $key);

                        $level = 3;
                        $this->update_level($session_id, $level);

                        $inputs = preg_replace('~\D~', '', $input);
                        if (!empty($inputs)) {//Check if the Input is not empty
                            //save service id.
                            $this->save_service($session_id, $inputs);

                            $level = 3;
//                            $this->update_level($session_id, $level);
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }
                        break;
                    //Go back.
                    case "0":
                        $text = 1;
                        $key = 1;
                        $this->response($text, $key);

                        $level = 2;
                        $this->update_level($session_id, $level);
                        break;

                    //Farmer pressed 98 for more products.                    
                    case "98":

                        //System product counter
                        $pcounter = 0;
                        //Display products.
                        $products = $this->getperson($session_id);
//                        print_r($products);
                        $texts = array('CON');
                        foreach ($products as $values) {
                            $pid = $values['product_id'];
                            $pname = $values['product']['product_name'];
                            //Sytstem generated product ID,default id is 0 .
                            $s_pid = $pcounter + 1;

                            $texts[] = $s_pid . ' ' . $pname . "\n";
                        }
//                   print_r(implode("",$text));
                        $text = implode(" ", $texts);
                        $key = '';
                        $this->response($text, $key);

                        $inputs = preg_replace('~\D~', '', $input);
                        if (!empty($inputs)) {//Check if the Input is not empty
                            //save service id.
                            $this->save_service($session_id, $inputs);

                            $level = 3;
//                            $this->update_level($session_id, $level);
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }

                        break;


                    default:
                        $text = 0.13;
                        $key = 1;
                        $this->response($text, $key);
                        break;
                }
                break;


            case "3"://Save products
//                echo 'Level 3, Display menu three';

//            //level 2-Display vet and list of products menu 
            case "3":
//                echo 'Level 2, Display menu two';
                //Display products.

                $service_id = $this->get_service($session_id);
                switch ($service_id) {
                    case 1:
//                        $text = 6;
//                        $key = 0; //Invalid input
//                        $this->response($text, $key);


//                        echo 'success Request ' . $input;
                        $this->save_vet_request($session_id, $input);



                        //Get farmer name and send peronalized respone
                        $id = Ussd_session::where('session_id', $session_id)->first();
                        $fname = $id->first_name;
                        $text = "Thank you " . $fname . ", Your request has been sent. Your registered vet officer will get in touch to serve you.";

                        $key = 0;
                        $this->response($text, $key);
                        break;

                    case 2:

                        $inputs = preg_replace('~\D~', '', $input);
                        if (!empty($inputs)) {//Check if the Input is not empty
                            //save service id.
//                    echo 'Inputs ' . $inputs;
                            $this->save_product($session_id, $inputs); //save selected product id

                            $pid = $this->check_pid($inputs); //check if the product id keyed by client is valid
                            if ($pid) {
//                        $this->save_service($session_id, $inputs);
//                        echo 'SA PID ' . $inputs;

                                $pid = $this->check_sa_pid($inputs); //check if the product_id keyed is available on sales_agents_products table
                                if ($pid) {
//                        $this->save_service($session_id, $inputs);
//                        echo 'SA PID ' . $inputs;

                                    $products = $this->product_sa($session_id); //get product id
//                            print_r($products);
                                    $text = implode(" ", $products);
                                    $key = '';
                                    $this->response($text, $key);

                                    $level = 4;
                                    $this->update_level($session_id, $level);
                                } else {//Product id does not exist
//                            echo 'Input ' . $inputs;
                                    $text = 1.2;
                                    $key = 1;
                                    $this->response($text, $key);
                                }
                            } else {//Product id does not exist
//                        echo 'Input ' . $inputs;
                                $text = 1.2;
                                $key = 1;
                                $this->response($text, $key);
                            }
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }

                        break;
                }

                break;
            //level 3-Capture vet request and save products id
            case "4"://Save products
//                echo 'Level 3, Display menu three';

                $inputs = preg_replace('~\D~', '', $input);
                if (!empty($inputs)) {//Check if the Input is not empty
                    $pid = $this->check_sa_id($inputs); //check if the product id keyed by client is valid
                    if ($pid) {
//                        $this->save_service($session_id, $inputs);
//                        echo 'SA PID ' . $inputs;
                        $this->save_sa_product($session_id, $inputs);
                        $level = 5;
                        $this->update_level($session_id, $level);


                        $text = 3;
                        $key = 1;
                        $this->response($text, $key);
                    } else {//Product id does not exist
//                        echo 'Input ' . $inputs;
                        $text = 1.2;
                        $key = 1;
                        $this->response($text, $key);
                    }
                } else {
                    echo 'Input ' . $inputs;
                    $text = 0.1;
                    $key = 1;
                    $this->response($text, $key);
                }
                break;

            //Save Quantity and display total amount
            case "5":
                switch ($input) {
                    //invalid input.
                    case "":
                        $text = 0.1;
                        $key = 1; //Invalid input
                        $this->response($text, $key);

                        break;
                    //Save the Quantity
                    case $input:
                        $inputs = preg_replace('~\D~', '', $input);
                        if (!empty($inputs)) {//Check if the Input is not empty
                            //save order quantity
                            $this->save_quantity($session_id, $inputs);

                            //display _totals.
                            $this->display_totals($session_id, $inputs);

                            $level = 6;
                            $this->update_level($session_id, $level);
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }


                        break;

                    //Take me to home/ Main menu.
                    case "55":
                        $text = 1;
                        $key = 1;
                        $this->response($text, $key);

                        $level = 3;
                        $this->update_level($session_id, $level);
                        break;

                    case "0":
                        //Display products.
                        $products = $this->product_sa($session_id);
//                        print_r($products);         
                        $text = implode(" ", $products);
                        $key = '';
                        $this->response($text, $key);

                        $level = 2;
                        $this->update_level($session_id, $level);
                        break;

                    default:
                        break;
                }
                break;
            //level 4- capture order confirmation from client'
            case "6":
//                echo 'Level 4, Display stocks';
                switch ($input) {
                    //invalid input.
                    case "":
                        $text = 1;
                        $key = 1; //Invalid input
                        $this->response($text, $key);
                        break;
                    //Totals menu
                    case $input:
                        $inputs = preg_replace('~\D~', '', $input);

                        if (!empty($inputs)) {//Check if the Input is not empty
                            switch ($inputs) {
                                case "1":
                                    $this->confirm_order($session_id, $inputs);

                                    //Get farmer name
                                    $id = Ussd_session::where('session_id', $session_id)->first();
                                    $fname = $id->first_name;

                                    $text = "Thank you " . $fname . ", Your order has been received.Your registered corp will deliver your product upon payment.";

                                    $key = 0;
                                    $this->response($text, $key);

//                                    $text = 5;
//                                    $key = 0;
//                                    $this->response($text, $key);

                                    $level = 6;
                                    $this->update_level($session_id, $level);

                                    break;
                                case "2":
                                    $this->cancel_order($session_id, $inputs);

                                    $text = 4;
                                    $key = 0;
                                    $this->response($text, $key);

                                    $level = 6;
                                    $this->update_level($session_id, $level);

                                    break;

                                default:
                                    $text = 0.1;
                                    $key = 0;
                                    $this->response($text, $key);

                                    break;
                            }


                            if ($inputs == 1) {
                                //confirm _totals.      
//                                print_r($inputs);
                            } elseif ($inputs == 2) {//Cancel order
//                                echo 'Cancelled  ' . $inputs;
                            }
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }


                        break;
                    //Go back.
                    case "0":
                        $text = 1.1;
                        $key = 1;
                        $this->response($text, $key);
                        $level = 4;
                        $this->update_level($session_id, $level);
                        break;
                    //Take me to home/ Main menu.
                    case "55":
                        $text = 0;
                        $key = 0;
                        $this->response($text, $key);
                        $level = 2;

                        break;
                    default:
                        break;
                }
                break;
            //Add more logic here
            case "7":
                switch ($input) {
                    //invalid input.
                    case "":
                        $text = 1;
                        $key = 1; //Invalid input
                        $this->response($text, $key);
                        break;

                    //Display totals menu
                    case $input:
                        if (!empty($inputs)) {//Check if the Input is not empty
//                            echo 'confirm order.';
//                            if ($inputs == 1) {//confirm order
//                                $this->confirm_order($session_id, $inputs);
//
//                                $level = 6;
//                                $this->update_level($session_id, $level);
//                                
//                                $text = 5; //confirm totals text
//                                $key = 0;
//                                $this->response($text, $key);
//                            } else if ($inputs = 2) {//cancel order
//                                $text = 4; //confirm totals text
//                                $key = 0;
//                                $this->response($text, $key);
//
//                                $level = 6;
//                                $this->cancel_order($session_id, $level,$inputs);
//                            }
                        } else {
                            $text = 0.1;
                            $key = 1;
                            $this->response($text, $key);
                        }

                        $level = 7;
                        $this->update_level($session_id, $level);

                        break;
                    //Go back.
                    case "0":
                        $text = 1;
                        $key = 0;
                        $this->response($text, $key);
                        $level = 2;
                        break;
                    //Take me to home/ Main menu.
                    case "55":
                        $text = 0;
                        $key = 0;
                        $this->response($text, $key);
                        $level = 2;

                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }
    }

    function display_totals($session_id, $inputs) {
//        $price = 20;

        $id = Ussd_session::where('session_id', $session_id)->get();
        foreach ($id as $tem) {
            $fname = $tem['first_name'];
            $phone = $tem['phone_number'];
            $pid = $tem['product_id'];
            $sa_pid = $tem['sa_product_id'];

            $quantity_ordered = $tem['quantity_ordered'];
            $cost = SA_Products::where('id', $sa_pid)->first();

//            $cost = SA_Products::find($pid);
//            print_r($cost);
            $price = $cost->buying_price;

            $amount = $price * $quantity_ordered;


//            $response = "CON Press 1 to Confirm order of kshs." . $amount . " \n";
//            $response .= "2. to cancel.";
//            header('Content-type: text/plain');
//            echo $response;


            $pid = "CON Press 1 to Confirm order of kshs." . $amount . " \n";
            $pname = "2. to cancel.";

            $text = "\n" . $pid . ' ' . $pname . "\n";
            $key = '';
            $this->response($text, $key);
        }
    }

    function confirm_totals($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->confirm_price = $inputs;
        $wal_level->session_id = $session_id;
        $wal_level->save();
    }

    function save_totals($session_id, $amount) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->total = $amount;
        $wal_level->session_id = $session_id;
        $wal_level->save();
    }

    function save_vet_request($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->vet_request = $inputs;
        $wal_level->save();
    }

    function confirm_order($session_id, $inputs) {

        $price = 20;



//        echo 'Status '.$inputs;
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->order_status = $inputs;
        $wal_level->save();



        $id = Ussd_session::where('session_id', $session_id)->get();

        foreach ($id as $tem) {
            $fname = $tem['first_name'];
            $phone = $tem['phonenumber'];
            $pid = $tem['product_id'];
            $quantity_ordered = $tem['quantity_ordered'];
//            $wal_level = SA_Products::where('product_id', $pid)->first();

            $cost = SA_Products::where('product_id', $pid)->first();
            $price = $cost->buying_price;

            $amount = $price * $quantity_ordered;

            $order_no = $this->generateRandomChars();
            $msg = "Dear " . $fname . ", Your order has been received, Kindly deposit KSh." . $amount . " to paybill number 23134 account number " . $order_no . " ";
            $Outbox = new Outbox();
            $Outbox->source = "40149";
            $Outbox->destination = $phone;
            $Outbox->message = $msg;
            $Outbox->status = "Not Processed";
            $Outbox->save();
        }
    }

    function generateRandomChars($length = 10) {
        return substr(str_shuffle("ABCDEFGHJKLMNPQRSTUVWXYZ123456789"), 0, $length);
    }

    function cancel_order($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->order_status = $inputs;
        $wal_level->save();
    }

    function save_service($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->service_id = $inputs;
        $wal_level->save();
    }

    function save_product($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->product_id = $inputs;
        $wal_level->save();
    }

    function save_sa_product($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->sa_product_id = $inputs;
        $wal_level->save();
    }

    function check_sa_id($inputs) {
        $id = SA_Products::where('id', $inputs)->first();
        return $id;
    }

    function check_sa_pid($inputs) {
        $id = SA_Products::where('product_id', $inputs)->first();
        return $id;
    }

    function check_pid($inputs) {
        $id = Products::where('product_id', $inputs)->first();
        return $id;
    }

    function save_quantity($session_id, $inputs) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->quantity_ordered = $inputs;
        $wal_level->save();
    }

    function save_ids($session_id, $level, $pid, $farmer_id, $pname, $p_number) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->level = $level;
//        $wal_level->phone_number = $p_number;
        $wal_level->person_id = $pid;
        $wal_level->first_name = $pname;
        $wal_level->farmer_id = $farmer_id;
        $wal_level->save();
    }

    function response($text, $key) {

        switch ($text) {
            case "0":
                $out = "Welocme to LIMA,Enter your ID number to start order process \n";
                break;
            case "0.1":
                $out = "Invalid input, Kindly input a number \n";
                break;

            case "0.11":
                $out = "Kindly input a valid ID number \n";
                break;
            case "0.12":
                $out = "Sorry, ID number not registered on LIMA, Kindly contact your agent \n";
                break;
            case "0.13":
                $out = "Invalid input 1 or 2 \n";
                break;
            case "1":
                $out = "Select the service option \n";
                $out .= "1. Vet Services \n";
                $out .= "2. Products order";
                break;


            case "1.1":
                $out = "Enter brief description of the vet service required \n";
                break;
            case "1.2":
                $out = "Selected Product was not found within your subcounty";
                break;

            //For hardcoded product for test purposes
            case "2.1":
                $out = "Choose \n";
                $out .= "1. Acaricides \n";
                $out .= "2. Dewormers";
                $out .= "3. Dairy Hygiene";
                break;
            case "2.1.1":
                $out = "Choose \n";
                $out .= "1. Ex-KUPE \n";
                $out .= "2. Actraz";
                $out .= "3. Nefluke";
                break;
            case "2.1.2":
                $out = "Choose \n";
                $out .= "1. ANALGON BOLUSL \n";
                $out .= "2. ANIVERM DRENCH";
                $out .= "3. ANIVERM FORTE";
                break;
            case "3":
                $out = "Enter quantity for selected product from step 5 \n";
                break;
            case "4":
                $out = "Order canceled. \n";
                break;

            case "5":
                $out = "Order confirmed you will receive an sms shortly. \n";
                break;
            case "6":
                $out = "Request saved successfully. \n";
                break;

            default:
                $out = $text;
                break;
        }

        switch ($key) {
            case "1":
                $response = "CON " . $out;
                break;
            case "0":
                $response = "END " . $out;
                break;

            default:
                $response = $out;
                break;
        }

        header('Content-type: text/plain');
        echo $response;
    }

    function update_level($session_id, $level) {
        $wal_level = Ussd_session::where('session_id', $session_id)->first();
        $wal_level->level = $level;
        $wal_level->save();
    }

    function getid($inputs) {
        $id = Person::with('farmerAccount')->where('id_number', $inputs)->first();

        return $id;
    }

    function get_service($session_id) {
        $id = Ussd_session::where('session_id', $session_id)->first();

        return $id = $id->service_id;
    }

    function getfarmer($inputs) {
        $date = date('Y-m-d H:i:s', time());
        $id = Person::with('farmerAccount')->where('id_number', $inputs)->get();

        return $id;
    }

    function farmer_name($inputs) {
        $date = date('Y-m-d H:i:s', time());
    }


    function getperson($session_id) {
        $id = Ussd_session::with('Person')->where('session_id', $session_id)->first();
//        print($id);
//        $p_id = $id->person_id;
        $subcnty = $id->Person->sub_county_id;
        $pid = Person::where('sub_county_id', $subcnty)->get();
        foreach ($pid as $tem) {
            $p_id = $tem['person_id'];
//            echo ' pid ' . $p_id . '<br>';

            $sa_id = SA_Products::where('sales_agent_id', $p_id)->with('product')->get();

    function getproducts() {
        $texts = array('CON Select product e.g 4 for Tick Off ');
        $p_id = Products::all();
        foreach ($p_id as $values) {

            $pid = $values['product_id'];
            $pname = $values['product_name'];

            $texts[] = "\n" . $pid . '.' . $pname . "\n";
        }
        return $texts;
    }

    function product_sa($session_id) {

        $id = Ussd_session::where('session_id', $session_id)->first();
//        print($id);
        $p_id = $id->product_id;
        $subcnty = $id->Person->sub_county_id;

        $pid = Person::where('sub_county_id', $subcnty)->orderBy('person_id', 'ASC')->get();
        $texts = array('CON Select product to order e.g 4 for Tick Off ');

//        $pid = Order::whereHas('customer.country', function($innerQuery) {
//            $innerQuery->where('countries.name', 'LIKE', 'Uk');
//        })->get();
//        $pid = SA_Products::whereHas('person', function($innerQuery) {
//                    $innerQuery->where('sub_county_id', '175');
//                })->get();
//        $posts = App\Post::whereHas('comments', function ($query) {
//                    $query->where('content', 'like', 'foo%');
//                })->get();
//        $posts = SA_Products::whereHas('person', function ($query) {
//                    $query->where('sub_county_id', 'like', '175%');
//                })->get();

        foreach ($pid as $tem) {
            $sales_id = $tem['person_id'];
//            print($id);
//            echo ' pid ' . $pid . '<br>';

            $sa_id = SA_Products::where('sales_agent_id', $sales_id)->where('product_id', $p_id)->with('product')->orderBy('product_id', 'ASC')->get();
//            print($sa_id);

            foreach ($sa_id as $values) {
                $pid = $values['id'];
                $pname = $values['product']['product_name'];
                $s_price = $values['buying_price'];
//                $s_pid = $s_pid + 1;

                $texts[] = "\n" . $pid . '.' . $pname . ' Ksh.' . $s_price . "\n";
//                                echo ' pid ' . $pid . '<br>';
            }
//                             print_r(implode("",$text));
            return $texts;

        }
        print($sa_id);

//        return $sa_id;
    }

    function capture_text($input) {
//        echo 'Success capturing text,press zero to go back 55 to go back home ' . $input;
        //insert text descrption
    }

    function menu_one() {
        // This is the first request. Note how we start the response with CON
        $response = "CON Choose \n";
        $response .= "1. Select Vet \n";
        $response .= "2. Select Products";
        header('Content-type: text/plain');
        echo $response;
    }

}
