<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Broadcast;
use Auth;

class MessagingController extends Controller
{
    //
    public function broadcast(){

        // $broadcast = Broadcast::all();
        $broadcast = Broadcast::with('creator')->get();
        // echo(json_encode($broadcast));

        return view('broadcast')->with('broadcast',$broadcast);

    }

    public function addbroadcast(Request $request){

        try {

            if ($request->broadcast_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the broadcast name"]);
            }else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $broadcast = new Broadcast;
                $broadcast->broadcast_name = $request->input('broadcast_name');
                $broadcast->broadcast_content = $request->input('broadcast_content');
                $broadcast->created_by = Auth::user()->user_id;
                $broadcast->updated_by = Auth::user()->user_id;
                $broadcast->created_at = $date;
                $broadcast->updated_at = $date;
                $broadcast->save();
                                        
                $broadcast = Broadcast::all();
        
                return response(['status' => 'success', 'details' => $broadcast]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function editbroadcast(){

    }

    public function deletebroadcast(){

    }
}
