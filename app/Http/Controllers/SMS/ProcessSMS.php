<?php

namespace App\Http\Controllers\SMS;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\API\SenderController;
use App\Outbox;
use App\Person;
use App\Corp;
use App\Farmer;
use App\User;

class ProcessSMS extends Controller {

    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function getOutbox($phone, $msg) {

        $Outbox = new Outbox();
        $Outbox->source = "40149";
        $Outbox->destination = $phone;
        $Outbox->message = $msg;
        $Outbox->status = "Not Processed";
        $Outbox->save();

        $msgs = DB::table('outbox')->distinct()->get()->where('status', 'Not Processed');

        foreach ($msgs as $msg) {

            $outbox_id = $msg->outbox_id;
            $source = $msg->source;
            $destination = $msg->destination;
            $message = $msg->message;
            $status = $msg->status;
            $created_at = $msg->created_at;
            $updated_at = $msg->updated_at;

            $from = $source;
            $recipients = $destination;

            $sender = new SenderController();
            $send_msg = $sender->sender($outbox_id, $from, $recipients, $message);
            if ($send_msg === false) {
                //Error has occured....
            } else {
                //Success posting the  message ...
                DB::table('outbox')
                        ->where('outbox_id', $outbox_id)
                        ->update(['status' => "Processed"]);
            }
        }
    }

    public function getInbox() {

        try {    // all good
            $msgs = DB::table('inbox')->get()->where('processed', 'No');
            foreach ($msgs as $msg) {

                $phone_no = $msg->phone_no;
                $str = $msg->content;
                $txtid = $msg->inbox_id;
                $phone = $msg->phone_no;
                $inbox_id = $msg->inbox_id;

                // echo "Phone No. " . $phone_no . " Message -> " . $str . " Inbox ID  -> " . $inbox_id . '<br>';

                $text = ($str);

                if (strpos($text, 'RC') !== false) {
                    $a_explode = explode("*", $text);
                    $reg_rep_uc = @$a_explode[0];
                    $msg = @$a_explode[1];
                    $reg_rep_android = strtoupper($reg_rep_uc);
                    $rcvd = $msg;

                    echo"RC Found...";
                    $this->registerPerson($rcvd, $phone_no, $inbox_id);
                }

                if (strpos($text, 'RF') !== false) {
                    echo "RF Found...";
                    $a_explode = explode("*", $text);
                    $reg_rep_uc = @$a_explode[0];
                    $msg = @$a_explode[1];
                    $reg_rep_android = strtoupper($reg_rep_uc);
                    $rcvd = $msg;


                    if ($this->check_if_mobile_exists($phone_no)) {

                        $this->registerFarmer($rcvd, $inbox_id, $phone_no);
                    } else {
                        $msg = "Phone No not authorised to access this module";

                        $this->getOutbox($phone_no, $msg);
                    }
                }

                if (strpos($text, 'DEL') !== false) {
                    $a_explode = explode("*", $text);
                    $reg_rep_uc = @$a_explode[0];
                    $msg = @$a_explode[1];
                    $reg_rep_android = strtoupper($reg_rep_uc);
                    $rcvd = $msg;

                    if ($this->check_if_mobile_exists($phone_no)) {
                        $this->process_delivered($rcvd, $inbox_id);
                    } else {
                        $msg = "Phone No not authorised to access this module";
                        $this->getOutbox($phone_no, $msg);
                    }
                }

                if (strpos($text, 'PROD') !== false) {
                    $a_explode = explode("*", $text);
                    $reg_rep_uc = @$a_explode[0];
                    $msg = @$a_explode[1];
                    $reg_rep_android = strtoupper($reg_rep_uc);
                    $rcvd = $msg;

                    echo $phone_no;
                    if ($this->check_if_mobile_exists($phone_no)) {
                        echo "Phone Found....";
                        $this->process_PRODService($rcvd, $inbox_id, $phone_no);
                    } else {
                        $msg = "Phone No not authorised to access this module";
                        $this->getOutbox($phone_no, $msg);
                    }
                }

                if (strpos($text, 'VET') !== false) {
                    $a_explode = explode("*", $text);
                    $reg_rep_uc = @$a_explode[0];
                    $msg = @$a_explode[1];
                    $reg_rep_android = strtoupper($reg_rep_uc);
                    $rcvd = $msg;

                    if ($this->check_if_mobile_exists($phone_no)) {
                        $this->process_VETService($rcvd, $inbox_id, $phone_no);
                    } else {
                        $msg = "Phone No not authorised to access this module";
                        $this->getOutbox($phone_no, $msg);
                    }
                }

                echo"End 2";

                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);

                echo "End 3";
            }
        } catch (\Exception $e) {
            echo "Get Inbox Error => " . $e;
            // something went wrong
        }
    }

    function registerPerson($rcvd, $phone_no, $inbox_id) {

        try {    // all good
            $rcvd = base64_decode($rcvd);

            $andrd_APP = explode("*", $rcvd);


            $fname = @$andrd_APP[0];
            $lname = @$andrd_APP[1];
            $idnum = @$andrd_APP[2];
            $subcntyid = @$andrd_APP[3];
            $isvet = @$andrd_APP[4];
//        $gender = $andrd_APP[5];


            echo "Fname. " . $fname . '<br>' . "Lname. " . $lname . '<br>' . " ID NO. -> " . $idnum . '<br>' . " Vet -> " . $isvet . '<br>';
            //exit();
            if (Person::where('phone_no', '=', $phone_no)->exists() or Person::where('id_number', '=', $idnum)->exists()) {
                $msg = "Dear " . $fname . "   Your phone number or ID number exists in the system. Kindly contact the  support for further assistance";
                $this->getOutbox($phone_no, $msg);

                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);
            } else {



                $Persons = new Person();
                $Persons->first_name = $fname;
                $Persons->last_name = $lname;
                $Persons->id_number = $idnum;
                $Persons->sub_county_id = $subcntyid;
                $Persons->phone_no = $phone_no;
                $Persons->created_by = 1;
                $Persons->updated_by = 1;
                $Persons->updated_at = date("Y-m-d H:i:s");
                $Persons->save();
                $registration_type = "Community Resource Person";
                $this->thankMSG($fname, $phone_no, $isvet, $registration_type);


                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);
            }
        } catch (\Exception $e) {
            echo "Register Person error => " . $e;
            // something went wrong
        }
    }

    function thankMSG($fname, $phone_no, $isvet, $registration_type) {

        try {    // all good
            $date = date("Y-m-d H:i:s");

            $msg = "Jambo " . $fname . " ! You have been registered successfully into mobivet platform as a " . $registration_type . " . ";



            $this->getOutbox($phone_no, $msg);
            $this->registerCorps($phone_no, $isvet, $date);
        } catch (\Exception $e) {
            echo "Thanks Message Error = > " . $e;
            // something went wrong
        }
    }

    function registerCorps($phone_no, $is_vet, $date) {
        try {    // all good
            $check = DB::table('persons')->get()->where('phone_no', $phone_no);

            foreach ($check as $value) {
                $p_id = $value->person_id;
                $upd = $value->updated_by;

                if (Corp::where('person_id', '=', $p_id)->exists()) {
                    $msg = "Dear " . $fname . "   You are already registered as a Community Resource Person in the system.";
                    $this->getOutbox($farmer_phone_no, $msg);

                    DB::table('inbox')
                            ->where('inbox_id', $inbox_id)
                            ->update(['processed' => "Yes"]);
                } else {

                    // echo '' . $phone_no . '<br>' . $msg_string . '';
                    $Corps = new Corp();
                    $Corps->person_id = $p_id;
                    $Corps->is_vet = $is_vet;
                    $Corps->updated_at = $date;
                    $Corps->updated_by = $upd;
                    $Corps->created_by = 1;
                    $Corps->save();
                }
            }
        } catch (\Exception $e) {
            echo "Register CORPS Error => " . $e;
            // something went wrong
        }
    }

    function registerFarmer($rcvd, $inbox_id, $phone) {
//Check if person being registered exists in the  Database , if he/she exists then select the persons details and save him/her to the  farmers table. 
        try {    // all good
            $get_persons_id = Person::join('corps', 'corps.person_id', '=', 'persons.person_id')
                    ->select('corps.person_id', 'corps.corp_id')
                    ->where('phone_no', $phone)
                    ->get();




            foreach ($get_persons_id as $value) {
                $corp_person_id = $value->person_id;
                $corps_id = $value->corp_id;


                $rcvd = base64_decode($rcvd);
                // echo "Decoded Msg => ".$rcvd;
                $andrd_APP = explode("*", $rcvd);


                $fname = @$andrd_APP[0];
                $l_name = @$andrd_APP[1];
                $idnum = @$andrd_APP[2];
                $farmer_phone_no = @$andrd_APP[3];
                $sub_county_id = @$andrd_APP[4];
                $location = @$andrd_APP[5];
                $no_of_livestock = @$andrd_APP[6];

                $farmer_phone_no = preg_replace('/0/', '+254', $farmer_phone_no, 1);
                echo "New Farmer Phone No " . $farmer_phone_no . '<br.';

                if (Person::where('phone_no', '=', $farmer_phone_no)->exists() or Person::where('id_number', '=', $idnum)->exists()) {
                    // person found get persons details who initiated the  registration and save to farmers table with the created_by and updated_by columns being his/her persons_id



                    $get_farmer_persons_id = DB::table('persons')->get()->where('phone_no', $farmer_phone_no);

                    foreach ($get_farmer_persons_id as $persons_value) {
                        $farmer_person_id = $persons_value->person_id;
                        echo "Farmer phone no => " . $farmer_phone_no . " <br> Farmer person ID => " . $farmer_person_id . " <br>";


                        echo "Phone No  is found in the  database...";

                        if (Farmer::where('person_id', '=', $farmer_person_id)->exists()) {
                            $msg = "Dear " . $fname . "   You are already registered as a farmer in the system.";
                            $this->getOutbox($farmer_phone_no, $msg);

                            DB::table('inbox')
                                    ->where('inbox_id', $inbox_id)
                                    ->update(['processed' => "Yes"]);
                        } else {

                            echo "Our values = > F Name => " . $fname . ' L Name => ' . $l_name . ' ID Num => ' . $idnum . ' Phone No => ' . $farmer_phone_no . ' Sub Count ID => ' . $sub_county_id . ' Location => ' . $location . ' No of Livestock => ' . $no_of_livestock . '<br>';

                            $Farmers = new Farmer();
                            $Farmers->person_id = $farmer_person_id;
                            $Farmers->location = $location;
                            $Farmers->no_of_livestock = $no_of_livestock;
                            $Farmers->corp_id = $corps_id;
                            $Farmers->created_by = $corp_person_id;
                            $Farmers->updated_by = $corp_person_id;
                            $Farmers->save();

                            $registration_type = " Farmer ";
                            $isvet = "Empty";
                            $this->thankMSG($fname, $farmer_phone_no, $isvet, $registration_type);
                        }
                    }
                } else {
                    //person not found , register person to the  persons table and later register him/her as farmer.






                    $Persons = new Person();
                    $Persons->first_name = $fname;
                    $Persons->last_name = $l_name;
                    $Persons->id_number = $idnum;
                    $Persons->sub_county_id = $sub_county_id;
                    $Persons->phone_no = $farmer_phone_no;
                    $Persons->created_by = $corp_person_id;
                    $Persons->updated_by = $corp_person_id;
                    $Persons->save();
                    $farmer_person_id = $Persons->person_id;

                    $Farmers = new Farmer();
                    $Farmers->person_id = $farmer_person_id;
                    $Farmers->location = $location;
                    $Farmers->no_of_livestock = $no_of_livestock;
                    $Farmers->corp_id = $corps_id;
                    $Farmers->created_by = $corp_person_id;
                    $Farmers->updated_by = $corp_person_id;
                    $Farmers->save();
                    $registration_type = " Farmer ";
                    $this->thankMSG($fname, $phone_no, $isvet, $registration_type);
                }


                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);
            }
        } catch (\Exception $e) {
            echo "Register Farmer Error => " . $e;
            // something went wrong
        }
    }

    function process_PRod($rcvd, $phone_no) {

        try {    // all good
            $andrd_APP = explode("*", $rcvd);
            $idnumber = @$andrd_APP[1];
            $msg = "Please send the Product ID and Quantity  of the following list : "
                    . "   ";
            //Get the person details
            $get_person_details = DB::table('persons')->get()->where('id_number', $idnumber);
            foreach ($get_person_details as $value) {
                $pserons_id = $value->person_id;


                $get_corp_person_id = Person::join('farmers', 'farmers.person_id', '=', 'persons.person_id')
                        ->join('corp', 'corp.corp_id', '=', 'farmers.corp_id')
                        ->select('corps.person_id', 'corps.corp_id')
                        ->get();
                foreach ($get_corp_person_id as $value) {
                    $person_id = $value->person_id;
                    $corp_id = $value->corp_id;
                    $get_corp_product = DB::table('order_details')
                            ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                            ->join('persons', 'orders.source', '=', 'persons.person_id')
                            ->join('products', 'products.product_id', '=', 'order_details.product_id')
                            ->select('products.*')
                            ->where('persons.person_id', $person_id)
                            ->get();
                    foreach ($get_corp_product as $value) {
                        $product_name = $value->product_name;
                        $product_id = $value->product_id;
                        $msg .= "  " . $product_name . " => " . $product_id . "  ";
                    }
                }

                $msg .= " e.g 1*2 ";
            }

            $this->getOutbox($phone, $msg);
            DB::table('inbox')
                    ->where('inbox_id', $inbox_id)
                    ->update(['processed' => "Yes"]);
        } catch (\Exception $e) {
            echo "Process Product Error => " . $e;
            // something went wrong
        }
    }

    function process_PRODService($rcvd, $inbox_id, $phone) {
        try {    // all good
            $andrd_APP = explode("*", $rcvd);
            $idnumber = @$andrd_APP[0];

            $msg = "Please send either one of the following Products : "
                    . "   ";
            //Get the person details
            $get_person_details = DB::table('persons')->get()->where('id_number', $idnumber);
            foreach ($get_person_details as $value) {
                $person_id = $value->person_id;
                if (Farmer::where('person_id', '=', $person_id)->exists()) {

                    $get_corp_person_id = Person::join('farmers', 'farmers.person_id', '=', 'persons.person_id')
                            ->join('corps', 'corps.corp_id', '=', 'farmers.corp_id')
                            ->select('corps.person_id', 'corps.corp_id')
                            ->where('persons.person_id', $person_id)
                            ->get();
                    foreach ($get_corp_person_id as $value) {
                        $person_id = $value->person_id;
                        $corp_id = $value->corp_id;


                        if (DB::table('order_details')
                                        ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                                        ->join('persons', 'orders.source', '=', 'persons.person_id')
                                        ->join('products', 'products.product_id', '=', 'order_details.product_id')
                                        ->select('products.*')
                                        ->where('persons.person_id', $person_id)->exists()) {


                            $get_corp_product = DB::table('order_details')
                                    ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                                    ->join('persons', 'orders.source', '=', 'persons.person_id')
                                    ->join('products', 'products.product_id', '=', 'order_details.product_id')
                                    ->select('products.*')
                                    ->where('persons.person_id', $person_id)
                                    ->get();

                            foreach ($get_corp_product as $value) {
                                $product_name = $value->product_name;
                                $product_id = $value->product_id;
                                $msg .= "  " . $product_name . " => " . $product_id . "  ";
                            }
                            $msg .= " e.g 1*2 ";

                            $this->getOutbox($phone, $msg);
                            DB::table('inbox')
                                    ->where('inbox_id', $inbox_id)
                                    ->update(['processed' => "Yes"]);
                        } else {
                            $msg = " No Products were found in the  Store, please try again later.. ";

                            $this->getOutbox($phone, $msg);
                            DB::table('inbox')
                                    ->where('inbox_id', $inbox_id)
                                    ->update(['processed' => "Yes"]);
                        }
                    }
                } else {
                    $msg = "You're not registered as a Farmer in the  platform. ";
                    $this->getOutbox($phone, $msg);
                    DB::table('inbox')
                            ->where('inbox_id', $inbox_id)
                            ->update(['processed' => "Yes"]);
                }
            }
        } catch (\Exception $e) {
            echo "Process Product Service Error =>" . $e;
            // something went wrong
        }
    }

    function process_VETService($rcvd, $inbox_id, $phone) {

        try {    // all good
            $andrd_APP = explode("*", $rcvd);
            $idnumber = @$andrd_APP[0];
            $msg = "Please send either one of the following : "
                    . "   ";
            //Get the person details
            $get_person_details = DB::table('persons')->get()->where('id_number', $idnumber);
            foreach ($get_person_details as $value) {
                $pserons_id = $value->person_id;


                $get_corp_person_id = Person::join('farmers', 'farmers.person_id', '=', 'persons.person_id')
                        ->join('corp', 'corp.corp_id', '=', 'farmers.corp_id')
                        ->select('corps.person_id', 'corps.corp_id')
                        ->get();
                foreach ($get_cop_person_id as $value) {
                    $person_id = $value->person_id;
                    $corp_id = $value->corp_id;
                    $get_corp_product = DB::table('order_details')
                            ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                            ->join('persons', 'orders.source', '=', 'persons.person_id')
                            ->join('products', 'products.product_id', '=', 'order_details.product_id')
                            ->select('products.*')
                            ->where('persons.person_id', $person_id)
                            ->get();
                    foreach ($get_corp_product as $value) {
                        $product_name = $value->product_name;
                        $product_id = $value->product_id;
                        $msg .= "  " . $product_name . " => " . $product_id . "  ";
                    }
                }

                $msg .= " e.g 1*2 ";

                $this->getOutbox($phone, $msg);
                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);
            }
        } catch (\Exception $e) {
            echo "Process VET Service Error => " . $e;
            // something went wrong
        }
    }

    function process_delivered($rcvd, $inbox_id) {

        try {    // all good
            $andrd_APP = explode("*", $rcvd);
            $order_details_id = @$andrd_APP[1];

            //Get the quantity ordered from the  table order_details
            $get_order_details = DB::table('order_details')->get()->where('order_details_id', $order_details_id);
            foreach ($get_order_details as $value) {
                $quantity_ordered = $value->quantity_ordered;

                DB::table('order_details')
                        ->where('order_details_id', $order_details_id)
                        ->update(['quantity_delivered' => $quantity_ordered]);

                DB::table('inbox')
                        ->where('inbox_id', $inbox_id)
                        ->update(['processed' => "Yes"]);
            }
        } catch (\Exception $e) {
            echo "Process delivered error => " . $e;
            // something went wrong
        }
    }

    function check_mobile_exists($phone_no) {
        try {    // all good
            $querycheck = DB::table('persons')->get()->where('phone_no', $phone_no);
            if (!$querycheck) {
                return "exists";
            } else {
                return "empty";
            }
        } catch (\Exception $e) {
            echo "Check Mobile Exists error => " . $e;
            // something went wrong
        }
    }

    function check_if_mobile_exists($phone) {

        try {    // all good
            if (Person::where('phone_no', '=', $phone)->exists()) {
                // user found
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (\Exception $e) {
            echo "Check mobile exists error => " . $e;
            // something went wrong
        }
    }

    function send_products() {

        try {    // all good
        } catch (\Exception $e) {
            echo $e;
            // something went wrong
        }


        $get_products = DB::table('products')->get()->where('is_delete', '0');
        $get_corps = DB::table('persons')
                ->join('corps', 'corps.person_id', '=', 'persons.person_id')
                ->select('persons.*', 'corps.is_vet', 'corps.corp_id')
                ->get();
        foreach ($get_products as $value) {
            $product_id = $value->product_id;
            $product_name = $value->product_name;
            $product_code = $value->product_code;
            $created_at = $value->created_at;
            $created_by = $value->created_by;

            $msg_string = "PR*" . base64_encode($product_id . "*" . $product_name . "*" . $product_code . "#");

            foreach ($get_corps as $corps) {
                $phone_no = $corps->phone_no;
                echo '' . $phone_no . '<br>' . $msg_string . '';
                $msg = $msg_string;
                $phone = $phone_no;
                $this->getOutbox($phone, $msg);
            }
        }
    }

    function send_orders() {
        try {    // all good
            $get_product_orders = DB::table('order_details')
                    ->join('orders', 'orders.order_id', '=', 'order_details.order_id')
                    ->join('persons', 'orders.source', '=', 'persons.person_id')
                    ->join('products', 'products.product_id', '=', 'order_details.product_id')
                    ->select('order_details.*', 'orders.source', 'orders.order_to', 'persons.id_number', 'products.product_name', 'products.product_name')
                    ->get();
            foreach ($get_product_orders as $value) {

                $product_id = $value->product_id;
                $product_name = $value->product_name;
                $product_code = $value->product_code;
                $order_id = $value->order_id;
                $order_details_id = $value->order_details_id;
                $quantity_ordered = $value->quantity_ordered;
                $id_number = $value->id_number;
                $created_at = $value->created_at;
                $created_by = $value->created_by;
                $order_to = $value->order_to;



                $get_corps = DB::table('persons')
                        ->join('corps', 'corps.person_id', '=', 'persons.person_id')
                        ->select('persons.*', 'corps.is_vet', 'corps.corp_id')
                        ->where('person_id', $order_to)
                        ->get();


                $msg_string = "ORD*" . base64_encode($order_details_id . "*" . $product_id . "*" . $quantity_ordered . "*" . $id_number . "#");

                foreach ($get_corps as $corps) {
                    $phone_no = $corps->phone_no;
                    echo '' . $phone_no . '<br>' . $msg_string . '';
                    $msg = $msg_string;
                    $phone = $phone_no;
                    $this->getOutbox($phone, $msg);
                }
            }
        } catch (\Exception $e) {
            echo "Send Orders Error => " . $e;
            // something went wrong
        }
    }

    function me() {
        $str = 'UkMqayprKjEyMzQ1NioyMDIqdHJ1ZQ==';
        echo base64_decode($str);
    }

}
