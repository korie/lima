<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\SupplierProduct;
use Auth;


class ProductsController extends Controller
{
    //

    public function products(){
        $uid = Auth::user()->person_id;
        $myproducts = SupplierProduct::with('product')->where('supplier_id', $uid)->get();
        $products = Products::all();

        return view('products')->with('myproducts',$myproducts)->with('products',$products);

    }
    
    public function getproducts(){

        $products = Products::all();
        // json_encode($products));

        return view('admin_products')->with('products',$products);

    }
    public function add_products_admin(Request $request){

        try {
            
                if ($request->product_name == '') {
                    return response(['status' => 'error', 'details' => "Please enter the product name"]);
                }else {

                    date_default_timezone_set('UTC');
                    $date = date('Y-m-d H:i:s', time());
                    
                    
                    $product = new Products;
                    $product->product_name = $request->input('product_name');
                    $product->product_code = $request->input('product_code');
                    
                    $product->created_by = Auth::user()->person_id;
                    $product->updated_by = Auth::user()->person_id;
                    
                    $product->created_at = $date;
                    $product->updated_at = $date;
                    $product->save();
                                        
                    $product = Products::all();
                    //$product = Products::where('person_id',Auth::user()->person_id)->get();
              }
            
        
                return response(['status' => 'success', 'details' => $product]);
            

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function add_products_supplier(Request $request){

        try {           

                $products = $request->input('product_id');


                for($i =0 ; $i < sizeOf($products); $i++){

                $supplier_product = new SupplierProduct;
                $supplier_product->supplier_id = Auth::user()->person_id;
                $supplier_product->product_id = $products[$i];
                $supplier_product->save();
                
               
               }
               $product = SupplierProduct::with('product')->where('supplier_id',Auth::user()->person_id)->get();

                return response(['status' => 'success', 'details' => $product]);
            

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function editproduct(){

    }

    public function deleteproduct(){

    }
}
