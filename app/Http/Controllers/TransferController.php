<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferCorpRequest;
use App\Person;
use App\CorpVetTransfer;
use App\FarmerTransfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class TransferController extends Controller
{
    public function transfer(TransferCorpRequest $request)
    {
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:s', time());

        $subcounty = request("sub_county_id");;
        $person = request("person_id");
        $reason = request("reason");
        $updated_at = $date;

        // echo json_encode($request->person);

           $subcounty_from = Person::where('person_id', $person)->value('sub_county_id');
            
           if($subcounty_from){

                $transfer_farmer = DB::update("UPDATE persons SET sub_county_id = ?, updated_at = ?, updated_by = ? WHERE person_id = ?",[$subcounty, $updated_at, $person, $person]);

                if($transfer_farmer){
                    //check if person id corp or vet
                    $corps_status = DB::table('corps')->select('is_vet')->where('person_id', '=', $person)->value('is_vet');

                    if($corps_status === "No"){
                        
                        // $reg_farmers_count = DB::table('farmer_corp')->where('corp_id', '=', $person)->count();

                        $corp_vet_transfer = new CorpVetTransfer;
                        $corp_vet_transfer->person_id = $person;
                        $corp_vet_transfer->registered_farmers = DB::table('farmer_corp')->where('corp_id', '=', $person)->count();
                        $corp_vet_transfer->subcounty_from = $subcounty_from;
                        $corp_vet_transfer->subcounty_to = $subcounty;
                        $corp_vet_transfer->transfer_reason = $reason;
                        $corp_vet_transfer->transfer_date = $date;
                        $corp_vet_transfer->save();

                        //person is a corp - unattach all farmers linked to the corp
                        $unattach_farmer = DB::update("UPDATE farmer_corp SET corp_id = ? WHERE corp_id = ?",[null, $person]);

                        return response()->json(['messages' => ['CORP transfer successful']]);

                    }else if($corps_status === "Both"){

                        // $registered_farmers = DB::select("SELECT COUNT(*) FROM farmer_corp WHERE corp_id = ? AND vet_id = ?",[$person, $person]);

                        $corp_vet_transfer = new CorpVetTransfer;
                        $corp_vet_transfer->person_id = $person;
                        $corp_vet_transfer->registered_farmers = DB::table('farmer_corp')->where(['corp_id', '=', $person],['vet_id', '=', $person])->count();
                        $corp_vet_transfer->subcounty_from = $subcounty_from;
                        $corp_vet_transfer->subcounty_to = $subcounty;
                        $corp_vet_transfer->transfer_reason = $reason;
                        $corp_vet_transfer->transfer_date = $date;
                        $corp_vet_transfer->save();

                        //person is both a corp and a vet - unattach all farmers linked to the vet
                        $unattach_farmer = DB::update("UPDATE farmer_corp SET corp_id = ? WHERE vet_id = ? AND corp_id = ?",[null, $person, $person]);

                        return response()->json(['messages' => ['Transfer successful']]);

                    }else{

                        $corp_vet_transfer = new CorpVetTransfer;
                        $corp_vet_transfer->person_id = $person;
                        $corp_vet_transfer->registered_farmers = DB::table('farmer_corp')->where('vet_id', '=', $person)->count();
                        $corp_vet_transfer->subcounty_from = $subcounty_from;
                        $corp_vet_transfer->subcounty_to = $subcounty;
                        $corp_vet_transfer->transfer_reason = $reason;
                        $corp_vet_transfer->transfer_date = $date;
                        $corp_vet_transfer->save();

                        //person is a vet - unattach all farmers linked to the vet
                        $unattach_farmer = DB::update("UPDATE farmer_corp SET vet_id = ? WHERE vet_id = ?",[null, $person]);

                        return response()->json(['messages' => ['Vet transfer successful']]);
                    }
                    
                }else{
                    return response()->json(['messages' => ['Transfer not successful']], 500);
                }
            }else{
	           return response()->json(['messages' => ['Invalid sub county. Transfer not successful']], 500);
           }
        
        
    }

    public function farmer_transfer(Request $request){

        $farmer_person_id = $request->farmer_person_id;
        $service_person_id = $request->service_person_id;
        $reason = $request->transfer_reason;

        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:s', time());

        //check if person id corp or vet
        $corps_status = DB::table('corps')->select('is_vet')->where('person_id', '=', $service_person_id)->value('is_vet');

        if($corps_status === "No"){

            $farmer_transfer = new FarmerTransfer;
            $farmer_transfer->person_id = $farmer_person_id;
            $farmer_transfer->registered_by = $service_person_id;
            $farmer_transfer->transfer_reason = $reason;
            $farmer_transfer->transfer_date = $date;
            $farmer_transfer->save();

            //person is a corp - unattach all farmers linked to the corp
            $unattach_farmer = DB::update("UPDATE farmer_corp SET corp_id = ? WHERE farmer_id = ?",[null, $farmer_person_id]);

            return response()->json(['farmer_transfer' => 'Farmer unattached to CORP Successfully']);

        }else if($corps_status === "Both"){

            $farmer_transfer = new FarmerTransfer;
            $farmer_transfer->person_id = $farmer_person_id;
            $farmer_transfer->registered_by = $service_person_id;
            $farmer_transfer->transfer_reason = $reason;
            $farmer_transfer->transfer_date = $date;
            $farmer_transfer->save();

            //person is both a corp and a vet - unattach all farmers linked to the vet
            $unattach_farmer = DB::update("UPDATE farmer_corp SET corp_id = ?, vet_id = ? WHERE farmer_id = ?",[null, null, $farmer_person_id]);

            return response()->json(['farmer_transfer' => 'Farmer unattached successfully']);

        }else{

            $farmer_transfer = new FarmerTransfer;
            $farmer_transfer->person_id = $farmer_person_id;
            $farmer_transfer->registered_by = $service_person_id;
            $farmer_transfer->transfer_reason = $reason;
            $farmer_transfer->transfer_date = $date;
            $farmer_transfer->save();
            
            //person is a vet - unattach all farmers linked to the vet
            $unattach_farmer = DB::update("UPDATE farmer_corp SET vet_id = ? WHERE farmer_id = ?",[null, $farmer_person_id]);

            return response()->json(['farmer_transfer' => 'Farmer unattached to VET successfully']);
        }

    }

    


}
