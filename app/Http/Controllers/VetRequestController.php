<?php
	
	namespace App\Http\Controllers;
	
	use App\VetRequest;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Validator;
	
	class VetRequestController extends Controller
	{
		//
		public function vet_request(Request $request)
		{
			Validator::make($request->all(), [
					'person_id' => 'required|string',
			], [
					'person_id.required' => 'Person id is required',
			])->validate();
			
			$pid = $request->person_id;
			
			$vrequests = VetRequest::with('farmer')->where('vet', $pid)->get();
			
			//return $vrequests;
			
			$myarr = array();
			
			foreach ($vrequests as $vrequest) {
				
				array_push($myarr, array(
						"vet_request_id" => $vrequest->vet_request_id,
						"farmer_name" => $vrequest->farmer->first_name . " " . $vrequest->farmer->last_name,
						"farmer_phone" => $vrequest->farmer->phone_no,
						"status" => $vrequest->farmer->status,
						"order_date" => $vrequest->created_at,
						"comments" => $vrequest->vet_comments,
						"vet_request_status" => $vrequest->status));
			}
			return response()->json(['requests' => $myarr]);
			
		}
		
		public function vet_status_update(Request $request)
		{
			
			$pid = $request->person_id;
			$vet_request_id = $request->vet_request_id;
			$vet_status = $request->status;
			$vet_comments = $request->comments;
			
			$UpdateDetails = VetRequest::where('vet_request_id',$vet_request_id)
					->where('vet', $pid)->first();
			
			if ($UpdateDetails == null) {
				
				return response()->json(['messages' => 'Vet request details not found'], 404);
			} else {
				if ($UpdateDetails->vet == $pid) {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					VetRequest::where('vet_request_id', $vet_request_id)->where('vet', $pid)->update(['status' => $vet_status, 'updated_by' => $pid, 'vet_comments' => $vet_comments]);
					
					$vrequests = VetRequest::with('farmer')->where('vet', $pid)->get();
					
					$myarr = array();
					
					foreach ($vrequests as $vrequest) {
						
						array_push($myarr, array(
								"vet_request_id" => $vrequest->vet_request_id,
								"farmer_name" => $vrequest->farmer->first_name . " " . $vrequest->farmer->last_name,
								"farmer_phone" => $vrequest->farmer->phone_no,
								"status" => $vrequest->farmer->status,
								"order_date" => $vrequest->created_at,
								"comments" => $vrequest->vet_comments,
								"vet_request_status" => $vrequest->status));
					}
					
					return response()->json(['requests' => $myarr]);
					
				} else {
					return response()->json(['messages' => ['You do not have a vet request with this ID']]);
				}
			}
		}
		
	}
