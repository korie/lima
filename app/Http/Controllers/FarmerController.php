<?php
	
	namespace App\Http\Controllers;
	
	use App\Corp;
	use App\County;
	use App\Farmer;
	use App\FarmerCorp;
	use App\Person;
	use App\SubCounty;
	use App\User;
	use App\UserLevel;
	use Exception;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Validator;
	
	class FarmerController extends Controller
	{
		public function Farmers()
		{
			$counties = County::all();
			$userlevels = UserLevel::all();
			$subcounties = SubCounty::all();
			$corps = Corp::all();
			
			$farmers = Farmer::with('person.sub_county.county')->get();
			
			$data = array(
					'farmers' => $farmers,
					'counties' => $counties,
					'corps' => $corps,
					'subcounties' => $subcounties,
					'userlevels' => $userlevels,
			);
			return view('farmers')->with($data);
			
		}
		
		public function adduser(Request $request)
		{
			
			try {
				
				if ($request->first_name == '') {
					return response(['status' => 'error', 'details' => "Please enter the First name"]);
				}
				if ($request->last_name == '') {
					return response(['status' => 'error', 'details' => "Please enter the Last name"]);
				} else {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					$pno = $request->input('phone_no');
					$str = substr($pno, 1);
					
					$fullno = "+254" . $str;
					
					$person = new Person;
					$person->first_name = $request->input('first_name');
					$person->last_name = $request->input('last_name');
					$person->id_number = $request->input('id_num');
					$person->phone_no = $fullno;
					$person->sub_county_id = $request->input('sub_county_id');
					$person->created_by = Auth::user()->user_id;
					$person->updated_by = Auth::user()->user_id;
					$person->created_at = $date;
					$person->updated_at = $date;
					$person->save();
					
					$pid = $person->person_id;
					
					$user = new User;
					
					$user->person_id = $pid;
					$user->email = $request->input('email');
					$user->password = bcrypt($request->input('phone_no'));
					$user->user_level = $request->input('user_level_id');
					$user->status = 'Active';
					$user->created_by = Auth::user()->user_id;
					$user->updated_by = Auth::user()->user_id;
					$user->created_at = $date;
					$user->updated_at = $date;
					$user->save();
					
					$counties = County::all();
					$subcounties = SubCounty::all();
					$userlevels = UserLevel::all();
					
					$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
					
					$data = array(
							'users' => $users,
							'counties' => $counties,
							'subcounties' => $subcounties,
							'userlevels' => $userlevels,
					);
					return response(['status' => 'success', 'details' => $data]);
				}
				
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
			
		}
		
		public function edituser(Request $request)
		{
			try {
				
				if ($request->first_name == '') {
					return response(['status' => 'error', 'details' => "Please enter the First name"]);
				} else {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					$user = User::find($request->input('id'));
					$user->email = $request->input('email');
					$user->user_level = $request->input('user_level_id');
					$user->status = $request->input('status');
					$user->updated_at = $date;
					$user->updated_by = Auth::user()->user_id;
					$user->save();
					
					$pid = $user->person_id;
					
					$pno = $request->input('phone_no');
					$str = substr($pno, 1);
					$fullno = "+254" . $str;
					
					$person = Person::find($pid);
					$person->first_name = $request->input('first_name');
					$person->last_name = $request->input('last_name');
					$person->id_number = $request->input('id_num');
					$person->phone_no = $fullno;
					$person->sub_county_id = $request->input('sub_county_id');
					$person->updated_at = $date;
					$person->updated_by = Auth::user()->user_id;
					$person->save();
					
					$counties = County::all();
					$subcounties = SubCounty::all();
					$userlevels = UserLevel::all();
					
					$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
					
					$data = array(
							'users' => $users,
							'counties' => $counties,
							'subcounties' => $subcounties,
							'userlevels' => $userlevels,
					);
					return response(['status' => 'success', 'details' => $data]);
					
				}
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
			
		}
		
		public function deleteuser(Request $request)
		{
			try {
				date_default_timezone_set('UTC');
				$date = date('Y-m-d H:i:s', time());
				
				$user = User::find($request->input('user_id'));
				$user->status = 'Deleted';
				$user->updated_at = $date;
				$user->updated_by = Auth::user()->user_id;
				$user->save();
				$counties = County::all();
				$subcounties = SubCounty::all();
				$userlevels = UserLevel::all();
				
				$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
				
				$data = array(
						'users' => $users,
						'counties' => $counties,
						'subcounties' => $subcounties,
						'userlevels' => $userlevels,
				);
				return response(['status' => 'success', 'details' => $data]);
				
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
		}
		
		public function get_subcounties(Request $request)
		{
			
			$county_id = $request->county_id;
			
			$subcounties = SubCounty::where('county_id', $county_id)->get();
			
			return $subcounties;
			
		}
		
		public function corp_farmers(Request $request)
		{
			
			$pids = FarmerCorp::with('farmer_details.person')->where('corp_id', $request->person_id)->get();
			
			$myarr = array();
			
			// echo json_encode($request->person_id);
			
			foreach ($pids as $pid) {
				array_push($myarr, array("corp_id" => $pid->corp_id, "farmer_id" => $pid->farmer_details->farmer_id, "farmer_name" => $pid->farmer_details->person->first_name . " " . $pid->farmer_details->person->last_name, "location" => $pid->farmer_details->location, "phone_no" => $pid->farmer_details->person->phone_no, "farmer_status" => $pid->farmer_details->person->status, "date_created" => $pid->farmer_details->person->created_at));
			}
			
			return response()->json(['corp_farmers' => $myarr]);
			
		}
		
		public function vet_farmers(Request $request)
		{
			
			$person_id = $request->person_id;
			
			$pids = FarmerCorp::with('farmer_details.person')->where('vet_id', $person_id)->get();
			
			$myarr = array();
			
			foreach ($pids as $pid) {
				array_push($myarr, array("vet_id" => $pid->vet_id, "farmer_id" => $pid->farmer_details->farmer_id, "farmer_name" => $pid->farmer_details->person->first_name . " " . $pid->farmer_details->person->last_name, "location" => $pid->farmer_details->location, "phone_no" => $pid->farmer_details->person->phone_no, "farmer_status" => $pid->farmer_details->person->status, "date_created" => $pid->farmer_details->person->created_at));
			}
			
			return response()->json(['vet_farmers' => $myarr]);
			
		}
		
		public function my_farmers(Request $request)
		{
			
			Validator::make($request->all(), [
					'person_id' => 'required|string',
			], [
					'person_id.required' => 'Person id is required',
			])->validate();
			
			//DB::enableQueryLog();
			
			$bulkInserts = collect($this->bulkInsert());
			
			$corp = Corp::where('person_id', $request->person_id)->first();
			
			if (strtolower($corp->is_vet) === "yes") {
				$farmers = FarmerCorp::where('vet_id', $corp->person_id)
						->with('person')
						->with('apiFarmer')
						->get();
			} else if (strtolower($corp->is_vet) === "no") {
				$farmers = FarmerCorp::where('corp_id', $corp->person_id)
						->with('person')
						->with('apiFarmer')
						->get();
			} else {
				$farmers = FarmerCorp::where(function ($query) use ($corp) {
					$query->where('corp_id', $corp->person_id);
					$query->orWhere('vet_id', $corp->person_id);
				})->whereHas('person', function ($query) use ($corp) {
					$query->where('sub_county_id', '=', $corp->person->sub_county_id);
				})->with('person')
						->with('apiFarmer')
						->get();
			}
			
			//var_dump(DB::getQueryLog());
			
			$farmers = $this->formatForRest($farmers);
			
			$farmers = $farmers->concat($bulkInserts)->sortBy('first_name');
			
			return response()->json($farmers->values()->all());
		}
		
		private function formatForRest($farmers)
		{
			$farmers = $farmers->map(function ($farmer) {
				$person = $farmer->person;
				$apiFarmer = $farmer->apiFarmer;
				$newFarmer = [
						'person_id' => $person != null ? $person->person_id : 0,
						'farmer_id' => $apiFarmer != null ? $apiFarmer->farmer_id : 0,
						'first_name' => $person != null ? $person->first_name : null,
						'last_name' => $person != null ? $person->last_name : null,
						'id_number' => $person != null ? $person->id_number : null,
						'phone_no' => $person != null ? $person->phone_no : null,
						'status' => $person != null ? $person->status : null,
						'created_at' => $person != null ? $person->created_at : null,
						'location' => $apiFarmer != null ? $apiFarmer->location : null,
						'no_of_livestock' => $apiFarmer != null && $apiFarmer->no_of_livestock != null ? $apiFarmer->no_of_livestock : 0,
						'vet_id' => $farmer->vet_id != null ? $farmer->vet_id : 0,
						'corp_id' => $farmer->corp_id != null ? $farmer->corp_id : 0,
						'sub_county_id' => $person != null ? $person->sub_county_id : 0,
				];
				return $newFarmer;
			});
			return $farmers;
		}
		
		private function bulkInsert()
		{
			$rejectedRecords = [];
			$personId = request('person_id');
			$newFarmers = json_decode(request("farmers"));
			$newFarmers = collect($newFarmers);
			$filteredNewFarmers = $newFarmers->reject(function ($farmer) use (&$rejectedRecords) {
				$farmer = (array)$farmer;
				$reasons = [];
				$exists = null;
				
				if (empty($farmer['first_name'])) {
					$reasons[] = 'Farmer\'s first name is required';
				} elseif (strlen($farmer['first_name']) > 50) {
					$reasons[] = 'Farmer\'s first name can only be so long. 50 characters maximum';
				}
				
				if (empty($farmer['last_name'])) {
					$reasons[] = 'Farmer\'s last name is required';
				} elseif (strlen($farmer['last_name']) > 50) {
					$reasons[] = 'Farmer\'s last name can only be so long. 50 characters maximum';
				}
				
				if (empty($farmer['id_number'])) {
					$reasons[] = 'Farmer\'s ID Number is required';
				} elseif (!is_numeric($farmer['id_number'])) {
					$reasons[] = 'Farmer\'s ID Number can only be a number';
				}
				
				if (empty($farmer['phone_no'])) {
					$reasons[] = 'Farmer\'s mobile number is required';
				}
				
				if (empty($farmer['location'])) {
					$reasons[] = 'Farmer location is required';
				}
				
				if (empty($farmer['location'])) {
					$reasons[] = 'Farmer location is required';
				} elseif (strlen($farmer['location']) > 255) {
					$reasons[] = 'Farmer\'s location can only be so long. 255 characters maximum';
				}
				
				if (empty($reasons)) {
					$exists = Person::where(function ($query) use ($farmer) {
						$query->where('id_number', $farmer["id_number"]);
						$query->orWhere('phone_no', $farmer["phone_no"]);
					})->whereHas('sub_county')->first();
					if ($exists != null) {
						$reasons[] = 'Another farmer exists with the same phone number or ID number';
					}
				}
				
				if (!empty($reasons)) {
					$farmer['status'] = "Rejected";
					$farmer['reasons'] = json_encode($reasons);
					$rejectedRecords[] = $farmer;
				}
				return $exists != null;
			});
			
			$filteredNewFarmers->each(function ($farmer) use ($personId) {
				$farmer = (array)$farmer;
				$farmer['person_id'] = $personId;
				app(\App\Http\Controllers\RegisterCorpController::class)->reg_farmer($farmer);
			});
			
			return $rejectedRecords;
		}
		
		public function unattached_farmers(Request $request)
		{
			
			Validator::make($request->all(), [
					'person_id' => 'required|string',
			], [
					'person_id.required' => 'Person id is required',
			])->validate();
			
			$corp = Corp::where('person_id', $request->person_id)->first();
			
			if (strtolower($corp->is_vet) === "yes") {
				$farmers = FarmerCorp::where('vet_id', null)
						->whereHas('person', function ($query) use ($corp) {
							$query->where('sub_county_id', '=', $corp->person->sub_county_id);
						})->with('person')
						->with('apiFarmer')
						->get();
			} else if (strtolower($corp->is_vet) === "no") {
				$farmers = FarmerCorp::where('corp_id', null)
						->with('person')
						->with('apiFarmer')
						->get();
			} else {
				$farmers = FarmerCorp::where(function ($query) use ($corp) {
					$query->where('corp_id', null);
					$query->orWhere('vet_id', null);
				})->whereHas('person', function ($query) use ($corp) {
					$query->where('sub_county_id', $corp->person->sub_county_id);
				})->with('person')
						->with('apiFarmer')
						->get();
			}
			
			$farmers = $this->formatForRest($farmers->sortBy('first_name'));
			
			return response()->json($farmers->values()->all());
		}
		
		public function attach_farmer(Request $request)
		{
			Validator::make($request->all(), [
					'person_id' => 'required|string',
					'farmer_id' => 'required|string',
			], [
					'person_id.required' => 'The Corp\'s Person Id is required',
					'farmer_id.required' => 'Farmer id is required',
			])->validate();
			
			$corp = Corp::where('person_id', $request->person_id)->first();
			
			if (strtolower($corp->is_vet) === "yes") {
				FarmerCorp::where('farmer_id', $request->farmer_id)
						->update(['vet_id' => $corp->person_id]);
			} else if (strtolower($corp->is_vet) === "no") {
				FarmerCorp::where('farmer_id', $request->farmer_id)
						->update(['corp_id' => $corp->person_id]);
			} else {
				$farmerCorp = FarmerCorp::where('farmer_id', $request->farmer_id)
						->first();
				$farmerCorp->corp_id = $farmerCorp->corp_id === null ? $corp->person_id : $farmerCorp->corp_id;
				$farmerCorp->vet_id = $farmerCorp->vet_id === null ? $corp->person_id : $farmerCorp->vet_id;
				$farmerCorp->save();
			}
			return response()->json(['messages' => ['Farmer successfully attached']]);
		}
		
		public function unattach_farmer(Request $request)
		{
			Validator::make($request->all(), [
					'person_id' => 'required|string',
					'farmer_id' => 'required|string',
					'release' => 'nullable|integer',
			], [
					'person_id.required' => 'The Corp\'s Person Id is required',
					'farmer_id.required' => 'Farmer id is required',
			])->validate();
			
			$corp = Corp::where('person_id', $request->person_id)->first();
			
			if (strtolower($corp->is_vet) === "yes") {
				FarmerCorp::where('farmer_id', $request->farmer_id)
						->update(['vet_id' => null]);
			} else if (strtolower($corp->is_vet) === "no") {
				FarmerCorp::where('farmer_id', $request->farmer_id)
						->update(['corp_id' => null]);
			} else {
				$farmerCorp = FarmerCorp::where('farmer_id', $request->farmer_id)
						->first();
				$farmerCorp->corp_id = $farmerCorp->corp_id === $corp->person_id ? null : $farmerCorp->corp_id;
				$farmerCorp->vet_id = $farmerCorp->vet_id === $corp->person_id ? null : $farmerCorp->vet_id;
				$farmerCorp->save();
			}
			
			return response()->json(['messages' => ['Farmer successfully unattached']]);
		}
		
		public function getFarmer(Request $request)
		{
			
			Validator::make($request->all(), [
					'farmer_id' => 'required|string',
			], [
					'farmer_id.required' => 'Person id is required',
			])->validate();
			
			$farmer = Farmer::with('person')
					->where('farmer_id', request('farmer_id'))->first();
			
			if ($farmer != null) {
				
				return [
						'person_id' => $farmer->person_id,
						'farmer_id' => $farmer->farmer_id,
						'first_name' => $farmer->person->first_name,
						'last_name' => $farmer->person->last_name,
						'id_number' => $farmer->person->id_number,
						'phone_no' => $farmer->person->phone_no,
						'status' => $farmer->person->status,
						'created_at' => $farmer->created_at,
						'location' => $farmer->location,
						'no_of_livestock' => $farmer->no_of_livestock,
						'vet_id' => $farmer->vet_id,
						'corp_id' => $farmer->corp_id,
						'sub_county_id' => $farmer != null ? $farmer->person->sub_county_id : null,
				];
			}
			
			return response()->json(['messages' => ['farmer not found']], 404);
		}
		
	}
