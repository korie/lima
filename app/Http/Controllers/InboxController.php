<?php

namespace App\Http\Controllers;

use App\Inbox;
use App\FarmOrderLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\SenderController;
use App\Outbox;
use App\Corp;
use App\Products;
use App\Stock;
use App\Person;
use App\Farmer;
use App\Order;
use App\OrderDetail;
use App\VetRequest;
use Illuminate\Support\Facades\DB;

class InboxController extends Controller
{
    public function process(){

        $inbox = Inbox::where('processed', 'No')->get();

        foreach($inbox as $inb){

            $inbid = $inb->inbox_id;
            $source = $inb->source;
            $text = strtolower($inb->content);

            if($text == 'lima'){

                $inlevel = FarmOrderLevel::where('phone_no',  $source)->orderBy('created_at', 'desc')->first();

               if($inlevel == null){
                    $level = new FarmOrderLevel;

                    $level->phone_no = $source;
                    $level->level = 0;

                    $level->save();
                    $msg = "Send desired service and your Id number to 40149 beginning with L*  eg L*Vet*12345678 or L*Prod*12345678. ";

               }else {
                    if(date('Ymd') == date('Ymd', strtotime($inlevel->created_at))){
                        if($inlevel->level === 10000){
                            $level = new FarmOrderLevel;

                            $level->phone_no = $source;
                            $level->level = 0;

                            $level->save();

                            $msg = "Send desired service and your Id number to 40149 beginning with L*  eg L*Vet*12345678 or L*Prod*12345678. ";
                            
                        }else{
                            $msg = "Kindly send message in required format";
                        }
                    
                    }else{

                        if( $inlevel->level === 10000){
                            $level = new FarmOrderLevel;

                            $level->phone_no = $source;
                            $level->level = 0;

                            $level->save();

                            $msg = "Send desired service and your Id number to 40149 beginning with L*  eg L*Vet*12345678 or L*Prod*12345678. ";
                        }
                        else{
                            $inlevel->level = 10000;
                            $inlevel->save();

                            $level = new FarmOrderLevel;

                            $level->phone_no = $source;
                            $level->level = 0;

                            $level->save();

                            $msg = "Send desired service and your Id number to 40149 beginning with L*  eg L*Vet*12345678 or L*Prod*12345678. ";
                        }

                    }
                   
                }
                $this->sender($source,$msg);
                   
            }
            else if(strpos($text, 'l*vet') !== false){

                $level = FarmOrderLevel::where('phone_no', $source)->orderBy('created_at', 'desc')->first();

                $explode = explode("*", $text);

                $idnum = $explode[2];

                if($level->level == 0){

                    $person = Person::where('id_number', $idnum)->with('sub_county.county')->first();
                    

                    if($person == null){
                        $msg = "The ID number has not been registered. Contact your CORP to aid with registration.";
                    }
                    else{
                        $farmer = Farmer::where('person_id', $person->person_id)->first();

                        if($farmer == null ){
                            $msg = "You are not registered in the system as a Farmer.";

                            $level->person_id = $person_id;
                            $level->level = 10000;
                            $level->save();
                        }
                        else{

                            $vet_id = $person->created_by;
                            $person_id = $person->person_id;
                                
        
                                $vreq = new VetRequest;
        
                                $vreq->source = $person_id;
                                $vreq->vet = $vet_id;
        
                                $savereq = $vreq->save();
        
                                if($savereq){
        
                                    $vet = Person::find($vet_id);
        
                                    $num = $vet->phone_no;
                                    $msg1 = "You have a request for a vet from ". $person->first_name ." ".$person->last_name .", ". $person->sub_county->name . " sub county. Kindly contact them";
                            
                                    $this->sender($num,$msg1);
                                }
        
                                $msg = "We have notified your CORP who will contact you shortly.";
        
                        }
                  
                        $level->person_id = $person_id;
                        $level->level = 10000;
                        $level->save();

                    }
                                       
                }
                else{

                    $msg = "Kindly send message in required format";
                }
                $this->sender($source,$msg);

            }
            else if(strpos($text, 'l*prod') !== false){
                $level = FarmOrderLevel::where('phone_no', $source)->orderBy('created_at', 'desc')->first();
                $explode = explode("*", $text);

                $idnum = $explode[2];
                if($level->level == 0){

                    $person = Person::where('id_number', $idnum)->with('sub_county.county')->first();

                    if($person == null){
                        $msg = "The ID number has not been registered. Contact your CORP to aid with registration.";
                    }
                    else {
                        $farmer = Farmer::where('person_id', $person->person_id)->first();

                        if($farmer == null ){
                            $msg = "You are not registered in the system as a Farmer.";
                            $level->person_id = $person_id;
                            $level->level = 10000;
                            $level->save();
                        }
                        else{

                            $corp_id = $person->created_by;
                            $person_id = $person->person_id;

                            $products = Products::all();

                            $append = "";
                            foreach($products as $product){

                                $pid = $product->product_id;

                                $name = $product->product_name;

                                $append = $append. $pid . ". ".$name . "\n";


                            }

                            $msg = " Select the product and send back its id and quantity in the format L*id*quantity: \n".$append."\n"."eg L*1*20";
                            $level->person_id = $person_id;
                            $level->level = 1;
                            $level->save();    
                                                
                        }  
                    }
                } 
                else {
                    $msg = "Kindly send message in required format";
                }              
                    
                    

                $this->sender($source,$msg);  
                    

             

            }
            else if(strpos($text, 'l*') !== false){
                $explode = explode("*", $text);

                $id = $explode[1];
                $q = $explode[2];

                if(is_numeric($id)){

                    $product = Products::find($id);

                    if($product == null){

                        $msg = "The Product ID sent does not exist, kindly confirm that you sent the right one";
                    }else{

                        $level = FarmOrderLevel::where('phone_no', $source)->where('level', 1)->first();

                        $person_id = $level->person_id;
        
                        $person = Person::find($person_id);
        
                        $corp_id = $person->created_by;

                        $order = new Order;
                        $order->source = $person_id;
                        $order->order_to = $corp_id;
                        $order->status = 0;
                        $order->created_by = 1;
                        $order->updated_by = 1;

                        $saveorder = $order->save();

                        if($saveorder){

                            $odts = new OrderDetail;
                            $odts->order_id = $order->order_id;
                            $odts->product_id = $id;
                            $odts->quantity_ordered = $q;
                            $odts->quantity_delivered = 0;
                            $odts->created_by = 1;
                            $odts->updated_by = 1;

                            $odts->save();


                        }

                        $msg = "Your order has been sent successfully, the CORP will be in touch shortly.";

                        $level->level = 10000;
                        $level->save();  


                        $this->sender($source,$msg);

                        $prod = Products::find($id);

                        $co = Person::find($corp_id);

                        $pno = $co->phone_no;

                        $pname = $product->product_name;

                        $msg3 = "You have an order for ". $pname . " from ". $person->first_name ." ".$person->last_name .", ". $person->sub_county->name . " sub county. Kindly contact them";
                
                        $this->sender($pno,$msg3);
                    

            

                    }

              
                }
            }


            $inb->processed = 'Yes';

            $inb->save();
        }


    }


    public function sender($source, $msg){

        $Outbox = new Outbox();
        $Outbox->source = "40149";
        $Outbox->destination = $source;
        $Outbox->message = $msg;
        $Outbox->status = "Not Processed";
        $Outbox->save();

       
        $msgs = Outbox::where('status', 'Not Processed')->get();

        foreach ($msgs as $msg) {

            $outbox_id = $msg->outbox_id;
            $short_code = $msg->source;
            $phone_no = $msg->destination;
            $message = $msg->message;
            $status = $msg->status;
            $created_at = $msg->created_at;
            $updated_at = $msg->updated_at;


            $from = $short_code;
            $recipients = $phone_no;


            $from = $short_code;
            $recipients = $phone_no;

            $sender = new SenderController();
            $send_msg = $sender->sender($outbox_id, $from, $recipients, $message);
            if ($send_msg === false) {
                //Error has occured....
            } else {
                //Success posting the  message ...

                DB::table('outbox')
                ->where('outbox_id', $outbox_id)
                ->update(['status' => "Processed"]);
            }
        }
    }


    // public function order(){

    //     $orders = OrderDetail::with('order')->get();

    //     echo json_encode($orders);

    // }
}
