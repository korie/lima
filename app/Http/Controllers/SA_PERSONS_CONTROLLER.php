<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SA_PERSONS;
use App\County;
use App\SubCounty;
use App\SAS;
use App\Person;
use App\User;
use Auth;

class SA_PERSONS_CONTROLLER extends Controller{
    
    public function SA_persons(){
        $counties = County::all();
        $subcounties = SubCounty::all();
        $sas = SAS::all();
        $user = User::all();
        $SA_persons = SA_PERSONS::with('person.sub_county.county')->with('sas')->with('creator')->get();
        $data = array(
            'sa_persons' => $SA_persons,
            'counties' => $counties,
            'subcounties' => $subcounties,
            'user' => $user,
            
        );
        return view('SA_persons')-> with($data);
}
public function add_sa_person(Request $request)
{
    try {
    if ($request ->first_name == ''){
        return response(['status' => 'error', 'details' => "Please enter the first name"]);
    }
    if ($request ->last_name == ''){
        return response(['status' => 'error', 'details' => "Please enter the last name"]);
    } else {
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:s', time());
        
        $person = new Person;
        $person->first_name = $request->input('first_name');
        $person->last_name = $request->input('last_name');
        $person->id_number = $request->input('id_num');
        $person->phone_no = $request->input('phone_no');
        $person->sub_county_id = $request->input('sub_county_id');
        $person->created_by = Auth::user()->user_id;
        $person->updated_by = Auth::user()->user_id;
        $person->created_at = $date;
        $person->updated_at = $date;
        $person->save();
        
        $pid = $person->person_id;
        
        $sa_person = new SA_PERSONS;
        
        $sa_person->person_id = $pid;
        $sa_person->created_by = Auth::user()->user_id;
        $sa_person->updated_by = Auth::user()->user_id;
        $sa_person->created_at = $date;
        $sa_person->updated_at = $date;
        $sa_person->save();
        $counties = County::all();
        $subcounties = SubCounty::all();

        $user = new User;

        $user->person_id = $pid;
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('phone_no'));
        $user->user_level = 2;
        $user->status = 'Active';
        $user->created_by = Auth::user()->user_id;
        $user->updated_by = Auth::user()->user_id;
        $user->created_at = $date;
        $user->updated_at = $date;
        $user->save();
        
        $SA_persons = SA_PERSONS::with('person.sub_county.county')->with('sas')->with('creator')->get();
        $data = array(
            'sa_persons' => $SA_persons,
            'counties' => $counties,
            'subcounties' => $subcounties,
        );
        return response(['status' => 'success', 'details' => $data]);
        
        
    
    } 
    }catch (Exception $e) {
        return response(['status' => 'error']);
        

    }

}
public function delete(){
    
}
public function edit(){
    
}
}

