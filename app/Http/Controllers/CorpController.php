<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\County;
use App\Person;
use App\SubCounty;
use App\Corp;
use App\User;
use Auth;

class CorpController extends Controller
{

    public function corp()
    {
        $counties = County::all();
        $subcounties = SubCounty::all();
        $person = Person::all();
        if(Auth::user()->user_level == 1)
        {
            //admin

            $corps = Corp::with('person.sub_county.county')->where('is_vet', 'No')->get();

        }elseif (Auth::user()->user_level == 2) {
            //super agent
            $corps = Corp::with('person.sub_county.county')->where('is_vet', 'No')->where('person_id', Auth::user()->person_id)->get();
        }
      

        $data = array(
            'counties' => $counties,
            'corps' => $corps,
            'subcounties' => $subcounties,
        );
        // echo json_encode($corps);
        return view('corps')->with($data);

    }
    public function addcorp(Request $request)
    {

        try {

            if ($request->first_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the First name"]);
            }
            if ($request->last_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the Last name"]);
            } else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $pno = $request->input('phone_no');
                $str = substr($pno, 1);

                $fullno = "+254" . $str;

                $person = new Person;
                $person->first_name = $request->input('first_name');
                $person->last_name = $request->input('last_name');
                $person->id_number = $request->input('id_num');
                $person->phone_no = $fullno;
                $person->sub_county_id = $request->input('sub_county_id');
                $person->created_by = Auth::user()->user_id;
                $person->updated_by = Auth::user()->user_id;
                $person->created_at = $date;
                $person->updated_at = $date;
                $person->save();

                $pid = $person->person_id;

                $user = new User;

                $user->person_id = $pid;
                $user->email = $request->input('email');
                $user->password = bcrypt($request->input('phone_no'));
                $user->user_level = $request->input('user_level_id');
                $user->status = 'Active';
                $user->created_by = Auth::user()->user_id;
                $user->updated_by = Auth::user()->user_id;
                $user->created_at = $date;
                $user->updated_at = $date;
                $user->save();

                $counties = County::all();
                $subcounties = SubCounty::all();
                $userlevels = UserLevel::all();

                $users = User::with('person.sub_county.county')->with('creator')->with('level')->get();

                $data = array(
                    'users' => $users,
                    'counties' => $counties,
                    'subcounties' => $subcounties,
                    'userlevels' => $userlevels,
                );
                return response(['status' => 'success', 'details' => $data]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function editcorp(Request $request)
    {
        try {

            if ($request->first_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the First name"]);
            } else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $user = User::find($request->input('id'));
                $user->email = $request->input('email');
                $user->user_level = $request->input('user_level_id');
                $user->status = $request->input('status');
                $user->updated_at = $date;
                $user->updated_by = Auth::user()->user_id;
                $user->save();

                $pid = $user->person_id;

                $pno = $request->input('phone_no');
                $str = substr($pno, 1);
                $fullno = "+254" . $str;

                $person = Person::find($pid);
                $person->first_name = $request->input('first_name');
                $person->last_name = $request->input('last_name');
                $person->id_number = $request->input('id_num');
                $person->phone_no = $fullno;
                $person->sub_county_id = $request->input('sub_county_id');
                $person->updated_at = $date;
                $person->updated_by = Auth::user()->user_id;
                $person->save();

                $counties = County::all();
                $subcounties = SubCounty::all();
                $userlevels = UserLevel::all();

                $users = User::with('person.sub_county.county')->with('creator')->with('level')->get();

                $data = array(
                    'users' => $users,
                    'counties' => $counties,
                    'subcounties' => $subcounties,
                    'userlevels' => $userlevels,
                );
                return response(['status' => 'success', 'details' => $data]);

            }
        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function deletecorp(Request $request)
    {
        try {
            date_default_timezone_set('UTC');
            $date = date('Y-m-d H:i:s', time());

            $user = User::find($request->input('user_id'));
            $user->status = 'Deleted';
            $user->updated_at = $date;
            $user->updated_by = Auth::user()->user_id;
            $user->save();
            $counties = County::all();
            $subcounties = SubCounty::all();
            $userlevels = UserLevel::all();

            $users = User::with('person.sub_county.county')->with('creator')->with('level')->get();

            $data = array(
                'users' => $users,
                'counties' => $counties,
                'subcounties' => $subcounties,
                'userlevels' => $userlevels,
            );
            return response(['status' => 'success', 'details' => $data]);

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }
    }

    public function get_subcounties(Request $request)
    {

        $county_id = $request->county_id;

        $subcounties = SubCounty::where('county_id', $county_id)->get();

        return $subcounties;

    }

}
