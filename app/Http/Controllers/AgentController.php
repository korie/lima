<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Agent;

class AgentController extends Controller
{
    public function agent(){

        $agent = Agent::all();
        // echo(json_encode($agent));

        return view('agent')->with('agent',$agent);

    }

    public function addagent(Request $request){

        try {

            if ($request->sa_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the super agent name"]);
            }else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $agent = new Agent;
                $agent->agent_name = $request->input('agent_name');
                $agent->created_by = Auth::user()->user_id;
                $agent->updated_by = Auth::user()->user_id;
                $agent->created_at = $date;
                $agent->updated_at = $date;
                $agent->save();
                                        
                $agent = Agent::all();
        
                return response(['status' => 'success', 'details' => $agent]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function editagent(){

    }

    public function deleteagent(){

    }
}
