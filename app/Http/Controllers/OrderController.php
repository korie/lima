<?php
	
	namespace App\Http\Controllers;
	
	//use App\County;
	use App\InforCenter;
	use App\Order;
	use App\OrderDetail;
	use App\Person;
	use App\Products;
	use App\Stock;
	use App\Survey;
	use App\User;
	use Auth;
	use Illuminate\Http\Request;
	
	class OrderController extends Controller
	{
		
		public function index()
		{
			$products = Products::all();
			$orders = Order::where('order_to', Auth::user()->person_id)->get();
			$suppliers = User::with('person')->where('user_level', 4)->get();
			//$subcounties = SubCounty::all();
			
			//$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
			
			$data = array(
					'products' => $products,
					'orders' => $orders,
					'suppliers' => $suppliers
			);
			return view('orders')->with($data);
			
		}
		
		public function orderssuppliers()
		{
			$products = Products::all();
			$orders = Order::where('source', Auth::user()->person_id)->get();
			$suppliers = User::with('person')->where('user_level', 4)->get();
			//$subcounties = SubCounty::all();
			
			//$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
			
			$data = array(
					'products' => $products,
					'orders' => $orders,
					'suppliers' => $suppliers
			);
			return view('orderssuppliers')->with($data);
			
		}
		
		public function vieworderdetails($order_id)
		{
			$order_details = OrderDetail::where('order_id', $order_id)->get();
			$order = Order::find($order_id);
			//$order_details = $request->query('order_id');
			// $order_details = Input::get('order_id');
			
			$data = array(
					'order_details' => $order_details,
					'order' => $order
			
			);
			return view('order_detail')->with($data);
			
		}
		
		public function updateorder(Request $request)
		{
			
			try {
				
				
				if ($request->quantity_delivered == '') {
					return response(['status' => 'error', 'details' => "Please enter Quantity"]);
				} else {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					
					$orderdetail = OrderDetail::find($request->input('order_detail_id'));
					$orderdetail->quantity_delivered = $request->input('quantity_delivered');
					$orderdetail->detail_status = 1;
					
					
					$stock->stock_in = $request->input('quantity');
					$stock->stock_out = 0;
					$stock->product_id = $request->input('product_id');
					$stock->person_id = Auth::user()->user_id;
					$stock->created_by = Auth::user()->user_id;
					$stock->created_at = $date;
					$stock->updated_by = Auth::user()->user_id;
					$stock->updated_at = $date;
					$orderdetail->save();
					
					$order_details_balance = ($orderdetail->order->no_order_details - 1);
					
					$orderdetail->order->no_order_details = $order_details_balance;
					$orderdetail->order->save();
					if ($orderdetail->order->no_order_details == 0) {
						$orderdetail->order->status = 1;
						$orderdetail->order->save();
					}
					//get current stock
					
					//add the stock
					$stock = new Stock;
					$stock->stock_code = 110;
					$stock->stock_in = 0;
					$stock->stock_out = $request->input('quantity_delivered');
					$stock->product_id = $orderdetail->product_id;
					$stock->person_id = Auth::user()->person->person_id;
					$stock->created_by = Auth::user()->person->person_id;
					$stock->created_at = $date;
					$stock->updated_by = Auth::user()->person->person_id;
					$stock->updated_at = $date;
					$stock->save();
					///update stock
					
					
					//$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
					$products = Products::all();
					$stocks = Stock::all();
					//$subcounties = SubCounty::all();
					
					//$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
					
					$data = array(
							'products' => $products,
							'stocks' => $stocks
					);
					return response(['status' => 'success', 'details' => $data]);
				}
				
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
			
		}
		
		public function disapproveorder(Request $request)
		{
			
			try {
				
				
				if ($request->comments == '') {
					return response(['status' => 'error', 'details' => "Please enter Comments"]);
				} else {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					
					$orderdetail = OrderDetail::find($request->input('order_detail_id'));
					$orderdetail->quantity_delivered = 0;
					$orderdetail->detail_status = 1;
					$orderdetail->comments = $request->input('comments');
					
					
					// $stock->stock_in = $request->input('quantity');
					// $stock->stock_out = 0;
					// $stock->product_id = $request->input('product_id');
					// $stock->person_id = Auth::user()->user_id;
					// $stock->created_by = Auth::user()->user_id;
					// $stock->created_at = $date;
					// $stock->updated_by = Auth::user()->user_id;
					// $stock->updated_at = $date;
					$orderdetail->save();
					
					$order_details_balance = ($orderdetail->order->no_order_details - 1);
					
					$orderdetail->order->no_order_details = $order_details_balance;
					$orderdetail->order->save();
					if ($orderdetail->order->no_order_details == 0) {
						$orderdetail->order->status = 1;
						$orderdetail->order->save();
					}
					
					
					$products = Products::all();
					$stocks = Stock::all();
					
					
					$data = array(
							'products' => $products,
							'stocks' => $stocks
					);
					return response(['status' => 'success', 'details' => $data]);
				}
				
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
			
		}
		
		public function addMore()
		{
			return view("addMore");
		}
		
		
		public function addsaorder(Request $request)
		{
			// $rules = [];
			
			
			// foreach($request->input('name') as $key => $value) {
			//     $rules["name.{$key}"] = 'required';
			// }
			
			
			// $validator = Validator::make($request->all(), $rules);
			
			
			// if ($validator->passes()) {
			
			
			//create new order
			date_default_timezone_set('UTC');
			$date = date('Y-m-d H:i:s', time());
			
			$order = new Order;
			$order->source = Auth::user()->person_id;
			$order->order_to = $request->input('supplier_id');
			$order->status = 0;
			$order->no_order_details = 0;
			$order->created_at = $date;
			$order->updated_at = $date;
			$order->created_by = Auth::user()->user_id;
			$order->updated_by = Auth::user()->user_id;
			$order->save();
			
			
			$product_id = $request->input('product_id');
			$quantity_ordered = $request->input('quantity_ordered');
			
			
			for ($i = 0; $i < sizeof($product_id); $i++) {
				
				
				$orderdetail = new OrderDetail;
				$orderdetail->order_id = $order->order_id;
				$orderdetail->product_id = $product_id[$i];
				$orderdetail->quantity_ordered = $quantity_ordered[$i];
				$orderdetail->created_at = $date;
				$orderdetail->updated_at = $date;
				$orderdetail->created_by = 1;
				$orderdetail->updated_by = 1;
				$orderdetail->save();
			}
			$order = Order::find($order->order_id);
			$order->no_order_details = $i;
			$order->save();
			
			
			return response()->json(['success' => 'done']);
			/// }
			
			
			// return response()->json(['error'=>$validator->errors()->all()]);
		}
		
		public function edituser(Request $request)
		{
			try {
				
				if ($request->first_name == '') {
					return response(['status' => 'error', 'details' => "Please enter the First name"]);
				} else {
					
					date_default_timezone_set('UTC');
					$date = date('Y-m-d H:i:s', time());
					
					$user = User::find($request->input('id'));
					$user->email = $request->input('email');
					$user->user_level = $request->input('user_level_id');
					$user->status = $request->input('status');
					$user->updated_at = $date;
					$user->updated_by = Auth::user()->user_id;
					$user->save();
					
					$pid = $user->person_id;
					
					$pno = $request->input('phone_no');
					$str = substr($pno, 1);
					$fullno = "+254" . $str;
					
					$person = Person::find($pid);
					$person->first_name = $request->input('first_name');
					$person->last_name = $request->input('last_name');
					$person->id_number = $request->input('id_num');
					$person->phone_no = $fullno;
					$person->sub_county_id = $request->input('sub_county_id');
					$person->updated_at = $date;
					$person->updated_by = Auth::user()->user_id;
					$person->save();
					
					$counties = County::all();
					$subcounties = SubCounty::all();
					$userlevels = UserLevel::all();
					
					$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
					
					$data = array(
							'users' => $users,
							'counties' => $counties,
							'subcounties' => $subcounties,
							'userlevels' => $userlevels,
					);
					return response(['status' => 'success', 'details' => $data]);
					
				}
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
			
		}
		
		public function deleteuser(Request $request)
		{
			try {
				date_default_timezone_set('UTC');
				$date = date('Y-m-d H:i:s', time());
				
				$user = User::find($request->input('user_id'));
				$user->status = 'Deleted';
				$user->updated_at = $date;
				$user->updated_by = Auth::user()->user_id;
				$user->save();
				$counties = County::all();
				$subcounties = SubCounty::all();
				$userlevels = UserLevel::all();
				
				$users = User::with('person.sub_county.county')->with('creator')->with('level')->get();
				
				$data = array(
						'users' => $users,
						'counties' => $counties,
						'subcounties' => $subcounties,
						'userlevels' => $userlevels,
				);
				return response(['status' => 'success', 'details' => $data]);
				
			} catch (Exception $e) {
				return response(['status' => 'error']);
			}
		}
		
		public function get_subcounties(Request $request)
		{
			
			$county_id = $request->county_id;
			
			$subcounties = SubCounty::where('county_id', $county_id)->get();
			
			return $subcounties;
			
		}
		
		
		public function corp_orders(Request $request)
		{
			
			$pid = $request->person_id;
			
			$orders = Order::with('detail.product')->with('client')->where('order_to', $pid)->get();
			
			$myarr = array();
			
			foreach ($orders as $order) {
				array_push($myarr, array(
						"order_id" => $order->order_id,
						"comments" => $order->detail->comments,
						"status" => $order->status,
						"order_date" => $order->created_at,
						"product" => $order->detail->product->product_name,
						"quantity" => $order->detail->quantity_ordered,
						"delivered" => $order->detail->quantity_delivered,
						"farmer_id" => $order->source,
						"farmer_name" => $order->client->first_name . " " . $order->client->last_name,
						"farmer_phone" => $order->client->phone_no)
				);
			}
			return response()->json(['orders' => $myarr]);
			
		}
		
		public function corp_order_status(Request $request)
		{
			
			$pid = $request->person_id;
			$order_id = $request->order_id;
			$status = $request->status;
			$quantity_delivered = $request->quantity_delivered;
			$comments = $request->comments;
			
			$UpdateDetails = Order::find($order_id);
			if ($UpdateDetails == null) {
				
				return response()->json(['messages' => ['Order not found']], 404);
			} else {
				if ($UpdateDetails->order_to == $pid) {
					if ($quantity_delivered > 0) {
						
						date_default_timezone_set('UTC');
						$date = date('Y-m-d H:i:s', time());
						
						Order::where('order_id', $order_id)->where('order_to', $pid)->update(['status' => $status, 'updated_by' => $pid]);
						OrderDetail::where('order_id', $order_id)->update(['quantity_delivered' => $quantity_delivered, 'comments' => $comments, 'updated_at' => $date, 'updated_by' => $pid]);
						
						$orders = Order::with('detail')->with('client')->where('order_to', $pid)->get();
						
						$myarr = array();
						
						foreach ($orders as $order) {
							array_push($myarr, array("order_id" => $order->order_id, "order_id" => $order->order_id, "status" => $order->status, "order_date" => $order->created_at, "product" => $order->detail->product->product_name, "quantity" => $order->detail->quantity_ordered, "delivered" => $order->detail->quantity_delivered, "farmer_id" => $order->source, "farmer_name" => $order->client->first_name . " " . $order->client->last_name, "farmer_phone" => $order->client->phone_no));
						}
						return response()->json(['orders' => $orders]);
						
					} elseif ($status == 3) {
						
						date_default_timezone_set('UTC');
						$date = date('Y-m-d H:i:s', time());
						
						Order::where('order_id', $order_id)->where('order_to', $pid)->update(['status' => $status, 'updated_by' => $pid]);
						OrderDetail::whereColumn('order_id', $order_id)->update(['quantity_delivered' => $quantity_delivered, 'comments' => $comments, 'updated_at' => $date, 'updated_by' => $pid]);
						
						$orders = Order::with('detail')->with('client')->where('order_to', $pid)->get();
						
						$myarr = array();
						
						foreach ($orders as $order) {
							array_push($myarr, array("order_id" => $order->order_id, "order_id" => $order->order_id, "status" => $order->status, "order_date" => $order->created_at, "product" => $order->detail->product->product_name, "quantity" => $order->detail->quantity_ordered, "delivered" => $order->detail->quantity_delivered, "farmer_id" => $order->source, "farmer_name" => $order->client->first_name . " " . $order->client->last_name, "farmer_phone" => $order->client->phone_no));
						}
						return response()->json(['orders' => $myarr]);
						
					} else {
						
						Order::where('order_id', $order_id)->where('order_to', $pid)->update(['status' => $status]);
						
						$orders = Order::with('detail')->with('client')->where('order_to', $pid)->get();
						
						$myarr = array();
						
						foreach ($orders as $order) {
							array_push($myarr, array("order_id" => $order->order_id, "order_id" => $order->order_id, "status" => $order->status, "order_date" => $order->created_at, "product" => $order->detail->product->product_name, "quantity" => $order->detail->quantity_ordered, "delivered" => $order->detail->quantity_delivered, "farmer_id" => $order->source, "farmer_name" => $order->client->first_name . " " . $order->client->last_name, "farmer_phone" => $order->client->phone_no));
						}
						return response()->json(['orders' => $myarr]);
					}
				} else {
					return response()->json(['messages' => ['You do not have an order with this ID']], 404);
				}
			}
			
		}
		
		public function surveys(Request $request)
		{
			
			$date = date('Y-m-d H:i:s', time());
			
			$survey = Survey::where('validity', '>=', $date)->get();
			
			return response(['surveys' => $survey]);
			
		}
		
		public function information_center(Request $request)
		{
			
			$date = date('Y-m-d H:i:s', time());
			
			$infor_centre = InforCenter::where('validity', '>=', $date)->get([
					'infor_name AS name',
					'infor_link AS link',
					'created_at',
					'updated_at',
					'created_by',
					'updated_by AS updated_by',
			]);
			
			return response(['infor_centre' => $infor_centre]);
			
		}
		
	}
