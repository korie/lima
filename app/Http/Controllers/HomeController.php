<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Corp;
use App\Farmer;
use App\Order;
use App\SA_PERSONS;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $no_corps = Corp::count();
        // //$no_corps = $corps;

        $data = array(
            'corps_count' => Corp::all()->where('is_vet', 'No')->count(),
            'farmers_count' => Farmer::count(),
            'sa_persons_count' => SA_PERSONS::count(),
            'orders_count' => Order::count(),
            
        );
        return view('admin_dashboard')->with($data);
       
    }
}
