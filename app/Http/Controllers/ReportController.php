<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application reports.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function report()
    {
        return view('report');
    }
}
