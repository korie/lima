<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Suppliers;
use App\Supplier_persons;
use App\County;
use App\SubCounty;
use App\Person;
use App\User;
use Auth;

class SupplierController extends Controller
{
    public function supplier(){

        $counties = County::all();
        $subcounties = SubCounty::all();
        $suppliers = Suppliers::all();
        // $supplier_persons = Supplier_persons::all();
        $user = User::all();
        $supplier_persons = Supplier_persons::with('person.sub_county.county')->with('suppliers')->with('creator')->get();
        $data = array(
            'supplier_persons' => $supplier_persons,
            'counties' => $counties,
            'subcounties' => $subcounties,
            'suppliers' => $suppliers,
            'user' => $user,
            
        );

        // echo(json_encode($suppliers));
        return view('supplier_person')-> with($data);

    }

    public function addsupplier(Request $request){

        try {
            if ($request ->first_name == ''){
                return response(['status' => 'error', 'details' => "Please enter the first name"]);
            }
            if ($request ->last_name == ''){
                return response(['status' => 'error', 'details' => "Please enter the last name"]);
            } else {
                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());
                
                $person = new Person;
                $person->first_name = $request->input('first_name');
                $person->last_name = $request->input('last_name');
                $person->id_number = $request->input('id_num');
                $person->phone_no = $request->input('phone_no');
                $person->sub_county_id = $request->input('sub_county_id');
                $person->created_by = Auth::user()->user_id;
                $person->updated_by = Auth::user()->user_id;
                $person->created_at = $date;
                $person->updated_at = $date;
                $person->save();
                
                $pid = $person->person_id;
                
                $supplier_person = new Supplier_persons;
                
                $supplier_person->person_id = $pid;
                $supplier_person->supplier_id = $request->input('supplier_id');
                $supplier_person->created_by = Auth::user()->user_id;
                $supplier_person->updated_by = Auth::user()->user_id;
                $supplier_person->created_at = $date;
                $supplier_person->updated_at = $date;
                $supplier_person->save();
                $counties = County::all();
                $subcounties = SubCounty::all();
                $suppliers = Suppliers::all();
        
                $user = new User;
        
                $user->person_id = $pid;
                $user->email = $request->input('email');
                $user->password = bcrypt($request->input('phone_no'));
                $user->user_level = 4;
                $user->status = 'Active';
                $user->created_by = Auth::user()->user_id;
                $user->updated_by = Auth::user()->user_id;
                $user->created_at = $date;
                $user->updated_at = $date;
                $user->save();
                
                $Supplier_persons = Supplier_persons::with('person.sub_county.county')->with('creator')->with('suppliers')->get();
                $data = array(
                    'suppliers_persons' => $Supplier_persons,
                    // 'counties' => $counties,
                    // 'subcounties' => $subcounties,
                    // 'suppliers' => $suppliers,
                );
                return response(['status' => 'success', 'details' => $data]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function addsupplieragent(Request $request){

        try {

            if ($request->supplier_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the supplier name"]);
            }else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());

                $suppliers = new Suppliers;
                $suppliers->supplier_name = $request->input('supplier_name');
                $suppliers->created_by = Auth::user()->user_id;
                $suppliers->updated_by = Auth::user()->user_id;
                $suppliers->created_at = $date;
                $suppliers->updated_at = $date;
                $suppliers->save();
                                        
                $suppliers = Suppliers::all();
        
                return response(['status' => 'success', 'details' => $suppliers]);
            }

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }

    }

    public function editsupplier(){

    }

    public function deletesupplier(){

    }
}
