<?php

namespace App\Http\Controllers;
use App\InforCenter;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InformationCenterController extends Controller
{
    public function informationcenter(){

        $infor = InforCenter::all();
        $data = array(
            'infor' => $infor,
        );

        return view('information')->with($data);

    }

    public function addinfor(Request $request){

        try {
            
            if ($request->infor_name == '') {
                return response(['status' => 'error', 'details' => "Please enter the Article name"]);
            }else {

                date_default_timezone_set('UTC');
                $date = date('Y-m-d H:i:s', time());
                
                
                $infor = new InforCenter;
                $infor->infor_name = $request->input('infor_name');
                $infor->infor_link = $request->input('infor_link');
                $infor->validity = $request->input('validity');                
                $infor->created_by = Auth::user()->person_id;
                $infor->updated_by = Auth::user()->person_id;                
                $infor->created_at = $date;
                $infor->updated_at = $date;
                $infor->save();
                                
            }
            
            $infor = InforCenter::all();
        
            return response(['status' => 'success', 'details' => $infor]);
            

        } catch (Exception $e) {
            return response(['status' => 'error']);
        }
        
    }

    public function editinfor(Request $request){
        
    }
}
