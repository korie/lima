<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send_Log extends Model {

    public $table = 'send_log';
    protected $primaryKey = 'send_log_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'status', 'statusCode', 'messageID', 'created_at', 'cost', 'updated_at',
    ];
}
