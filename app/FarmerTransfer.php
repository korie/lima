<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmerTransfer extends Model
{
    public $table = 'farmer_transfer';
    protected $primaryKey = 'transfer_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'person_id', 'registered_by', 'transfer_reason', 'transfer_date', 
    ];
    
    public function farmer_transfer(){
        return $this->belongsTo('App\Person','person_id','person_id');
    }
    
}
