<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Products extends Model
{
    public $table = 'products';
    protected $primaryKey = 'product_id';
    public $timestamps = false;


    protected $fillable = [
        'product_name', 'product_code', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

    public function person(){
        return $this->belongsTo('App\Person','person_id','person_id');
    }
   
    public function stock(){
        return $this->belongsTo('App\Stock','stock_id','stock_id');
    }
    
    

}
