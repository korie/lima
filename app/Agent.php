<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    public $table = 'sas';
    protected $primaryKey = 'sa_id';
    public $timestamps = false;
    
    protected $fillable = [
        'sa_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
