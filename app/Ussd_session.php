<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ussd_session extends Model
{
    //
    public $table = 'ussd_session';
    protected $primaryKey = 'id';
     protected $fillable = [
        'session_id', 'farmer_id','phone_no','person_id', 'sub_county_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status',
    ];   
  
           
    public function Farmer(){
        return $this->belongsTo('App\Farmer','farmer_id','id');
    }
     public function Person(){
        return $this->belongsTo('App\Person','person_id','person_id');
    }
    
    public function SA_Person(){
        return $this->belongsTo('App\SA_Products','sales_agent_id','person_id');
    }
    
    



}
