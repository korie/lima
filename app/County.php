<?php
	
	namespace App;
	
	use Illuminate\Database\Eloquent\Model;
	
	class County extends Model
	{
		public $table = 'county';
		public $timestamps = false;
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'name', 'time_stamp', 'status',
		];
		
		public function subCounties()
		{
			return $this->hasMany(SubCounty::class);
		}
		
	}
