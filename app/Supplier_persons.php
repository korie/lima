<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_persons extends Model
{
    public $table = 'supplier_persons';
    protected $primaryKey = 'supplier_person_id';
    // use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id', 'person_id', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];


    public function person(){
        return $this->belongsTo('App\Person', 'person_id', 'person_id');
    }
    public function creator(){
        return $this->belongsTo('App\Person','created_by','person_id');
        
    }
    public  function suppliers(){
          return $this->belongsTo('App\Suppliers', 'supplier_id', 'supplier_id' );
      }
}
