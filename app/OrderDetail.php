<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public $table = 'order_details';
    protected $primaryKey = 'order_details_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'product_id', 'quantity_ordered', 'quantity_delivered', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
    
    public function order() {
        return $this->belongsTo('App\Order','order_id','order_id');
    }
	
	public function product(){
		return $this->belongsTo('App\Products','product_id','product_id');
	}
}
