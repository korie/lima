<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VetRequest extends Model
{
    public $table = 'vet_requests';
    protected $primaryKey = 'vet_request_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'source', 'status', 'vet', 'vet_notified', 'created_at', 'updated_at', 'updated_by', 'vet_comments',
    ];

    public function person(){
        return $this->belongsTo('App\Person','person_id','person_id');
    }

    public function farmer(){
        return $this->belongsTo('App\Person','source','person_id');
    }
    public function vet(){
        return $this->belongsTo('App\Person','vet','person_id');
    }
}
