<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Stock extends Authenticatable
{
    public $table = 'stocks';
    protected $primaryKey = 'stock_id';
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stock_id', 'person_id', 'product_id', 'stock_code', 'stock_in','stock_out','created_at', 'created_by', 'updated_at', 'updated_by', 'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function person(){
        return $this->belongsTo('App\Person','person_id','person_id');
    }


    public function creator(){
        return $this->belongsTo('App\Person','created_by','person_id');
    }
    public function products()
    {
    return $this->hasMany('App\Products','product_id','product_id');
    }

    // public function level(){
    //     return $this->belongsTo('App\UserLevel', 'user_level', 'user_level_id');
    // }
}
