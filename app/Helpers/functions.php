<?php

function generateRandomChars($length = 10)
{
    return substr(str_shuffle("ABCDEFGHJKLMNPQRSTUVWXYZ123456789"), 0, $length);
}

function trimSpace($string)
{
    return preg_replace('/\s+/', '', trim($string));
}
	
