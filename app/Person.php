<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

    public $table = 'persons';
    protected $primaryKey = 'person_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'id_no', 'phone_no', 'sub_county_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status',
    ];

    protected function getCreatedAtAttribute($value) {
        return $value;
    }

    public function sub_county() {
        return $this->belongsTo('App\SubCounty', 'sub_county_id', 'id');
    }

    public function orders() {
        return $this->hasMany('App\Order', 'source', 'order_id');
    }

    public function products() {
        return $this->hasMany('App\Products', 'product_id', 'product_id');
    }

    public function userAccount() {
        return $this->hasOne(User::class, 'person_id');
    }

    public function corpAccount() {
        return $this->hasOne(Corp::class, 'person_id');
    }

    public function farmerAccount() {
        return $this->hasOne(Farmer::class, 'person_id');
    }

    public function sa_products() {
        return $this->belongsTo('App\SA_Products', 'person_id', 'sales_agent_id');
    }

}
