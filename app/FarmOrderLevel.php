<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmOrderLevel extends Model
{
    public $table = 'farmorder_level';
    protected $primaryKey = 'level_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_no', 'person_id', 'level', 'created_at', 'updated_at',
    ];

}
