<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    public $table = 'inbox';
    protected $primaryKey = 'inbox_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'source', 'destination', 'content', 'processed', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];


}
