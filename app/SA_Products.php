<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SA_Products extends Model
{
    public $table = 'sales_agent_products';
    protected $primaryKey = 'id';
    public $timestamps = false;


    protected $fillable = [
        'product_id', 'sales_agent_id','available_balance','','buying_price','selling_price', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
    
    public function person(){
        return $this->belongsTo('App\Person','person_id','sales_agent_id');
    }
    public function product(){
        return $this->belongsTo('App\Products','product_id','product_id');
    }

    
    
    

}
