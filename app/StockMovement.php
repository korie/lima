<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockMovement extends Model
{
    public $table = 'stock_movement';
    protected $primaryKey = 'stock_movement_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id', 'receiver_id', 'balance', 'product_id', 'quantity_sold','quantity_available','created_at',
    ];

    public function seller(){
        return $this->belongsTo('App\Person','seller_id','person_id');
    }


    public function receiver(){
        return $this->belongsTo('App\Person','receiver_id','person_id');
    }
}
