<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    public $table = 'suppliers';
    protected $primaryKey = 'supplier_id';
    // use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_name', 'created_at', 'created_by', 'updated_at', 'updated_by',
    ];
}
