<?php
	
	namespace App;
	
	use Illuminate\Database\Eloquent\Model;
	
	class Farmer extends Model
	{
		public $table = 'farmers';
		protected $primaryKey = 'farmer_id';
		
		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
				'farmer_id', 'person_id', 'location', 'no_of_livestok', 'corp_id', 'created_at', 'created_by', 'updated_at', 'updated_by',
		];
		
		protected function getCreatedAtAttribute($value)
		{
			return $value;
		}
		
		public function person()
		{
			return $this->belongsTo('App\Person', 'person_id', 'person_id');
		}
		
		public function corp()
		{
			return $this->belongsTo('App\Corp', 'corp_id', 'corp_id');
		}
		
		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
				'password', 'remember_token',
		];
	}
