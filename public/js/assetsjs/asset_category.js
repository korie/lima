
//enabling the tooltip fctionality
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   

		$(function	()	{
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
			
			});
	


	
//function executed on submit clicked on modal
	$("#btn-submit").click(function(e){

		console.log("submit clciked");
		$("#modalsuccess").html("");
		$("#modalerror").html("");
		var categoryName=$("#catname").val();
		var categoryStatus=$("#status").val();


		if(isEmpty(categoryName)){


			$("#modalsuccess").html("");
			$("#modalerror").html("category name is required");
		}
		else if(isEmpty(categoryStatus)){

			$("#modalsuccess").html("");
			$("#modalerror").html("category status is required");

		}
		
		else{


		e.preventDefault(); 

		        var token = '{{ Session::token() }}';

		        console.log(token);

		        var formData = {
		            category:$("#catname").val(),
		            _token:token,
		            status:$("#status").val()
		        }

		          $.ajax({

		            type: "POST",
		            url: "/assets/submit_assetCategory",
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		                console.log(data);

		               console.log(data["success"]);
		               console.log(data["errors"]);
		                
		                if(isEmpty(data["errors"])){

		                	$("#modalerror").html("");
		                }
		                else{

		                	$("#modalerror").html(data["errors"]);
		                	$("#modalsuccess").html("");
		                }

		                if(isEmpty(data["success"])){
		                	$("#modalsuccess").html("");
		                }
		                else{
		                	
		                	$("#modalsuccess").html(data["success"]);
		                	$("#modalerror").html("");
		                	this.clearFields();
		                }
		                
		                          

		                
		            },
		            error: function (data) {
		                console.log('Error:'+data);
		            }
		        });

		}
        




	});

//end function


function clearFields(){

		$("#catname").val("");
		$("#status").val("");


		console.log("clearing fields");
	}


function isEmpty(val){

    return (val === undefined || val == null || val.length <= 0) ? true : false;
}


});
