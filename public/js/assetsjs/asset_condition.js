
//enabling the tooltip fctionality
$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip(); 
    loadAssetsConditionDataTable();
    getAssetCondition();
    // $('.mystatus').select2();
    showEditModal();
    deleteData();

}); 

function showEditModal(){

	$(document).on('click','.editmodaltrigger',function(){
        
         var theid=$(this).attr("id");
         
         var res=alasql("select * from assetcond where condid=?",[theid]);
             
            for (var t = 0; t < res.length; t++) {

                var econdid = res[t].condid;
                var econdname = res[t].condname;
                var econddesc = res[t].conddesc;
                var ecreatedby = res[t].createdby;
                var edatecr = res[t].datecr;
                var edatem = res[t].datem;
                var emodby = res[t].modby;
                var estatus = res[t].status;
                console.log(econdid);
                $("#edcondid").val(econdid);
                $("#edcondname").val(econdname);
                $("#edconddesc").val(econddesc);
                $("#edcreatedby").val(ecreatedby);
                $("#eddatecreated").val(edatecr);
                $("#eddatemod").val(edatem);
                $("#edmodby").val(emodby);
                $("#edstatus").val(estatus);




            }

	});


} 

function deleteData(){



	$(document).on('click','.deletedata',function(){
        
         var theid=$(this).attr("id");
         console.log("the passed id is "+theid);
         

         //function delete

         swal({
        title: "Are you sure?",
        text: "You will not be able to recover the deleted record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false 
    },
    function(isConfirm) {
        if (isConfirm) {
        	
        	//deleting function here


        	var token = '{{ Session::token() }}';

		        console.log(token);

		        var formData = {

		            _token:token,
		            id:theid
		        }

				 showSweetProgress("deleting asset...","Hold on..")

		          $.ajax({

		            type: "POST",
		            url: "/assets/delete_assetCondition",
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		                console.log(data);
		                getAssetCondition();
		                dismissSweetProgress("");
		                // swal("Deleted!", "The specified record has been deleted.", "success");
		                swal({
							        title: "SUCCESS.",
							        text: "Your record was deleted successfully",
							        type: "success",
							        showCancelButton: false,
							        confirmButtonColor: "#DD6B55",
							        confirmButtonText: "Ok",
							        cancelButtonText: "No, cancel!",
							        closeOnConfirm: true,
							        closeOnCancel: false ,
							        
							    },
							    function(isConfirm) {
							        if (isConfirm) {
							        }

							});
		                	                
		            },
		            error: function (data) {
		                console.log('Error:'+data);
		            }
		        });

		          
        	//deleting function here

            
             


        } else {
            swal("Cancelled", "Your Record was not deleted:)", "error");
        }
    }
);

         // function delete
         
    });

}

	
//function executed on submit clicked on modal
	$("#btn-submit").click(function(e){

		console.log("submit clciked");
		$("#modalsuccess").html("");
		$("#modalerror").html("");
		var conditionName=$("#condname").val();
		var conditionDesc=$("#conddesc").val();
		var conditionStatus=$("#status").val();


		if(isEmpty(conditionName)){


			$("#modalsuccess").html("");
			$("#modalerror").html("condition name is required");
		}

		else if(isEmpty(conditionDesc)){

			$("#modalsuccess").html("");
			$("#modalerror").html("condition dscription is required");

		}
		else if(isEmpty(conditionStatus)){

			$("#modalsuccess").html("");
			$("#modalerror").html("condition status is required");

		}
		
		else{


		e.preventDefault(); 

		        var token = '{{ Session::token() }}';

		        console.log(token);

		        var formData = {
		            conditionname:$("#condname").val(),
		            conditiondesc:$("#conddesc").val(),
		            _token:token,
		            status:$("#status").val()
		        }

				 showSweetProgress("adding condition..","Hold on..")

		          $.ajax({

		            type: "POST",
		            url: "/assets/submit_assetCondition",
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		                console.log(data);

		               console.log(data["success"]);
		               console.log(data["errors"]);
		                
		                if(isEmpty(data["errors"])){

		                	$("#modalerror").html("");
		                }
		                else{

		                	$("#modalerror").html(data["errors"]);
		                	$("#modalsuccess").html("");
		                	dismissSweetProgress("error...");
		                }

		                if(isEmpty(data["success"])){
		                	$("#modalsuccess").html("");
		                	dismissSweetProgress("error...");
		                }
		                else{
		                	
		                	$("#modalsuccess").html(data["success"]);
		                	$("#modalerror").html("");
		                	clearFields();
		                	getAssetCondition();
		                	dismissSweetProgress("done..");
		                	// showPopupMessage("success","success");
				              
		                }
		                
		                          

		                
		            },
		            error: function (data) {
		                console.log('Error:'+data);
		            }
		        });

		}
        




	});

//end function



// function to update edited 

//function executed on submit clicked on modal
	$("#btn-update").click(function(e){

		console.log("update clciked");
		$("#editmodalsuccess").html("");
		$("#editmodalerror").html("");

		var conditionid=$("#edcondid").val();
		var conditionname=$("#edcondname").val();
		var conditiondesc=$("#edconddesc").val();
		var createdby=$("#edcreatedby").val();
		var datecreated=$("#eddatecreated").val();
		var datemod=$("#eddatemod").val();
		var modby=$("#edmodby").val();
		var status=$("#edstatus").val();
		


		if(isEmpty(conditionid)){


			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("condition id is required");
		}
		else if(isEmpty(conditionname)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("condition name is required");

		}
		else if(isEmpty(conditiondesc)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("condition description is required");

		}

		else if(isEmpty(createdby)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("created by is required");

		}

		else if(isEmpty(datecreated)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("date created is required");

		}

		else if(isEmpty(datemod)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("date modified is required");

		}

		else if(isEmpty(modby)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("modified by is required");

		}

		else if(isEmpty(status)){

			$("#editmodalsuccess").html("");
			$("#editmodalerror").html("status is required");

		}
		
		else{


		e.preventDefault(); 

		        var token = '{{ Session::token() }}';

		        console.log(token);

		        var formData = {		           
		            _token:token,
		            id:conditionid,
		            acondname:conditionname,
		            aconddesc:conditiondesc,
                    dcreated:datecreated,
                    createdby:createdby,
                    dmodified:datemod,
                    status:status,
                    modby:modby
		        }

				 showSweetProgress("updating condition...","Hold on..")

		          $.ajax({

		            type: "POST",
		            url: "/assets/update_edit_assetCondition",
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		                console.log(data);

		               	                
		                if((data["update"]=="success")){

		                	$("#editmodalerror").html("");
		                	$("#editmodalsuccess").html("success updating asset condition");
		                	// clearFields();
		                	getAssetCondition();
		                	dismissSweetProgress("done..");
		                	// $('#editformModal').modal('toggle'); 
		                }
		                else{

		                	$("#modalerror").html("error updating asset condition");
		                	$("#modalsuccess").html("");
		                }

		               		                          

		                
		            },
		            error: function (data) {
		                console.log('Error:'+data);
		            }
		        });

		}
        




	});
// function to update edited data

function clearFields(){

		$("#condname").val("");
		$("#conddesc").val("");
		$("#status").val("");


		console.log("clearing fields");
	}

function cleareditFieldsClose(){

		$("#condname").val("");
		$("#conddesc").val("");
		$("#status").val("");

		$("#editmodalsuccess").html("");
		$("#editmodalerror").html("");

		$("#edcondid").val("");
		$("#edcondname").val("");
		$("#edconddesc").val("");
		$("#edcreatedby").val("");
		$("#eddatecreated").val("");
		$("#eddatemod").val("");
		$("#edmodby").val("");
		$("#edstatus").val("");	
	}	

function clearFieldsClose(){

		$("#condname").val("");
		$("#conddesc").val("");
		$("#status").val("");
		$("#modalerror").html("");
		 $("#modalsuccess").html("");


		// console.log("clearing fields");
	}

function isEmpty(val){

    return (val === undefined || val == null || val.length <= 0) ? true : false;

}

function showSweetProgress(mytitle,mymessage){

	window.swal({
	              title: mytitle,
	              text: mymessage,				              
	              showConfirmButton: false,
	              allowOutsideClick: false
	            });


}

function dismissSweetProgress(mytitle){

	window.swal({
                  title: mytitle,
                  showConfirmButton: false,
                  timer: 2000
                });


}


function showPopupMessage(mytitle,mymessage){

	$.gritter.add({
					title: mytitle,
					text:  mymessage,
					sticky: false,
					time: '',
					class_name: 'gritter-success'
				});
}

// datatable function


function loadAssetsConditionDataTable() {

    $("#dataTable").DataTable();

	
}

function getAssetCondition() {
    var trData = "";

    var token = '{{ Session::token() }}';

    console.log(token);

    var formData = {

        _token:token

    }

    // populate data into our local alasql database for easy access
    alasql("DROP TABLE IF EXISTS assetcond");
    alasql("CREATE TABLE assetcond (condid text, condname text,conddesc text,createdby text,datecr text,datem text,modby text,status text)");
    // populate data into our local alasql database for easy access

    $.ajax({
        url: "/assets/get_assetCondition",
        type: "POST",
        data: formData,
        success: function (data) {

            console.log("asset condition");
            console.log(data);

            var table = $('#dataTable').DataTable( );
            table.clear().draw(false);
            for (var x = 0; x < data.length; x++) {

               var condid = data[x].asset_condition_id;
                var condname = data[x].asset_condition_name;
                var conddesc = data[x].asset_condition_description;
                var createdby = data[x].created_by;
                var datecr= data[x].date_created;
                var datem = data[x].date_modified;
                var modby = data[x].modified_by;
                var st = data[x].status;

// populate data into our local alasql database for easy access
                alasql("INSERT INTO assetcond VALUES ('"+condid+"','"+condname+"','"+conddesc+"','"+createdby+"','"+datecr+"','"+datem+"','"+modby+"','"+st+"')");
// populate data into our local alasql database for easy access

                

                table.row.add($(
                            '<tr><td id="condid">'
                            + condid +
                            '</td><td>'
                            + condname +
                            '</td><td>'
                             + conddesc +
                            '</td><td>'
                            + createdby +
                            '</td><td>'
                            + datecr +
                             '</td><td>'
                            + datem +
                             '</td><td>'
                            + modby +
                             '</td><td>'
                            + st +
                            '</td><td ><a href="#editformModal" data-toggle="modal" id=\'' + condid + '\' data-backdrop="static" data-keyboard="false" class="fa-items editmodaltrigger"><span class="label label-success fa-row"><i class="fa fa-pencil" ></i></span></a><a href="#" id=\'' + condid + '\' class="deletedata"><span class="label label-danger fa-row"><i class="fa fa-trash-o"></i></span></a></td></tr>'
                            )).draw(false);

                
            }
        },
        error: function () {
            console.log("error");

        }
    });
}

//datatable function
