$(document).ready(function() {
    var table = $('#dataTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    }); 
} );

$('#addSupplierModal').on('hidden.bs.modal', function (e) {
    $(this).find('#addsupplierform')[0].reset();
});

$('#editSupplierModal').on('hidden.bs.modal', function (e) {
    $(this).find('#editsupplierform')[0].reset();
});

$('#supplier_county').change(function () {

    $("#supplier_sub_county").empty();

    var x = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/get_subcounties',
        data: {
            "county_id": x
        },
        dataType: "json",
        success: function (data) {

            for (var i = 0; i < data.length; i++) {
                var select = document.getElementById("supplier_sub_county"),
                    opt = document.createElement("option");

                opt.value = data[i].id;
                opt.textContent = data[i].name;
                select.appendChild(opt);
            }
        }
    })
});

$('#addsupplierbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addsupplierform").attr("action"),
    data: $("#addsupplierform").serialize(),
    dataType: "json",
    beforeSend: function() {

    btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
         btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Suppier'); 
       if(data.status=='error'){
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  data.details,
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
       }else{
            $.gritter.add({
            title: "<strong>Success!</strong>",
            text:  'Supplier added successfully.',
            sticky: false,
            time: '',
            class_name: 'gritter-success'
        });

            console.log(data);
            console.log(data.details);
          $('#addSupplierModal').modal('toggle');
          $('#totalspan').html(data.details.suppliers_persons.length+' Supplier(s)');
          
          var oTableToUpdate =  $('#dataTable').dataTable( { bRetrieve : true } );
         oTableToUpdate .fnDraw();

        //  $('#dataTable').DataTable({
        //     dom: 'Bfrtip',
        //     buttons: [
        //         'copy', 'excel', 'pdf'
        //     ]
        // });

        $('#dataTable').dataTable().fnClearTable();

                        var i=1;
                        $.each(data.details.suppliers_persons, function(key, value) {
                           
                           
                             $('#dataTable').dataTable().fnAddData([
                  
                              ''!=null ? i : "",
                              ''!=null ? value.person.first_name + ' '+ value.person.last_name  : "",
                              ''!=null ? value.person.id_number : "",
                              ''!=null ? value.person.phone_no : "",
                              ''!=null ? value.person.sub_county.county.name : "",
                              ''!=null ? value.person.sub_county.name : "",
                              ''!=null ? value.suppliers.supplier_name : "",
                              ''!=null ? value.created_at : "",
                              ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
                              '<a onclick="return editsupplier(' + value.supplier_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + value.person.phone_no + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.supplier_name + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                              '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'

                            ]);
                            i++;
                        }); 

            }             
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Supplier');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});

$('#addsupplieragentbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addsupplieragentform").attr("action"),
    data: $("#addsupplieragentform").serialize(),
    dataType: "json",
    beforeSend: function() {

    btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
         btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Suppier Agent'); 
       if(data.status=='error'){
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  data.details,
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
       }else{
            $.gritter.add({
                title: "<strong>Success!</strong>",
                text:  'Supplier Agent added successfully.',
                sticky: false,
                time: '',
                class_name: 'gritter-success'
            });
          $('#addSupplierAgentModal').modal('toggle');
          $('#totalspan').html(data.details.length+' Supplier Agent(s)');
          
        //   var oTableToUpdate =  $('#dataTable').dataTable( { bRetrieve : true } );
        //     oTableToUpdate .fnDraw();
        //     $('#dataTable').dataTable().fnClearTable();

        //         var i=1;
        //         $.each(data.details, function(key, value) {
                           
                           
        //         $('#dataTable').dataTable().fnAddData([
                  
        //                       ''!=null ? i : "",
        //                       ''!=null ? value.person.first_name + ' '+ value.person.last_name  : "",
        //                       ''!=null ? value.person.id_number : "",
        //                       ''!=null ? value.person.phone_no : "",
        //                       ''!=null ? value.person.sub_county.county.name : "",
        //                       ''!=null ? value.person.sub_county.name : "",
        //                       ''!=null ? value.person.supplier_name : "",
        //                       ''!=null ? value.created_at : "",
        //                       ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
        //                       '<a onclick="return editsupplier(' + value.supplier_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + value.person.phone_no + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.supplier_name + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
        //                       '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'

        //                     ]);
        //                     i++;
        //                 }); 

            }             
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Supplier Agent');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});

$('#editsupplierbtn').click(function (e) {

    var btn = $(this);

    var form = $("#editsupplierform");

    $.ajax({
        type: "POST",
        url: $("#editsupplierform").attr("action"),
        data: $("#editsupplierform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html(' <i class="fa fa-spinner fa-spin"></i> Editing');
        },
        cache: false,
        success: function (data) {
            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Supplier');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong style='color:#3ec291'>Success!</strong>",
                    text: 'Supplier edited successfully .',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });

                $('#editsupplierModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' Suppliers(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.suppliers, function (key, value) {

                    var p = "" + value.person.phone_no;

                    var newno = p.substring(4);
                
                    var pno = "0" + newno;

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? pno : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editSupplier(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + pno + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

                $('#editSupplierModal').modal('hide');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Supplier');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$(document).on("click", ".btndeletesupplier", function () {
    var id = $(this).data('supplier');
    $(".btndeletesupplier-confirm").attr('data-supplier_id', id);
});


$(".btndeletesupplier-confirm").click(function () {

    var id = $(".btndeletesupplier-confirm").attr('data-supplier_id');

    var btn = $(".btndeletesupplier[data-supplier=" + id + "]");

    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: server + "/deletesupplier",
        data: {"supplier_id": id},
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'Supplier deleted successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#totalspan').html(data.details.users.length + ' Supplier(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.suppliers, function (key, value) {

                    var p = "" + value.person.phone_no;

                    var newno = p.substring(4);
                
                    var pno = "0" + newno;

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? pno : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editSupplier(' + value.supplier_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + pno+ '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

});