$('#addSa_personModal').on('hidden.bs.modal', function(e){
    $(this).find('#add_sa_personform')[0].reset();           
});


$('#addSAbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#add_sa_personform").attr("action"),
    data: $("#add_sa_personform").serialize(),
    dataType: "json",
    beforeSend: function() {

    btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
         btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add SaPerson'); 
       if(data.status=='error'){
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  data.details,
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
       }else{
            $.gritter.add({
            title: "<strong>Success!</strong>",
            text:  'Sa_Person added successfully.',
            sticky: false,
            time: '',
            class_name: 'gritter-success'
        });
          $('#addSa_personModal').modal('toggle');
          $('#totalspan').html(data.details.length+' User(s)');
          
          var oTableToUpdate =  $('#dataTable').dataTable( { bRetrieve : true } );
         oTableToUpdate .fnDraw();
        $('#dataTable').dataTable().fnClearTable();

                        var i=1;
                        $.each(data.details, function(key, value) {
                           
                           
                             $('#dataTable').dataTable().fnAddData([
                  
                              ''!=null ? i : "",
                              ''!=null ? value.person.first_name + ' '+ value.person.last_name  : "",
                              ''!=null ? value.person.id_number : "",
                              //''!=null ? value.person.sas : "",
                              ''!=null ? value.person.phone_no : "",
                              ''!=null ? value.person.sub_county.county.name : "",
                              ''!=null ? value.person.sub_county.name : "",
                              ''!=null ? value.email : "",
                              ''!=null ? value.created_at : "",
                              ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
                              '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                                    ]);
                            i++;
                        }); 

            }             
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add SaPerson');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});


