$('#addSaOrderModal').on('hidden.bs.modal', function (e) {
    $(this).find('#addsaorder')[0].reset();
});
$('#editUserModal').on('hidden.bs.modal', function (e) {
    $(this).find('#edituserform')[0].reset();
});

//dynamic field
var i=1;  


$('#add').click(function(){  
     i++;  
     $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><select name="product_id[]" id="product_id'+i+'"  class = "form-control input-sm category" ></td><td><input type="text" name="quantity_ordered[]" placeholder="Quantity" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
     
     get_products("#product_id"+i);

});  


function get_products(product_id) {
    $.ajax({
        url: "/getproducts",
        type: "GET",
        dataType: "JSON",
        success: function (data) {
                console.log(data);
            for (var x = 0; x < data.length; x++) {


                var option = "<option value='"+data[x].product_id + "'>" + data[x].product_name + "</option>";
                $(product_id).append(option);


            }

        }, error: function (data) {

            swal(
                    'Oops...',
                    'Something went wrong!',
                    'error'
                    );
        }
    });
}


$(document).on('click', '.btn_remove', function(){  
     var button_id = $(this).attr("id");   
     $('#row'+button_id+'').remove();  
});  


$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#submit').click(function(){            
    $.ajax({  
         url:"/addsaorder",  
         method:"POST",  
         data:$('#add_order').serialize(),
         type:'json',
         success:function(data)  
         {
             if(data.error){
                 printErrorMsg(data.error);
             }else{
                 i=1;
                 $('.dynamic-added').remove();
                 $('#add_order')[0].reset();
                 $(".print-success-msg").find("ul").html('');
                 $(".print-success-msg").css('display','block');
                 $(".print-error-msg").css('display','none');
                 $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
             }
         }  
    });  
});  


function printErrorMsg (msg) {
  $(".print-error-msg").find("ul").html('');
  $(".print-error-msg").css('display','block');
  $(".print-success-msg").css('display','none');
  $.each( msg, function( key, value ) {
     $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
  });
}

$('#adduserbtn').click(function (e) {

    var btn = $(this);

    $.ajax({
        type: "POST",
        url: $("#add_order").attr("action"),
        data: $("#add_order").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add USer');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'User added successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#addSaOrderModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' User(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.users, function (key, value) {

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? value.person.phone_no : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editUser(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + value.person.phone_no + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add User');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});






function approveorder(order_details_id, product_name, quantity_ordered) {
 console.log("product_name"+quantity_ordered);
 $('#order_detail_id').val(order_details_id);
 $('#product_id').val(product_name);
 $('#quantity_ordered').val(quantity_ordered);
    // document.getElementById("order_detail_id").value = order_details_id;
    // document.getElementById("product_id").value = product_id;
    // document.getElementById("quantity_ordered").value = quantity_ordered;
   // document.getElementById("idno").value = idno;

   
   // document.getElementById("phoneno").value = phoneno;

    //document.getElementById("editemail").value = email;
    //document.getElementById("editstatus").value = status;
    // $("#editcounty").val(countyid).change();
    // $("#editsubcounty").val(subcountyid).change();
    // $("#editlevel").val(level).change();

    $('#orderDetailsModal').modal('show');
}
function disapproveorder(order_details_id) {
    // console.log(order_details_id);
    $('#order_detail_id1').val(order_details_id);
    // $('#product_id').val(product_name);
    // $('#quantity_ordered').val(quantity_ordered);
       // document.getElementById("order_detail_id").value = order_details_id;
       // document.getElementById("product_id").value = product_id;
       // document.getElementById("quantity_ordered").value = quantity_ordered;
      // document.getElementById("idno").value = idno;
   
      
      // document.getElementById("phoneno").value = phoneno;
   
       //document.getElementById("editemail").value = email;
       //document.getElementById("editstatus").value = status;
       // $("#editcounty").val(countyid).change();
       // $("#editsubcounty").val(subcountyid).change();
       // $("#editlevel").val(level).change();
   
       $('#disapproveCommentsModal').modal('show');
   }
function vieworderdetails(order_id){

    

    $.ajax({
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
      url:"/vieworderdetails",
      //url: '',
      method:"POST",
      dataType: "json",
      data:{order_id:order_id},
      success:function(data)
      {
      //alert("data");
      
      }
    });
    
    
  }

