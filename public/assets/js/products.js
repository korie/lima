$('#addProductModal').on('hidden.bs.modal', function(e){
    $(this).find('#addproductform')[0].reset();           
});


$('#addproductbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addproductform").attr("action"),
    data: $("#addproductform").serialize(),
    dataType: "json",
    beforeSend: function() {
        btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
        btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Products'); 
        if(data.status=='error'){
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text:  data.details,
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }else{
            $.gritter.add({
                title: "<strong>Success!</strong>",
                text:  'Product added successfully.',
                sticky: false,
                time: '',
                class_name: 'gritter-success'
            });
            $('#addProductModal').modal('toggle');
            $('#totalspan').html(data.details.length+' Product(s)');
          
            var oTableToUpdate = $('#products_dataTable').dataTable({
                bRetrieve: true
            });
            oTableToUpdate.fnDraw();
            $('#products_dataTable').dataTable().fnClearTable();

            var i=1;
            $.each(data.details, function(key, value) {
                                                      
                $('#products_dataTable').dataTable().fnAddData([
                  
                    ''!=null ? i : "",
                    ''!=null ? value.product.product_name : "",
                    ''!=null ? value.product.product_code : "",
                    ''!=null ? value.product.created_at : "",
                    '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                ]);
                i++;
            }); 

        }             
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Product');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});

$('#addproductbtnsupplier').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addproductssupplierform").attr("action"),
    data: $("#addproductssupplierform").serialize(),
    dataType: "json",
    beforeSend: function() {
        btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
        btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Products'); 
        if(data.status=='error'){
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text:  data.details,
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }else{
            $.gritter.add({
                title: "<strong>Success!</strong>",
                text:  'Product added successfully.',
                sticky: false,
                time: '',
                class_name: 'gritter-success'
            });
            $('#addProductModal').modal('toggle');
            $('#totalspan').html(data.details.length+' Product(s)');
          
            var oTableToUpdate = $('#products_dataTable').dataTable({
                bRetrieve: true
            });
            oTableToUpdate.fnDraw();
            $('#products_dataTable').dataTable().fnClearTable();

            var i=1;
            $.each(data.details, function(key, value) {
                                                      
                $('#products_dataTable').dataTable().fnAddData([
                  
                    ''!=null ? i : "",
                    ''!=null ? value.products.product_name : "",
                    ''!=null ? value.products.product_code : "",
                    ''!=null ? value.products.created_at : "",
                    '<a onclick="return editProduct(' + value.product_id + ',\'' + value.product_name + '\',\'' + value.product_code + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                    '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                ]);
                i++;
            }); 

        }             
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Product');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});