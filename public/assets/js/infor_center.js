$('#addInforModal').on('hidden.bs.modal', function(e){
    $(this).find('#addinforform')[0].reset();           
});


$('#addinforbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addinforform").attr("action"),
    data: $("#addinforform").serialize(),
    dataType: "json",
    beforeSend: function() {
        btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
        btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Information'); 
        if(data.status=='error'){
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text:  data.details,
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }else{
            $.gritter.add({
                title: "<strong>Success!</strong>",
                text:  'Information added successfully.',
                sticky: false,
                time: '',
                class_name: 'gritter-success'
            });
            $('#addInforModal').modal('toggle');
            $('#totalspan').html(data.details.length+' Infor(s)');
          
            var oTableToUpdate = $('#infor_dataTable').dataTable({
                bRetrieve: true
            });
            oTableToUpdate.fnDraw();
            $('#infor_dataTable').dataTable().fnClearTable();

            var i=1;
            $.each(data.details, function(key, value) {
                                                      
                $('#infor_dataTable').dataTable().fnAddData([
                  
                    ''!=null ? i : "",
                    ''!=null ? value.infor_name : "",
                    ''!=null ? value.infor_link : "",
                    ''!=null ? value.validity : "",
                    ''!=null ? value.created_at : "",
                    '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                ]);
                i++;
            }); 

        }             
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Infor');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});
