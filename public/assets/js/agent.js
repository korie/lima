$('#addAgentModal').on('hidden.bs.modal', function (e) {
    $(this).find('#addagentform')[0].reset();
});
$('#editAgentModal').on('hidden.bs.modal', function (e) {
    $(this).find('#editagentform')[0].reset();
});


$('#addagentbtn').click(function (e) {

    var btn = $(this);

    $.ajax({
        type: "POST",
        url: $("#addagentform").attr("action"),
        data: $("#addagentform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Agent');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'Agent added successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#addUserModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' Agent(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.agent, function (key, value) {

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.agent.agent_name : "",
                        '' != null ? value.created_at : "",
                        '<a onclick="return editAgent(' + value.sa_id + ',\'' + value.agent.agent_name + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Agent');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

function editAgent(sa_id, agentname) {
console.log(user_id);
    document.getElementById("sa_id").value = sa_id;
    document.getElementById("agent_name").value = agentname;

    $('#editAgentModal').modal('show');
}

$('#editagentbtn').click(function (e) {

    var btn = $(this);

    var form = $("#editagentform");

    $.ajax({
        type: "POST",
        url: $("#editagentform").attr("action"),
        data: $("#editagentform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html(' <i class="fa fa-spinner fa-spin"></i> Editing');
        },
        cache: false,
        success: function (data) {
            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Agent');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong style='color:#3ec291'>Success!</strong>",
                    text: 'Agent edited successfully .',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });

                $('#editAgentModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' Agent(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.agent, function (key, value) {                    

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.agent.agent_name : "",
                        '' != null ? value.created_at : "",
                        '<a onclick="return editAgent(' + value.sa_id + ',\'' + value.agent.agent_name + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

                $('#editAgentModal').modal('hide');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Agent');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$(document).on("click", ".btndeleteagent", function () {
    var id = $(this).data('agent');
    $(".btndeleteagent-confirm").attr('data-sa_id', id);
});


$(".btndeleteagent-confirm").click(function () {

    var id = $(".btndeleteagent-confirm").attr('data-sa_id');

    var btn = $(".btndeleteagent[data-agent=" + id + "]");
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: server + "/deleteagent",
        data: {"sa_id": id},
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'Agent deleted successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#totalspan').html(data.details.users.length + ' Agent(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.agent, function (key, value) {

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.agent.agent_name : "",
                        '' != null ? value.created_at : "",
                        '<a onclick="return editAgent(' + value.sa_id + ',\'' + value.agent.agent_name + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

});