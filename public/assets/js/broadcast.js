$('#addBroadcastModal').on('hidden.bs.modal', function (e) {
    $(this).find('#addbroadcastform')[0].reset();
});

$('#editBroadcastModal').on('hidden.bs.modal', function (e) {
    $(this).find('#editbroadcastform')[0].reset();
});

$('#addbroadcastbtn').click(function(e) {

    var btn = $(this);

    $.ajax({
    type: "POST",
    url:$("#addbroadcastform").attr("action"),
    data: $("#addbroadcastform").serialize(),
    dataType: "json",
    beforeSend: function() {

    btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');        
    },
    cache: false,
    success: function(data) {
        
         btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add broadcast'); 
       if(data.status=='error'){
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  data.details,
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
       }else{
            $.gritter.add({
            title: "<strong>Success!</strong>",
            text:  'Broadcast added successfully.',
            sticky: false,
            time: '',
            class_name: 'gritter-success'
        });
          $('#addBroadcastModal').modal('toggle');
          $('#totalspan').html(data.details.length+' Broadcast(s)');
          
          var oTableToUpdate =  $('#broadcast_dataTable').dataTable( { bRetrieve : true } );
         oTableToUpdate .fnDraw();
        $('#broadcast_dataTable').dataTable().fnClearTable();

                        var i=1;
                        $.each(data.details, function(key, value) {
                                                      
                             $('#broadcast_dataTable').dataTable().fnAddData([
                  
                              ''!=null ? i : "",
                              ''!=null ? value.broadcast_name : "",
                              ''!=null ? value.broadcast_content : "",
                              ''!=null ? value.created_at : "",
                              ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
                              '<a onclick="return editbroadcast(' + value.broadcast_id + ',\'' + value.broadcast_name + '\',\'' + value.broadcast_content + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                              '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'

                            ]);
                            i++;
                        }); 

            }             
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add Broadcast');
            $.gritter.add({
            title: "<strong>Oops!</strong>",
            text:  "An error occurred.Please try again",
            sticky: false,
            time: '',
            class_name: 'gritter-danger'
        });
            }
        });

e.preventDefault();
});

$('#editbroadcastbtn').click(function (e) {

    var btn = $(this);

    var form = $("#editbroadcastform");

    $.ajax({
        type: "POST",
        url: $("#editbroadcastform").attr("action"),
        data: $("#editbroadcastform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html(' <i class="fa fa-spinner fa-spin"></i> Editing');
        },
        cache: false,
        success: function (data) {
            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Broadcast');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong style='color:#3ec291'>Success!</strong>",
                    text: 'Broadcast edited successfully .',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });

                $('#editBroadcastModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' Broadcast(s)');

                var oTableToUpdate = $('#broadcast_dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#broadcast_dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.broadcast, function (key, value) {

                    $('#broadcast_dataTable').dataTable().fnAddData([

                        ''!=null ? i : "",
                        ''!=null ? value.broadcast_name : "",
                        ''!=null ? value.broadcast_content : "",
                        ''!=null ? value.created_at : "",
                        ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
                        '<a onclick="return editbroadcast(' + value.broadcast_id + ',\'' + value.broadcast_name + '\',\'' + value.broadcast_content + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'

                    ]);
                    i++;
                });

                $('#editBroadcastModal').modal('hide');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Broadcast');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$(document).on("click", ".btndeletebroadcast", function () {
    var id = $(this).data('broadcast');
    $(".btndeletebroadcast-confirm").attr('data-broadcast_id', id);
});

$(".btndeletebroadcast-confirm").click(function () {

    var id = $(".btndeletebroadcast-confirm").attr('data-broadcast_id');

    var btn = $(".btndeletebroadast[data-broadcast=" + id + "]");

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: server + "/deletebroadcast",
        data: {"broadcast_id": id},
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'Broadcast deleted successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#totalspan').html(data.details.broadcast.length + ' Broadcast(s)');

                var oTableToUpdate = $('#broadcast_dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#broadcast_dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.suppliers, function (key, value) {

                    $('#broadcast_dataTable').dataTable().fnAddData([
                  
                        ''!=null ? i : "",
                        ''!=null ? value.broadcast_name : "",
                        ''!=null ? value.broadcast_content : "",
                        ''!=null ? value.created_at : "",
                        ''!=null ? value.creator.first_name + ' '+ value.creator.last_name  : "",
                        '<a onclick="return editbroadcast(' + value.broadcast_id + ',\'' + value.broadcast_name + '\',\'' + value.broadcast_content + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'

                      ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

});