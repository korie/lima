$(document).ready(function() {
    var table = $('#dataTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    }); 
} );


$('#addUserModal').on('hidden.bs.modal', function (e) {
    $(this).find('#adduserform')[0].reset();
});
$('#editUserModal').on('hidden.bs.modal', function (e) {
    $(this).find('#edituserform')[0].reset();
});


$('#adduserbtn').click(function (e) {

    var btn = $(this);

    $.ajax({
        type: "POST",
        url: $("#adduserform").attr("action"),
        data: $("#adduserform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add User');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'User added successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#addUserModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' User(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                // $('#dataTable').DataTable({
                //     dom: 'Bfrtip',
                //     buttons: [
                //         'copy', 'excel', 'pdf'
                //     ]
                // });

                var i = 1;
                $.each(data.details.users, function (key, value) {

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? value.person.phone_no : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editUser(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + value.person.phone_no + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add User');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$('#adduserlevelbtn').click(function (e) {

    var btn = $(this);

    $.ajax({
        type: "POST",
        url: $("#adduserlevelform").attr("action"),
        data: $("#adduserlevelform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Adding');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add User Level');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'User Level added successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#addUserLevelModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' User(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.users, function (key, value) {

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? value.person.phone_no : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editUser(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + value.person.phone_no + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-plus" aria-hidden="true"></i> Add User Level');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$('#county').change(function () {

    $("#sub_county").empty();

    var x = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/get_subcounties',
        data: {
            "county_id": x
        },
        dataType: "json",
        success: function (data) {

            for (var i = 0; i < data.length; i++) {
                var select = document.getElementById("sub_county"),
                    opt = document.createElement("option");

                opt.value = data[i].id;
                opt.textContent = data[i].name;
                select.appendChild(opt);
            }
        }
    })
});


$('#editcounty').change(function () {

    $("#editsubcounty").empty();

    var x = $(this).val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: '/get_subcounties',
        data: {
            "county_id": x
        },
        dataType: "json",
        success: function (data) {

            for (var i = 0; i < data.length; i++) {
                var select = document.getElementById("editsubcounty"),
                    opt = document.createElement("option");

                opt.value = data[i].id;
                opt.textContent = data[i].name;
                select.appendChild(opt);
            }
        }
    })
});

function editUser(user_id, fname, lname, idno, phoneno, countyid, subcountyid, email, status, level) {
console.log(user_id);
    document.getElementById("user_id").value = user_id;
    document.getElementById("firstname").value = fname;
    document.getElementById("lastname").value = lname;
    document.getElementById("idno").value = idno;

   
    document.getElementById("phoneno").value = phoneno;

    document.getElementById("editemail").value = email;
    document.getElementById("editstatus").value = status;
    $("#editcounty").val(countyid).change();
    $("#editsubcounty").val(subcountyid).change();
    $("#editlevel").val(level).change();

    $('#editUserModal').modal('show');
}

$('#edituserbtn').click(function (e) {

    var btn = $(this);

    var form = $("#edituserform");

    $.ajax({
        type: "POST",
        url: $("#edituserform").attr("action"),
        data: $("#edituserform").serialize(),
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html(' <i class="fa fa-spinner fa-spin"></i> Editing');
        },
        cache: false,
        success: function (data) {
            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit User');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong style='color:#3ec291'>Success!</strong>",
                    text: 'User edited successfully .',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });

                $('#editUsersModal').modal('toggle');
                $('#totalspan').html(data.details.users.length + ' Users(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.users, function (key, value) {

                    var p = "" + value.person.phone_no;

                    var newno = p.substring(4);
                
                    var pno = "0" + newno;

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? pno : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editUser(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + pno + '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

                $('#editUserModal').modal('hide');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit User');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

    e.preventDefault();
});

$(document).on("click", ".btndeleteuser", function () {
    var id = $(this).data('user');
    $(".btndeleteuser-confirm").attr('data-user_id', id);
});


$(".btndeleteuser-confirm").click(function () {



    var id = $(".btndeleteuser-confirm").attr('data-user_id');

    var btn = $(".btndeleteuser[data-user=" + id + "]");

    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: server + "/deleteuser",
        data: {"user_id": id},
        dataType: "json",
        beforeSend: function () {

            btn.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        cache: false,
        success: function (data) {

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            if (data.status == 'error') {
                $.gritter.add({
                    title: "<strong>Oops!</strong>",
                    text: data.details,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
            } else {
                $.gritter.add({
                    title: "<strong>Success!</strong>",
                    text: 'User deleted successfully.',
                    sticky: false,
                    time: '',
                    class_name: 'gritter-success'
                });
                $('#totalspan').html(data.details.users.length + ' User(s)');

                var oTableToUpdate = $('#dataTable').dataTable({
                    bRetrieve: true
                });
                oTableToUpdate.fnDraw();
                $('#dataTable').dataTable().fnClearTable();

                var i = 1;
                $.each(data.details.users, function (key, value) {

                    var p = "" + value.person.phone_no;

                    var newno = p.substring(4);
                
                    var pno = "0" + newno;

                    $('#dataTable').dataTable().fnAddData([

                        '' != null ? i : "",
                        '' != null ? value.person.first_name + ' ' + value.person.last_name : "",
                        '' != null ? value.person.id_number : "",
                        '' != null ? pno : "",
                        '' != null ? value.person.sub_county.county.name : "",
                        '' != null ? value.person.sub_county.name : "",
                        '' != null ? value.email : "",
                        '' != null ? value.status : "",
                        '' != null ? value.created_at : "",
                        '' != null ? value.creator.first_name + ' ' + value.creator.last_name : "",
                        '<a onclick="return editUser(' + value.user_id + ',\'' + value.person.first_name + '\',\'' + value.person.last_name + '\',' + value.person.id_number + ',\'' + pno+ '\',' + value.person.sub_county.county.id + ',' + value.person.sub_county.id + ',\'' + value.email + '\', \'' + value.status + '\',' + value.level.user_level_id + ')"  class="btn btn-info btn-xs" style="margin-right:3px;" ><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                        '<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open "><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
                    ]);
                    i++;
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) { // on error..

            btn.prop("disabled", false).html('<i class="fa fa-trash-o" aria-hidden="true"></i>');
            $.gritter.add({
                title: "<strong>Oops!</strong>",
                text: "An error occurred.Please try again",
                sticky: false,
                time: '',
                class_name: 'gritter-danger'
            });
        }
    });

});