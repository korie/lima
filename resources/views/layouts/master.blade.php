<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{{config('app.name', 'AfyaPoa')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- sends csrf token to controllers via ajax -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    

    <!-- Bootstrap core CSS -->
    <link href="{{ url('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link href="{{ url('/css/font-awesome.min.css ') }}" rel="stylesheet">
	
	<!-- Pace -->
	<link href="{{ url('/css/pace.css ') }}" rel="stylesheet">

	<!-- sweet alert css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" >
		<!-- Datatable -->
	<link href="{{ url('/css/jquery.dataTables_themeroller.css ') }}" rel="stylesheet">
	<!-- Color box -->
	<link href="{{ url('/css/colorbox/colorbox.css ') }}" rel="stylesheet">
	
	<!-- Morris -->
	<link href="{{ url('/css/morris.css ') }}" rel="stylesheet"/>	
	
	<!-- Endless -->
	<link href="{{ url('/css/endless.min.css ') }}" rel="stylesheet">
	<link href="{{ url('/css/endless-skin.css ') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ url('/css/gritter/jquery.gritter.css') }}">	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	@yield('styles')
  </head>

  <body class="overflow-hidden">
	<!-- Overlay Div -->
	<div id="overlay" class="transparent"></div>
	
	<a href="" id="theme-setting-icon"><i class="fa fa-cog fa-lg"></i></a>
	<div id="theme-setting">
		<div class="title">
			<strong class="no-margin">Skin Color</strong>
		</div>
		<div class="theme-box">
			<a class="theme-color" style="background:#323447" id="default"></a>
			<a class="theme-color" style="background:#efefef" id="skin-1"></a>
			<a class="theme-color" style="background:#a93922" id="skin-2"></a>
			<a class="theme-color" style="background:#3e6b96" id="skin-3"></a>
			<a class="theme-color" style="background:#635247" id="skin-4"></a>
			<a class="theme-color" style="background:#3a3a3a" id="skin-5"></a>
			<a class="theme-color" style="background:#495B6C" id="skin-6"></a>
		</div>
		<div class="title">
			<strong class="no-margin">Sidebar Menu</strong>
		</div>
		<div class="theme-box">
			<label class="label-checkbox">
				<input type="checkbox" checked id="fixedSidebar">
				<span class="custom-checkbox"></span>
				Fixed Sidebar
			</label>
		</div>
	</div><!-- /theme-setting -->

	<div id="wrapper" class="preload">
		<div id="top-nav" class="fixed skin-6">
			<a href="#" class="brand">
				<!-- <span>LIMA</span> -->
				<img src="{{ url('img/lima.png') }}">
			</a><!-- /brand -->					
			<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<ul class="nav-notification clearfix">
			
			
				<li class="profile dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<strong>{{ ucwords(Auth::user()->user_fname)}} {{ ucwords(Auth::user()->user_lname)}}</strong>
						<span><i class="fa fa-chevron-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="clearfix" href="#">
									@if(empty(Auth::user()->photo))
									<img src="{{ url('img/user.jpg') }}" alt="User Avatar">
									@else
									<img src="{{ url('profile_photos/'.Auth::user()->photo) }}" alt="User Avatar">
									@endif
								<div class="detail">
									<strong>{{ ucwords(Auth::user()->user_fname)}} {{ ucwords(Auth::user()->user_lname)}}</strong>
									<p class="grey">{{ Auth::user()->email}}</p> 
								</div>
							</a>
						</li>
						<li><a tabindex="-1" href="{{url('/profile')}}" class="main-link"><i class="fa fa-edit fa-lg"></i> Edit profile</a></li>
						<li><a tabindex="-1" href="{{url('/changepassword')}}" class="main-link"><i class="fa fa-lock fa-lg"></i> Change Password</a></li>
						<li><a tabindex="-1" href="#" class="theme-setting"><i class="fa fa-cog fa-lg"></i> Setting</a></li>
						<li class="divider"></li>
						<li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
					</ul>
				</li>
				
			</ul>
		</div><!-- /top-nav-->
		
		<aside class="fixed skin-6">
			<div class="sidebar-inner scrollable-sidebar">
				<div class="size-toggle">
					<a class="btn btn-sm" id="sizeToggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
						<i class="fa fa-power-off"></i>
					</a>
				</div><!-- /size-toggle -->	
				<div class="user-block clearfix">
									@if(empty(Auth::user()->photo))
									<img src="{{ url('img/user.jpg') }}" alt="User Avatar">
									@else
									<img src="{{ url('profile_photos/'.Auth::user()->photo) }}" alt="User Avatar">
									@endif
					<div class="detail">
						<strong>{{ ucwords(Auth::user()->user_fname)}}</strong><span class="badge badge-danger m-left-xs bounceIn animation-delay4"></span>

					</div>
				</div><!-- /user-block -->

				<div class="main-menu">
					<ul>
						<!-- menu view for super user -->
						@if(Auth::user()->user_level == 1)
						<li @if(Route::currentRouteName() == "home") class="active" @endif>
							<a href="{{ url('/home') }}">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i> 
								</span>
								<span class="text">
									Dashboard
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						
						<li>					
							<a href="{{ url('/users') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									User
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
						<a href="{{ url('/corp') }}">
								<span class="menu-icon">
									<i class="fa fa-users fa-lg"></i> 
								</span>
								<span class="text">
									CORP
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
						<a href="{{ url('/farmers') }}">
								<span class="menu-icon">
									<i class="fa fa-users fa-lg"></i> 
								</span>
								<span class="text">
									Farmer
								</span>
								<span class="menu-hover"></span>
							</a>

						</li>
						<li>
							<a href="{{ url('/sa_person') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Agro Agents
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="{{ url('/healthofficer') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Animal Health Officer
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>	
						<li>
							<a href="{{ url('/supplier') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Suppliers
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="{{ url('/products') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
								 Products
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<!-- <li>
							<a href="{{ url('/stocks') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
								Stock
								</span>
								<span class="menu-hover"></span>
							</a>
						</li> -->
						<li>
							<a href="{{ url('/adminorders') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
								 Orders
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="{{ url('/broadcast') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Message Broadcast
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="{{ url('/informationcenter') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Information Centre
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li @if(Route::currentRouteName() == "report") class="active" @endif>
							<a href="{{ url('/report') }}">
								<span class="menu-icon">
									<i class="fa fa-user fa-lg"></i> 
								</span>
								<span class="text">
									Reports
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						@endif
						<!-- end menu view for super user -->
						<!-- menu view for super agent -->
						@if(Auth::user()->user_level == 2)
						<li @if(Route::currentRouteName() == "home") class="active" @endif>
							<a href="{{ url('/home') }}">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i> 
								</span>
								<span class="text">
									Dashboard
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
                        <li>
                            <a href="{{ url('/corps') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Registered CORPs
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/products') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Products
                                </span>
                                <span class="menu-hover"></span>
                            </a>
						</li>
						<li>
                            <a href="{{ url('/orders') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Orders
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/stocks') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                    Stock levels
                                </span>
                                <span class="menu-hover"></span>
                            </a>                        
                        </li>
                        <li>
                            <a href="{{ url('/report') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                    Reports
                                </span>
                                <span class="menu-hover"></span>
                            </a>                        
                        </li>
                        @endif
						<!-- end menu view for super agent -->
						<!-- menu view for supplier -->
						@if(Auth::user()->user_level == 4)
						<li @if(Route::currentRouteName() == "home") class="active" @endif>
							<a href="{{ url('/home') }}">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i> 
								</span>
								<span class="text">
									Dashboard
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
                        <li>
                            <a href="{{ url('/orders') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Orders
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
						<li class="openable">
                            <a href="{{ url('/stocks') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Stock
                                </span>
                                <span class="menu-hover"></span>
                            </a>
							<ul class="submenu">
							<li>
                            <a href="{{ url('/stocks') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 My Stock
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
						<li>
                            <a href="{{ url('/stockmovement') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Stock Movement
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
							</ul>
                        </li>
                        <li>
                            <a href="{{ url('/myproducts') }}">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Products
                                </span>
                                <span class="menu-hover"></span>
                            </a>
						</li>
						<li>
                            <a href="#">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                 Messaging
                                </span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                       
                        <li>
                            <a href="#">
                                <span class="menu-icon">
                                    <i class="fa fa-user fa-lg"></i> 
                                </span>
                                <span class="text">
                                    Reports
                                </span>
                                <span class="menu-hover"></span>
                            </a>                        
                        </li>
						@endif
						
                        <!-- end menu view for supplier -->
						                                             

					</ul>
					
				</div><!-- /main-menu -->
			</div><!-- /sidebar-inner -->
		</aside>

		@yield('content')
		<!-- Footer
		================================================== -->
		<footer>
			<div class="row">
				<div class="col-sm-6">
					<span class="footer-brand">
						<strong class="text-danger">AfyaPoa</strong> 
					</span>
					<p class="no-margin">
						&copy; {{ date('Y') }} <strong>mHealth Kenya</strong>. ALL Rights Reserved. 
					</p>
				</div><!-- /.col -->
			</div><!-- /.row-->
		</footer>
		
		
		<!--Modal-->
		<div class="modal fade" id="newFolder">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4>Create new folder</h4>
      				</div>
				    <div class="modal-body">
				        <form>
							<div class="form-group">
								<label for="folderName">Folder Name</label>
								<input type="text" class="form-control input-sm" id="folderName" placeholder="Folder name here...">
							</div>
						</form>
				    </div>
				    <div class="modal-footer">
				        <button class="btn btn-sm btn-success" data-dismiss="modal" aria-hidden="true">Close</button>
						<a href="#" class="btn btn-danger btn-sm">Save changes</a>
				    </div>
			  	</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div><!-- /wrapper -->

	<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
	
	<!-- Logout confirmation -->
	<div class="custom-popup width-100" id="logoutConfirm">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to logout?</h4>
		</div>

		<div class="text-center">
			<a class="btn btn-success m-right-sm" href="{{ route('logout') }}">Logout</a>
			<a class="btn btn-danger logoutConfirm_close">Cancel</a>
		</div>
	</div>
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<!-- Jquery -->

	<script src="{{ url('/js/jquery-1.10.2.min.js') }}"></script>

	 <script src="https://cdn.jsdelivr.net/alasql/0.2/alasql.min.js"></script> 


	<!-- Bootstrap -->
   <script src="{{ url('/bootstrap/js/bootstrap.js') }}"></script>
   <script src="{{ url('/js/bootstrap-datepicker.updated.min.js') }}"></script>
   
   	<!-- Datatable -->
	<script src="{{ url('/js/jquery.dataTables.min.js ') }}"></script>	
	
	<!-- Flot -->
	<script src="{{ url('/js/jquery.flot.min.js ') }}"></script>
   
	<!-- Morris -->
	<script src="{{ url('/js/rapheal.min.js ') }}"></script>	
	<script src="{{ url('/js/morris.min.js ') }}"></script>	
	
	<!-- Colorbox -->
	<script src="{{ url('/js/jquery.colorbox.min.js') }}"></script>	

	<!-- Sparkline -->
	<script src="{{ url('/js/jquery.sparkline.min.js ') }}"></script>
	
	<!-- Pace -->
	<script src="{{ url('/js/uncompressed/pace.js') }}"></script>
	

	<!-- Popup Overlay -->
	<script src="{{ url('/js/jquery.popupoverlay.min.js') }}"></script>
	
	<!-- Slimscroll -->
	<script src="{{ url('/js/jquery.slimscroll.min.js') }}"></script>

	<!-- Modernizr -->
	<script src="{{ url('/js/modernizr.min.js') }}"></script>
	
	<!-- Cookie -->
	<script src="{{ url('/js/jquery.cookie.min.js') }}"></script>

<!--start  sweet alert scripts tag -->
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<!-- end sweet alert script tag -->
	
	<!-- Endless -->
	<script src="{{ url('/js/endless/endless.js ') }} "></script>
	<script src="{{ url('/js/jquery.gritter.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script>
	  var server = '{{ url('/') }}';
	$(function	()	{
	
		$("body").tooltip({ selector: '[data-toggle=tooltip]' });	
			$('#lightCustomModal').popup({
				pagecontainer: '.container',
				 transition: 'all 0.3s'
			});
		});
	</script>

	
	@yield('scripts')
  </body>
</html>
