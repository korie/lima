@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/') }}"> Home</a></li>
					 <li class="active">Dashboard</li>	 
				</ul>
			</div><!-- /breadcrumb-->
			<div class="main-header clearfix">
				<div class="page-title">
					<h3 class="no-margin">Dashboard</h3>
					<span>Welcome back {{ ucwords(Auth::user()->first_name)}} {{ ucwords(Auth::user()->last_name)}}</span>
				</div><!-- /page-title -->
				
				<!-- <ul class="page-stats">
			    	<li>
			    		<div class="value">
			    			<span>New visits</span>
			    			<h4 id="currentVisitor">4256</h4>
			    		</div>
						<span id="visits" class="sparkline"></span>
			    	</li>
			    	<li>
			    		<div class="value">
			    			<span>My balance</span>
			    			<h4>$<strong id="currentBalance">32153</strong></h4>
			    		</div>
			    		<span id="balances" class="sparkline"></span>
			    	</li>
			    </ul>/page-stats -->
			</div><!-- /main-header -->
			
			<!-- <div class="grey-container shortcut-wrapper">
			
			@if(isset($manager) && $manager==true)
			
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-user"></i>
					</span>
					<span class="text">Clients</span>
				</a>
			@endif
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-bar-chart-o"></i>
					</span>
					<span class="text">Statistic</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-envelope-o"></i>
						<span class="shortcut-alert">
							5
						</span>	
					</span>
					<span class="text">Messages</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-user"></i>
					   <span class="shortcut-alert">
					  10
						</span>	
					</span>
					<span class="text">All Employees</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-building-o"></i>
						<span class="shortcut-alert">
						5
						</span>	
					</span>
					<span class="text">Facilities</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-list"></i>
					</span>
					<span class="text">Activity</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-cog"></i></span>
					<span class="text">Setting</span>
				</a>
			</div>/grey-container -->
			
			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-danger">
							<h2 class="m-top-none" id="superAgentCount"><a href="{{ url('/sa_person') }}">{{ $sa_persons_count }}</a></h2>
							<h5><a href="{{ url('/sa_person') }}">Registered Agro Agents</a></h5>
							<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">5% Higher than last week</span> -->
							<div class="stat-icon">
								<i class="fa fa-user fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-info">
							<h2 class="m-top-none"><id="corpsCount"><a href="{{ url('/corps') }}">{{ $corps_count }}</a></h2>
							<h5><a href="{{ url('/corps') }}">Registered Corps</a></h5>
							<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">1% Higher than last week</span> -->
							<div class="stat-icon">
								<i class="fa fa-hdd-o fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-warning">
							<h2 class="m-top-none" id="farmersCount"><a href="{{ url('/farmers') }}">{{ $farmers_count }}</a></h2>
							<h5><a href="{{ url('/farmers') }}">Registered Farmers</a></h5>
							<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">3% Higher than last week</span> -->
							<div class="stat-icon">
								<i class="fa fa-shopping-cart fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-success">
							<h2 class="m-top-none" id="ordersCount">{{ $orders_count }}</h2>
							<h5>Orders</h5>
							<!-- <i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">15% Higher than last week</span> -->
							<div class="stat-icon">
								<i class="fa fa-bar-chart-o fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading clearfix">
								<span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i> Registered Farmers</span>
								<ul class="tool-bar">
									<li><a href="#" class="refresh-widget" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
								</ul>
							</div>
							<div class="panel-body" id="trafficWidget">
								<div id="placeholder" class="graph" style="height:250px"></div>
							</div>
							<div class="panel-footer">
								<div class="row row-merge">
									<div class="col-xs-3 text-center border-right">
										<h4 class="no-margin">12</h4>
										<small class="text-muted">Users</small>
									</div>
									<div class="col-xs-3 text-center border-right">
										<h4 class="no-margin">54</h4>
										<small class="text-muted">Products</small>
									</div>
									<div class="col-xs-3 text-center border-right">
										<h4 class="no-margin">10</h4>
										<small class="text-muted">Suppliers</small>
									</div>
									<div class="col-xs-3 text-center">
										<h4 class="no-margin">8</h4>
										<small class="text-muted">Animal Health Officers</small>
									</div>
								</div><!-- ./row -->
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div><!-- /panel -->
								
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<div class="panel panel-default panel-stat2 bg-success">
									<div class="panel-body">
									<a style="color:#ffffff" href="#">
										<span class="stat-icon">
											<i class="fa fa-users"></i>
										</span>
										<div class="pull-right text-right">
											<div class="value">8</div>
											<div class="title">Farmer orders</div>
										</div>
									</a>
									</div>
								</div><!-- /panel -->
							</div><!-- /.col -->
							<div class="col-md-4 col-sm-4">
								<div class="panel panel-default panel-stat2 bg-warning">
									<div class="panel-body">
									<a style="color:#ffffff" href="#">
										<span class="stat-icon">
											<i class="fa fa-users"></i>
										</span>
										<div class="pull-right text-right">
											<div class="value">3</div>
											<div class="title">CORP Orders </div>
										</div>
									</a>
									</div>
								</div><!-- /panel -->
							</div><!-- /.col -->
							<div class="col-md-4 col-sm-4">
								<div class="panel panel-default panel-stat2 bg-info">
									<div class="panel-body">
									<a style="color:#ffffff" href="#">
										<span class="stat-icon">
											<i class="fa fa-users"></i>
										</span>
										<div class="pull-right text-right">
											<div class="value">5</div>
											<div class="title">Suppliers Orders</div>
										</div>
										</a>
									</div>
								</div><!-- /panel -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						<div class="panel panel-default">
							<div class="panel-heading">
								Work Progress

								<span class="badge badge-info pull-right">	
									4 left
								</span>
							</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Task</th>
										<th>Progress</th>
										<th></th>
										<th>Time</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Bug Fixes</td>
										<td>
											<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">
												<div class="progress-bar" style="width: 45%">
													<span class="sr-only">45% Complete</span>
												</div>
											</div>
										</td>
										<td>45%</td>
										<td><span class="badge badge-info">2hr</span></td>
									</tr>
									<tr>
										<td>Mobile Development</td>
										<td>
											<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">
												<div class="progress-bar progress-bar-success" style="width: 61%">
													<span class="sr-only">61% Complete</span>
												</div>
											</div>
										</td>
										<td>61%</td>
										<td><span class="badge badge-info">1hr</span></td>
									</tr>
									<tr>
										<td>Unit Testing</td>
										<td>
											<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">
												<div class="progress-bar progress-bar-danger" style="width: 97%">
													<span class="sr-only">97% Complete</span>
												</div>
											</div>
										</td>
										<td>97%</td>
										<td><span class="badge badge-info">5m</span></td>
									</tr>
									<tr>
										<td>New frontend layout</td>
										<td>
											<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">
												<div class="progress-bar progress-bar-warning" style="width: 18%">
													<span class="sr-only">18% Complete</span>
												</div>
											</div>
										</td>
										<td>18%</td>
										<td><span class="badge badge-info">12hr</span></td>
									</tr>
								</tbody>
							</table>
						</div><!-- /panel -->
						<div class="row">
							<div class="col-lg-6">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<span class="pull-left">
											To Do List <span class="text-success m-left-xs"><i class="fa fa-check"></i></span>
										</span>
										<ul class="tool-bar">
											<li><a href="#" class="refresh-widget" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
											<li><a href="#toDoListWidget" data-toggle="collapse"><i class="fa fa-arrows-v"></i></a></li>
										</ul>
									</div>
									<div class="panel-body no-padding collapse in" id="toDoListWidget">
										<ul class="list-group task-list no-margin collapse in">
											<li class="list-group-item selected">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish" checked>
													 <span class="custom-checkbox"></span>
												</label>
												SEO Optimisation
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
											</li>
											<li class="list-group-item">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish">
													 <span class="custom-checkbox"></span>
												</label>
												Unit Testing
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
											</li>
											<li class="list-group-item">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish">
													 <span class="custom-checkbox"></span>
												</label>
												Mobile Development 
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
												<span class="badge badge-success m-right-xs">3</span>
											</li>
											<li class="list-group-item">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish">
													 <span class="custom-checkbox"></span>
												</label>
												Database Migration
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
											</li>
											<li class="list-group-item">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish">
													 <span class="custom-checkbox"></span>
												</label>
												New Frontend Layout <span class="label label-warning m-left-xs">PENDING</span>
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
											</li>
											<li class="list-group-item">
												<label class="label-checkbox inline">
													 <input type="checkbox" class="task-finish">
													 <span class="custom-checkbox"></span>
												</label>
												Bug Fixes <span class="label label-danger m-left-xs">IMPORTANT</span>
												<span class="pull-right">
													<a href="#" class="task-del"><i class="fa fa-trash-o fa-lg text-danger"></i></a>
												</span>
											</li>
										</ul><!-- /list-group -->
									</div>
									<div class="loading-overlay">
										<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
									</div>
								</div><!-- /panel -->
							</div><!-- /.col -->
							<div class="col-lg-6">
								<div class="panel panel-default">	
									<div class="panel-heading clearfix">
										<span class="pull-left">Feeds</span>
										<ul class="tool-bar">
											<li><a href="#" class="refresh-widget" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a></li>
											<li><a href="#feedList" data-toggle="collapse"><i class="fa fa-arrows-v"></i></a></li>
										</ul>
									</div>		
									<ul class="list-group collapse in" id="feedList">
										<li class="list-group-item clearfix">
											<div class="activity-icon small">
												<i class="fa fa-camera"></i>
											</div>
											<div class="pull-left m-left-sm">
												<span>Sales agent Add a new photo.</span><br/>
												<small class="text-muted"><i class="fa fa-clock-o"></i> 2m ago</small>
											</div>
										</li>
										<li class="list-group-item clearfix">
											<div class="activity-icon bg-success small">
												<i class="fa fa-usd"></i>
											</div>
											<div class="pull-left m-left-sm">
												<span>2 items sold.</span><br/>
												<small class="text-muted"><i class="fa fa-clock-o"></i> 30m ago</small>
											</div>	
										</li>
										<li class="list-group-item clearfix">
											<div class="activity-icon bg-info small">
												<i class="fa fa-comment"></i>
											</div>
											<div class="pull-left m-left-sm">
												<span>John Doe commented on <a href="#">This Article</a></span><br/>
												<small class="text-muted"><i class="fa fa-clock-o"></i> 1hr ago</small>
											</div>
										</li>
										<li class="list-group-item clearfix">
											<div class="activity-icon bg-success small">
												<i class="fa fa-usd"></i>
											</div>
											<div class="pull-left m-left-sm">
												<span>3 items sold.</span><br/>
												<small class="text-muted"><i class="fa fa-clock-o"></i> 2days ago</small>
											</div>	
										</li>
									</ul><!-- /list-group -->	
									<div class="loading-overlay">
										<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
									</div>
								</div><!-- /panel -->
							</div><!-- /.col -->
						</div><!-- ./row -->
					</div><!-- /.col -->
					<div class="col-lg-4">
						<div class="panel bg-info fadeInDown animation-delay4">
							<div class="panel-body">
								<div id="lineChart" style="height: 150px;"></div>
								<div class="pull-right text-right">
									<strong class="font-14">Broadcast Messages</strong><br/>
									<span><i class="fa fa-shopping-cart"></i> Total Broadcasts 15</span>
									<div class="seperator"></div>
								</div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-xs-4">
										Broadcasts in September
										<strong class="block">4</strong>
									</div><!-- /.col -->
									<div class="col-xs-4">
										Broadcasts in October
										<strong class="block">6</strong>
									</div><!-- /.col -->
									<div class="col-xs-4">
										Broadcasts in November
										<strong class="block">5</strong>
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div>
						</div><!-- /panel -->
								
						<div class="panel bg-success fadeInDown animation-delay5">
							<div class="panel-body">
								<div id="barChart" style="height: 150px;"></div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-xs-6">
										<h4 class="no-margin">Total CORP Earnings</h4>
									</div><!-- /.col -->
									<div class="col-xs-6 text-right">
										<h4 class="no-margin">Ksh.17,531</h4>
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div>
						</div><!-- /panel -->
								
						<div class="panel bg-danger">
							<div class="panel-body">
								<h4>Database Migration</h4>
								<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">
									<div class="progress-bar progress-bar-danger" style="width: 65%">
										<span class="sr-only">65% Complete</span>
									</div>
								</div>
								<strong class="pull-left m-top-xs">65% Complete</strong>
								<strong class="pull-right m-top-xs">1hr left</strong>
							</div>
						</div><!-- /panel -->
						<div class="panel panel-default">
							<div class="panel-body">
								<div id="donutChart" style="height: 250px;"></div>
								<div class="panel-group" id="accordion">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
													IN-STORE Sales
													<span class="badge badge-success pull-right">75%</span>
												</a>
											</h4>
										</div>
										<div id="collapseOne" class="panel-collapse collapse">
											<div class="panel-body">
												Orders in store pending processing and delivery
											</div>
										</div>
									</div><!-- panel -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
													Pending Sales
												</a>
											</h4>
										</div>
										<div id="collapseTwo" class="panel-collapse collapse">
											<div class="panel-body">
												Sales accepted pending delivery to the farmers.
											</div>
										</div>
									</div><!-- panel -->
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
													MAIL-ORDER Sales 
													<span class="badge badge-danger pull-right"><i class="fa fa-arrow-down"></i> 3%</span>
												</a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse">
											<div class="panel-body">
												Sales orders received by mail pending delivery
											</div>
										</div>
									</div><!-- panel -->
								</div><!-- panel-group -->
							</div>
						</div><!-- /panel -->
					</div><!-- /.col -->
				</div><!-- /.row -->
				
				
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->
@endsection
@section('scripts')

			<script src="{{ url('/js/endless/endless_dashboard.js ') }}"></script>

@endsection
