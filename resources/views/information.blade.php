@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Information Center</li>	 
				</ul>
			</div><!-- /breadcrumb-->

            <div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addInforModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Information</button>
						</div>

					</div><!-- /.col -->					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Information
								<span class="label label-info pull-right" id="totalspan">{{count($infor)}} Infor </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="infor_dataTable">
								<thead class="thead-dark">
									<tr>
                                        <th scope="col">No </th>
                                        <th scope="col">Article Name</th>
                                        <th scope="col">Article Link</th>
                                        <th scope="col">Date Valid </th>
                                        <th scope="col">Date Added </th>
                                        <th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($infor as $inf)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$inf->infor_name}}</td>
                                        <td>{{$inf->infor_link}}</td>
                                        <td>{{$inf->validity}}</td>
										<td>{{$inf->created_at}}</td>
										<!-- <td><a onclick="editinfor({{$inf->infor_id}}, '{{$inf->infor_name}}', '{{$inf->infor_link}}', '{{$inf->validity}}');" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                                        <td><a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteinfor" data-user="{{ $inf->infor_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                        </div>
                    </div>

				</div>
			</div><!-- /row-->
		<div class="row">
					
				</div><!-- /.row -->												
			</div><!-- /.padding-md -->			
		</div><!-- /main-container -->
        <div class="modal fade" id="addInforModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Information</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addinfor') ?>" id="addinforform">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Article Name</label>
									<input type="text" class="form-control input-sm" name="infor_name" placeholder="Article Name">
								</div><!-- /form-group -->
								<div class="form-group">
									<label>Article Link</label>
										<input type="text" class="form-control input-sm" name="infor_link" placeholder="Article Link">
                                </div><!-- /form-group -->
                                <div class="form-group">
                                    <label>Validity</label>
                                    <!-- <input type="text" class="form-control input-sm" name="validity" placeholder="validity"> -->
                                    <input class="form-control input-sm" type="date" name="validity">
								</div><!-- /form-group -->
												
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addinforbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Infor</button>
							</form>
						</div>
					</div>
				</div>
            </div>
			
@endsection
@section('scripts')
<script>
$(".category").select2();
</script>
<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/infor_center.js"></script>

@endsection
