@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">All Orders</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
				   
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								All Order Details
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>									
									<th scope="col">Product Name</th>
									<th scope="col">Order From</th> <!--Names of e.g Farmer, Corp, SA etc -->
									<th scope="col">Order To</th> <!--Names of e.g Farmer, Corp, SA etc -->
									<th scope="col">Order Status</th> <!--e.g Delivered, Pending etc -->
									<th scope="col">Quantity Ordered</th>
									<th scope="col">Quantity Delivered</th>
									<th scope="col">Order Date</th>
									<th scope="col">Comments</th>
									</tr>
								</thead>
								<tbody>
								@foreach($order_details as $order_detail)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$order_detail->product->product_name}}</td>
										<!-- <td>{{$order_detail->quantity_ordered}}</td> -->
										<td>{{$order_detail->order->client->first_name}} {{$order_detail->order->client->last_name}}</td>									
										<td>{{$order_detail->order->seller->first_name}} {{$order_detail->order->seller->last_name}}</td>
										if({{$order_detail->order->status}} == 0){
											<td>Pending</td>
										}elseif({{$order_detail->order->status}} == 1){
											<td>In Progress</td>
										}else{
											<td>Delivered</td>
										}
										<!-- <td>{{$order_detail->order->status}}</td> -->
										<td>{{$order_detail->quantity_ordered}}</td>
										<td>{{$order_detail->quantity_delivered}}</td>
										<td>{{$order_detail->order->created_at}}</td>
                                        <td>{{$order_detail->comments}}</td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->
        	
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/order.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
