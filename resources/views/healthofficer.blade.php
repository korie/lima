@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Animal Health Officer</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<!-- <button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addHealthOfficerrModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Health Officer</button> -->
						</div>

					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
							Animal Health Officer
								<span class="label label-info pull-right" id="totalspan">{{count($corps)}} Animal Health Officer </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="healthofficer_dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>
									<th scope="col">Name</th>
									<th scope="col">ID No.</th>
									<th scope="col">Phone No.</th>
									<th scope="col">Registration No.</th>
									<th scope="col">County</th>
									<th scope="col">Sub County</th>
									<th scope="col">Date Created</th>
									<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($corps as $corp)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{ucwords($corp->person->first_name)}} {{ucwords($corp->person->last_name)}}</td>
										<td>{{$corp->person->id_number}}</td>
										<td>{{$corp->person->phone_no}}</td>
										<td>{{$corp->vet_reg_number}}</td>
										<td>{{$corp->person->sub_county->county->name}}</td>
										<td>{{$corp->person->sub_county->name}}</td>
                                        <td>{{$corp->created_at}}</td>
										<td><a onclick="edithealthofficer({{$corp->corp_id}}, '{{ucwords($corp->person->first_name)}}','{{ucwords($corp->person->last_name)}}', {{$corp->person->id_number}}, '0{!! substr($corp->person->phone_no, 4) !!}', {{$corp->person->sub_county->county->id}}, {{$corp->person->sub_county->id}}, '{{$corp->is_vet}}');" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeletehealthofficer" data-user="{{ $corp->corp_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->


    

        <!-- Edit user modal -->


        


        	<!-- pop confirm modal -->
	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeletehealthofficer-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/healthofficer.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
