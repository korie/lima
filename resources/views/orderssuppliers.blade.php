@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
		
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/report') }}"> Orders</a></li>
					 <li class="active">Orders</li>
				</ul>
			</div><!-- /breadcrumb-->
			
			<div class="padding-md">
			
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addSaOrderModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Order</button>
						</div>

					</div><!-- /.col -->
				</div>
	
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								 Orders
								<span class="label label-info pull-right" id="totalspan">{{count($orders)}} Orders </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>
									<th scope="col">Order ID</th>
									<th scope="col">Order To</th>
									<th scope="col">Status</th>
									<th scope="col">Date Created</th>
									<!-- <th scope="col">Product Name</th>
									<th scope="col">Quantity Ordered</th>
									<th scope="col">Quantity Delivered</th> -->
									
									<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($orders as $order)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$order->order_id}}</td>
										<td>{{$order->person->first_name}}</td>
										<td>
										@if($order->status ==  0)
										   Pending
										@else
										   Delivered
										@endif
										</td>
										<td>{{$order->created_at}}</td>										
										<td><a href="{{ url('vieworderdetails/'.$order->order_id) }}"> <i>View Order Details</i></a>
                                      
					          	@if(Auth::user()->user_level !== 1)
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteuser" data-user="{{ $order->order_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
										@endif
                               </tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->


        <!-- Add user modal -->
		<div class="modal fade" id="addSaOrderModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Order</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<div class="modal-body">
							<form role="form"  action="" id="add_order">
												{{ csrf_field() }}
												<div class="form-group" >
												<div class="alert alert-danger print-error-msg" style="display:none">
												<ul></ul>
												</div>


												<div class="alert alert-success print-success-msg" style="display:none">
												<ul></ul>
												</div>
											
													<label>Supplier</label>
													<select class="form-control input-sm category" data-width="100%" id="supplier_id" name="supplier_id">
												
												<!-- <option value="">Select Product</option> -->
											
													@if (count($suppliers) > 0)
														@foreach($suppliers as $supplier)
														<option value="{{$supplier->person_id }}">{{ ucwords($supplier->person->first_name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->
												<div class="table-responsive">  
													<table class="table table-bordered" id="dynamic_field">  
														<tr>  
															<td>
															<label>Select Product</label>
															<select class="form-control input-sm category" data-width="100%" id="product_id" name="product_id[]">
															<!-- <option value="">Select Product</option> -->
																@if (count($products) > 0)
																	@foreach($products as $product)
																	<option value="{{$product->product_id }}">{{ ucwords($product->product_name) }}</option>
																		@endforeach
																@endif
															</select>
															
															</td>  
															<td>
															<label>Quantity</label>
															<input type="text" name="quantity_ordered[]" placeholder="Quantity" class="form-control name_list" /></td>  


															<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
														</tr>  
													</table>  
												</div>

												

												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  

												<!-- <button type="submit" id="adduserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add User</button> -->
											</form>
						</div>
						</div>
					</div>
        </div>

        <!-- Edit user modal -->


        <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Edit User</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('editorder') ?>" id="edituserform">
												{{ csrf_field() }}
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" id = "firstname" name="first_name" placeholder="First Name">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" id="lastname" name="last_name" placeholder="Last Name">
												</div><!-- /form-group -->

												<div class="form-group">
													<label>ID Number</label>
												<input type="text" class="form-control input-sm" id="idno" name="id_num" id="id_num">
												</div>
												<div class="form-group">
													<label>Phone Number</label>
													<input type="text" class="form-control input-sm" id ="phoneno"  name="phone_no" placeholder="0700000000">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Email</label>
													<input type="email" class="form-control input-sm" id="editemail" name="email" placeholder="example@example.org">
												</div><!-- /form-group -->
												
												
												<input type="hidden" id="user_id" name="id">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="edituserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Edit User</button>
											</form>
						</div>
						</div>
					</div>
        </div>


        	<!-- pop confirm modal -->
	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeleteuser-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/user.js"></script>
<script src="/assets/js/order.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
