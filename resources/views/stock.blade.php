@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/report') }}"> Stock</a></li>
					 <li class="active">Stock</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addUserModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Stock</button>
						</div>

					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Stock Movement
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>
									<th scope="col">Product Name</th>
									<th scope="col">Stock Code</th>
									<th scope="col">Stock In</th>
									<th scope="col">Stock Out</th>
									<th scope="col">Date Created</th>
									<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($stocks as $stock)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$stock->products->first()->product_name}}</td>
										<td>{{$stock->stock_code}}</td>
										<td>{{$stock->stock_in}}</td>
										<td>{{$stock->stock_out}}</td>
                                        <td>{{$stock->created_at}}</td>
										<td></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->


        <!-- Add user modal -->
		<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Stock</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addstock') ?>" id="adduserform">
												{{ csrf_field() }}
												<div class="form-group" >
													<label>Product</label>
													<select class="form-control input-sm category" data-width="100%" id="product_id" name="product_id">
												<option value="">Select Product</option>
													@if (count($products) > 0)
														@foreach($products as $product)
														<option value="{{$product->product->product_id }}">{{ ucwords($product->product->product_name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Stock Code</label>
													<input type="text" class="form-control input-sm" name="stock_code" placeholder="Stock Code">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Quantity</label>
													<input type="text" class="form-control input-sm" name="quantity" placeholder="Quantity">
												</div><!-- /form-group -->
											

												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="adduserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Stock</button>
											</form>
						</div>
						</div>
					</div>
        </div>

        <!-- Edit user modal -->


        <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Edit Stock</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('editstock') ?>" id="editstockform">
												{{ csrf_field() }}
												<div class="form-group" >
													<label>Product</label>
													<select class="form-control input-sm category" data-width="100%" id="editstock" name="stock_id">
												<option value="">Select Product</option>
													@if (count($products) > 0)
														@foreach($products as $product)
														<option value="{{$product->product->product_id }}">{{ ucwords($product->product->product_name) }}</option>
															@endforeach
													@endif
												</select>
												</div
												<div class="form-group">
													<label>Stock Code</label>
													<input type="text" class="form-control input-sm" id = "stock code" name="stock_code" placeholder="Stock Code">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Quantity</label>
													<input type="text" class="form-control input-sm" id="quantity" name="quantity" placeholder="Quantity">
												</div><!-- /form-group -->
  
												<input type="hidden" id="stock_id" name="stock_id">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="edituserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Edit Stock</button>
											</form>
						</div>
						</div>
					</div>
        </div>


        	<!-- pop confirm modal -->
	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeleteuser-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/user.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
