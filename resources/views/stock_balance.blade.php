@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/report') }}"> Stock</a></li>
					 <li class="active">Stock</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<!-- <div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addUserModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Stock</button>
						</div>

					</div> -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Stock Balance
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>
									<th scope="col">Product Name</th>
									<th scope="col">Product Code</th>
									<th scope="col">Stock Balance</th>
									<th scope="col">Updated at</th>						
									</tr>
								</thead>
								<tbody>
								@foreach($products as $product)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$product->product_name}}</td>
										<td>{{$product->product_code}}</td>
										<td>{{$product->balance}}</td>										<td>{{$product->stock_out}}</td>
                                        <td>{{$product->updated_at}}</td>										
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->


        	<!-- pop confirm modal -->
	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeleteuser-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/user.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
