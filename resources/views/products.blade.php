@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Products</li>	 
				</ul>
			</div><!-- /breadcrumb-->

            <div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addProductModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Product</button>
						</div>

					</div><!-- /.col -->					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Users
								<span class="label label-info pull-right" id="totalspan">{{count($myproducts)}} Products </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="products_dataTable">
								<thead class="thead-dark">
									<tr>
                                        <th scope="col">No </th>
                                        <th scope="col">Product Name</th>
                                        <th scope="col">Product Code</th>
                                        <th scope="col">Date Added </th>
                                        <th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($myproducts as $product)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$product->product->product_name}}</td>
										<td>{{$product->product->product_code}}</td>
										<td>{{$product->product->created_at}}</td>
										<!-- <td><a onclick="editProducts({{$product->product_id}}, '{{$product->product_name}}', '{{$product->product_code}}');" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                                        <td><a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteproduct" data-user="{{ $product->product_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">
					
				</div><!-- /.row -->												
			</div><!-- /.padding-md -->			
		</div><!-- /main-container -->
		@if(Auth::user()->user_level == 1)
        <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addproducts') ?>" id="addproductform">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Product Name</label>
									<input type="text" class="form-control input-sm" name="product_name" placeholder="Product Name">
								</div><!-- /form-group -->
								<div class="form-group">
									<label>Product Code</label>
										<input type="text" class="form-control input-sm" name="product_code" placeholder="Product Code">
								</div><!-- /form-group -->
												
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addproductbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Product</button>
							</form>
						</div>
					</div>
				</div>
            </div>
			@else
			<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addproductssupplier') ?>" id="addproductssupplierform">
								{{ csrf_field() }}
								<div class="form-group">

								    
											
										<label>Product</label>
										<select  multiple class="form-control input-sm category" data-width="100%" id="product_id" name="product_id[]">
										
										<!-- <option value="">Select Product</option> -->
									
											@if (count($products) > 0)
												@foreach($products as $product)
												<option value="{{$product->product_id }}">{{ ucwords($product->product_name) }}</option>
													@endforeach
											@endif
										</select>
										
									
								</div><!-- /form-group -->
											
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addproductbtnsupplier" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Product</button>
							</form>
						</div>
					</div>
				</div>
            </div>
			@endif
@endsection
@section('scripts')
<script>
$(".category").select2();
</script>
<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/products.js"></script>

@endsection
