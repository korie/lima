@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/agent') }}"> Agents</a></li>
					 <li class="active">Agents</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addAgentModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Agent</button>
						</div>

					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Users
								<span class="label label-info pull-right" id="totalspan">{{count($agent)}} Agents </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
                                        <th scope="col">No </th>
                                        <th scope="col">Agent Name</th>
                                        <thscope="col">Date Created </th>
                                        <th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($agent as $agent)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$agent->sa_name}}</td>
										<td><a onclick="editAgent({{$agent->sa_id}});" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteagent" data-user="{{ $agent->sa_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                       	</div>

					</div>
				</div><!-- /row-->
				<div class="row">
					
				</div><!-- /.row -->												
			</div><!-- /.padding-md -->			
		</div><!-- /main-container -->

        <div class="modal fade" id="addAgentModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Agent</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addagent') ?>" id="addagentform">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Agent Name</label>
									<input type="text" class="form-control input-sm" name="agent_name" placeholder="Agent Name">
								</div><!-- /form-group -->
												
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addagentbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus-plus" aria-hidden="true"></i> Add Agent</button>
							</form>
						</div>
					</div>
				</div>
            </div>
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/agent.js"></script>

@endsection
