@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/report') }}"> Reports</a></li>
					 <li class="active">Reports</li>	 
				</ul>
			</div><!-- /breadcrumb-->
			<div class="main-header clearfix">
				<div class="page-title">
					<h3 class="no-margin">Reports</h3>
				</div><!-- /page-title -->
				
			</div><!-- /main-header -->
			
			<div class="grey-container shortcut-wrapper">
			
			@if(isset($manager) && $manager==true)
			
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-user"></i>
					</span>
					<span class="text">Oder Reports</span>
				</a>
			@endif
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-bar-chart-o"></i>
					</span>
					<span class="text">Oder Reports</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-envelope-o"></i>
						<span class="shortcut-alert">
							5
						</span>	
					</span>
					<span class="text">Order Delivery Reports</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-user"></i>
					   <span class="shortcut-alert">
					  10
						</span>	
					</span>
					<span class="text">Products Reports</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-building-o"></i>
						<span class="shortcut-alert">
						5
						</span>	
					</span>
					<span class="text">Suppliers Stock</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-list"></i>
					</span>
					<span class="text">Delivered Broadcasts</span>
				</a>
				<a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-cog"></i></span>
					<span class="text">Agents</span>
				</a>
			</div><!-- /grey-container -->
			
			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
                        <table class="table table-hover" id="tb_reports">
                            <thead>
                                <tr>
                                    <th>Stock Id</th>
                                    <th>Product Name</th>
                                    <th>Quantity Order</th>
                                    <th>Quantity Delivered</th>
                                    <th>Order Date</th>
                                    <th>Delivery Date</th>
                                </tr>
                            </thead>
                        </table>
					</div><!-- /.col -->
					
				</div>
				<div class="row">
					
				</div><!-- /.row -->								
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->
@endsection
@section('scripts')

			<script src="{{ url('/js/endless/endless_dashboard.js ') }}"></script>

@endsection
