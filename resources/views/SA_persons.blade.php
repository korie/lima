@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Super Agents</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addSa_personModal"> <i class="fa fa-plus" aria-hidden="true"></i>Add Super Agents</button>
						</div>

					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Super Agents
								<span class="label label-info pull-right" id="totalspan">{{count($sa_persons)}} Super Agents </span>
							</div>
							<div class="padding-md clearfix">
                                        <table class="table" id="dataTable">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">No.</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">ID No.</th>
                                                    <th scope="col">Phone No.</th>
                                                    <th scope="col">County</th>
                                                    <th scope="col">Sub County</th>
                                                    <th scope="col">Date Created</th>
                                                    <th scope="col">Created By</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($sa_persons as $SA_person)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ucwords($SA_person->person->first_name)}} {{ucwords($SA_person->person->last_name)}}</td>
                                                    <td>{{$SA_person->person->id_number}}</td>
                                                    <td>{{$SA_person->person->phone_no}}</td>
                                                    <td>{{$SA_person->person->sub_county->county->name}}</td>
                                                    <td>{{$SA_person->person->sub_county->name}}</td>
                                                    <td>{{$SA_person->created_at}}</td>
                                                    <td>{{ucwords($SA_person->creator->first_name)}} {{ucwords($SA_person->creator->last_name)}}</td>
                                                    <td><a onclick="editsaperson({{$SA_person->farmer_id}}, '{{ucwords($SA_person->person->first_name)}}','{{ucwords($SA_person->person->last_name)}}', {{$SA_person->person->id_number}}, '0{!! substr($SA_person->person->phone_no, 4) !!}', {{$SA_person->person->sub_county->county->id}}, {{$SA_person->person->sub_county->id}});" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeletesaperson" data-user="{{ $SA_person->sa_person_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									            </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div><!-- /row-->
                        <div class="row">

                        </div><!-- /.row -->
                    </div><!-- /.padding-md -->
                </div><!-- /main-container -->

                <div class="modal fade" id="addSa_personModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Add SA</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form role="form" county="post" action="<?php echo URL::route('add_sa_person') ?>" id="add_sa_personform">
                                    {{ csrf_field() }} 
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name">
                                    </div><!-- /form-group -->
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name">
                                    </div><!-- /form-group -->

                                    <div class="form-group">
                                        <label>ID Number</label>
                                        <input type="text" class="form-control input-sm"  name="id_num" id="id_num">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control input-sm" name="phone_no" placeholder="0700000000">
                                    </div><!-- /form-group -->
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control input-sm" name="email" placeholder="example@example.org">
                                    </div><!-- /form-group -->
                                    <div class="form-group" >
                                        <label>County</label>
                                        <select class="form-control input-sm category" data-width="100%" id="county" name="county_id">
                                            <option value="">Select County</option>
                                            @if (count($counties) > 0)
                                            @foreach($counties as $county)
                                            <option value="{{$county->id }}">{{ ucwords($county->name) }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div><!-- /form-group -->
                                    <div class="form-group">
                                        <label>Sub County</label>
                                        <select class="form-control input-sm category" data-width="100%" id="sub_county" name="sub_county_id" >
                                            <option value="">Select Sub County</option>
                                            @if (count($subcounties) > 0)
                                            @foreach($subcounties as $sub_county)
                                            <option value="{{$sub_county->id }}">{{ ucwords($sub_county->name) }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div><!-- /form-group -->
                                   

                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="submit" id="addSAbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add SaPerson</button>
                                </form>



                            </div>
                        </div>
                    </div>
                    @endsection

                    @section('scripts')
                    @section('scripts')

                    <script src="/assets/js/datatables.js"></script>
                    <script src="/assets/js/sa_person.js"></script>
                    <script type="text/javascript">
                    </script>

                    @endsection
                    @endsection
