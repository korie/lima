@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Users</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addUserModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add User</button>
						</div>

					</div><!-- /.col -->
					<div class="col-sm-6 col-md-6">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="levelmodalbtn" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#addUserLevelModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add User Level</button>
						</div>

					</div><!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Users
								<span class="label label-info pull-right" id="totalspan">{{count($users)}} Users </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table table-hover" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>
									<th scope="col">Name</th>
									<th scope="col">ID No.</th>
									<th scope="col">Phone No.</th>
									<th scope="col">County</th>
									<th scope="col">Sub County</th>
									<th scope="col">Email</th>
									<th scope="col">Status</th>
									<th scope="col">Date Created</th>
									<th scope="col">Created By</th>
									<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($users as $user)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{ucwords($user->person->first_name)}} {{ucwords($user->person->last_name)}}</td>
										<td>{{$user->person->id_number}}</td>
										<td>0{!! substr($user->person->phone_no, 4) !!}</td>
										<td>{{$user->person->sub_county->county->name}}</td>
										<td>{{$user->person->sub_county->name}}</td>
										<td>{{$user->email}}</td>
										<td>{{$user->status}}</td>
                                        <td>{{$user->created_at}}</td>
										<td>{{ucwords($user->creator->first_name)}} {{ucwords($user->creator->last_name)}}</td>
										<td><a onclick="editUser({{$user->user_id}}, '{{ucwords($user->person->first_name)}}','{{ucwords($user->person->last_name)}}', {{$user->person->id_number}}, '0{!! substr($user->person->phone_no, 4) !!}', {{$user->person->sub_county->county->id}}, {{$user->person->sub_county->id}}, '{{$user->email}}', '{{$user->status}}',{{$user->level->user_level_id }});" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteuser" data-user="{{ $user->user_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->


        <!-- Add user modal -->
		<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add User</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" method="post" action="<?php echo URL::route('adduser') ?>" id="adduserform">
												{{ csrf_field() }}
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" name="first_name" placeholder="First Name">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name">
												</div><!-- /form-group -->

												<div class="form-group">
													<label>ID Number</label>
												<input type="text" class="form-control input-sm"  name="id_num" id="id_num">
												</div>
												<div class="form-group">
													<label>Phone Number</label>
													<input type="text" class="form-control input-sm" name="phone_no" placeholder="0700000000">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Email</label>
													<input type="email" class="form-control input-sm" name="email" placeholder="example@example.org">
												</div><!-- /form-group -->
												<div class="form-group" >
													<label>County</label>
													<select class="form-control input-sm category" data-width="100%" id="county" name="county_id">
												<option value="">Select County</option>
													@if (count($counties) > 0)
														@foreach($counties as $county)
														<option value="{{$county->id }}">{{ ucwords($county->name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Sub County</label>
													<select class="form-control input-sm category" data-width="100%" id="sub_county" name="sub_county_id" >
												<option value="">Select Sub County</option>

												</select>
												</div><!-- /form-group -->
												<div class="form-group">
													<label>User Level</label>
													<select class="form-control input-sm category" data-width="100%" id="level" name="user_level_id" >
												<option value="">Select User Level</option>
												@if (count($userlevels) > 0)
														@foreach($userlevels as $userlevel)
														<option value="{{$userlevel->user_level_id }}">{{ ucwords($userlevel->user_level_name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->

												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="adduserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add User</button>
											</form>
						</div>
						</div>
					</div>
        </div>

        <!-- Edit user modal -->

		//****************************

		<!-- Add user level modal -->
		<div class="modal fade" id="addUserLevelModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add User Level</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('adduserlevel') ?>" id="adduserlevelform">
												{{ csrf_field() }}
												<div class="form-group">
													<label>User Level Name</label>
													<input type="text" class="form-control input-sm" name="user_level_name" placeholder="User Level Name">
												</div><!-- /form-group -->												

												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="adduserlevelbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add User Level</button>
											</form>
						</div>
					</div>
        </div>

        <!-- Edit user Level modal -->

		//*******************************************


        <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Edit User</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('edituser') ?>" id="edituserform">
												{{ csrf_field() }}
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" id = "firstname" name="first_name" placeholder="First Name">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control input-sm" id="lastname" name="last_name" placeholder="Last Name">
												</div><!-- /form-group -->

												<div class="form-group">
													<label>ID Number</label>
												<input type="text" class="form-control input-sm" id="idno" name="id_num" id="id_num">
												</div>
												<div class="form-group">
													<label>Phone Number</label>
													<input type="text" class="form-control input-sm" id ="phoneno"  name="phone_no" placeholder="0700000000">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Email</label>
													<input type="email" class="form-control input-sm" id="editemail" name="email" placeholder="example@example.org">
												</div><!-- /form-group -->
												<div class="form-group" >
													<label>County</label>
													<select class="form-control input-sm category" data-width="100%" id="editcounty" name="county_id">
												<option value="">Select County</option>
													@if (count($counties) > 0)
														@foreach($counties as $county)
														<option value="{{$county->id }}">{{ ucwords($county->name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Sub County</label>
													<select class="form-control input-sm category" data-width="100%" id="editsubcounty" name="sub_county_id" >
												<option value="">Select Sub County</option>
												@if (count($subcounties) > 0)
														@foreach($subcounties as $subcounty)
														<option value="{{$subcounty->id }}">{{ ucwords($subcounty->name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->
												<div class="form-group">
													<label>User Level</label>
													<select class="form-control input-sm category" data-width="100%" id="editlevel" name="user_level_id" >
												<option value="">Select User Level</option>
												@if (count($userlevels) > 0)
														@foreach($userlevels as $userlevel)
														<option value="{{$userlevel->user_level_id }}">{{ ucwords($userlevel->user_level_name) }}</option>
															@endforeach
													@endif
												</select>
												</div><!-- /form-group -->

                                                <div class="form-group">
													<label>Status</label>
													<select class="form-control input-sm category" data-width="100%" id="editstatus" name="status" >
												<option value="Active">Active</option>
                                                <option value="Disabled">Disabled</option>

												</select>
												</div><!-- /form-group -->
												<input type="hidden" id="user_id" name="id">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="edituserbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Edit User</button>
											</form>
							</div>
						</div>
					</div>
        </div>


        	<!-- pop confirm modal -->
	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeleteuser-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/datatables.min.js"></script>
<script src="/assets/js/user.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
