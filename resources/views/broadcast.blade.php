@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Broadcast</li>	 
				</ul>
			</div><!-- /breadcrumb-->

            <div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addBroadcastModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Broadcast Message</button>
						</div>

					</div><!-- /.col -->					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Broadcast Messages
								<span class="label label-info pull-right" id="totalspan">{{count($broadcast)}} Broadcast </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="broadcast_dataTable">
								<thead class="thead-dark">
									<tr>
                                        <th scope="col">No </th>
                                        <th scope="col">Broadcast Name</th>
                                        <th scope="col">Broadcast Content</th>
                                        <th scope="col">Date Created </th>
                                        <th scope="col">Created By</th>
                                        <th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($broadcast as $bcast)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$bcast->broadcast_name}}</td>
										<td>{{$bcast->broadcast_content}}</td>
										<td>{{$bcast->created_at}}</td>
										<!-- <td>{{$bcast->creator->first_name}}</td> -->
										<td>{{ucwords($bcast->creator->first_name)}} {{ucwords($bcast->creator->last_name)}}</td>
										<td><a onclick="editbroadcast({{$bcast->broadcast_id}}, '{{$bcast->broadcast_name}}', '{{$bcast->broadcast_content}}');" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeletebroadcast" data-user="{{ $bcast->broadcast_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">
					
				</div><!-- /.row -->												
			</div><!-- /.padding-md -->			
		</div><!-- /main-container -->

        <div class="modal fade" id="addBroadcastModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Broadcast</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addbroadcast') ?>" id="addbroadcastform">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Broadcast Name</label>
									<input type="text" class="form-control input-sm" name="broadcast_name" placeholder="Broadcast Name">
								</div><!-- /form-group -->
								<div class="form-group">
									<label>Broadcast Content</label>
										<input type="text" class="form-control input-sm" name="broadcast_content" placeholder="Broadcast Content">
								</div><!-- /form-group -->
												
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addbroadcastbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Broadcast</button>
							</form>
						</div>
					</div>
				</div>
            </div>
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/broadcast.js"></script>

@endsection
