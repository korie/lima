@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/home') }}"> Home</a></li>
					 <li class="active">Suppliers</li>	 
				</ul>
			</div><!-- /breadcrumb-->

            <div class="padding-md">
				<div class="row">
					<div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addSupplierModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Supplier</button>
						</div>

					</div><!-- /.col -->					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Suppliers
								<span class="label label-info pull-right" id="totalspan">{{count($suppliers)}} Suppliers </span>
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
                                        <th scope="col">No </th>
                                        <th scope="col">Product Name</th>
                                        <thscope="col">Date Created </th>
                                        <th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($suppliers as $supplier)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$suppliers->supplier_name}}</td>
										<td><a onclick="editSupplier({{$supplies->supplier_id}});" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeletesupplier" data-user="{{ $supplier->supplier_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">
					
				</div><!-- /.row -->												
			</div><!-- /.padding-md -->			
		</div><!-- /main-container -->

        <div class="modal fade" id="addSupplierModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Supplier</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('addsupplier') ?>" id="addsupplierform">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Supplier Name</label>
									<input type="text" class="form-control input-sm" name="supplier_name" placeholder="Supplier Name">
								</div><!-- /form-group -->
												
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								<button type="submit" id="addsupplierbtn" class="btn btn-success btn-sm pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> Add Supplier</button>
							</form>
						</div>
					</div>
				</div>
            </div>
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/supplier.js"></script>
<script type="text/javascript">
</script>

@endsection
