@extends('layouts.master')
@section('styles')

@endsection('styles')
@section('content')
		<div id="main-container">
			<div id="breadcrumb">
				<ul class="breadcrumb">
					 <li><i class="fa fa-home"></i><a href="{{ url('/report') }}"> Orders</a></li>
					 <li class="active">Orders</li>
				</ul>
			</div><!-- /breadcrumb-->

			<div class="padding-md">
				<div class="row">

				   Order ID: {{$order->order_id}} <br>
				   Order From: {{$order->person->first_name}} <br>
				   Order Status:
				   @if($order->status ==  0)
						Pending
					@else
						Delivered
					@endif
					<br>
										
					<!-- <div class="col-sm-6 col-md-12">
						<div class="col-md-12" style="padding-bottom:10px">
							<button type="button" id="modalbtn" class="btn btn-info btn-sm pull-left" data-toggle="modal" data-target="#addUserModal"> <i class="fa fa-plus" aria-hidden="true"></i> Add Order</button>
						</div>

					</div> -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default table-responsive">
							<div class="panel-heading">
								Order Details
							</div>
							<div class="padding-md clearfix">
							<table class="table" id="dataTable">
								<thead class="thead-dark">
									<tr>
									<th scope="col">No.</th>									
									<th scope="col">Product Name</th>
									<th scope="col">Quantity Ordered</th>
									<th scope="col">Quantity Delivered</th>
									
									<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
								@foreach($order_details as $order_detail)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$order_detail->product->product_name}}</td>
										<td>{{$order_detail->quantity_ordered}}</td>
										<td>{{$order_detail->quantity_delivered}}</td>									
										<!-- <td><a onclick="vieworderdetails({{$order_detail->order_detail_id}});" class="btn btn-info btn-xs" style="margin-right:3px;"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
										<td><a onclick="approveorder({{$order_detail->order_details_id}},'{{$order_detail->product->product_name}}',{{$order_detail->quantity_ordered}})" class="btn btn-info btn-xs" style="margin-right:3px;"><i> Approve </i></a>
										<a onclick="disapproveorder({{$order_detail->order_details_id}})" class="btn btn-info btn-xs" style="margin-right:3px;"><i> Disapprove </i></a>

                                        <!-- <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" style="margin-right:3px;" href="#lightCustomModal"  class="btn btn-xs btn-danger lightCustomModal_open btndeleteuser" data-user="{{ $order_detail->order_detail_id }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
										</td>
									</tr>
 								 @endforeach
								</tbody>
							</table>
                            </div>
                            </div>

					</div>
				</div><!-- /row-->
				<div class="row">

				</div><!-- /.row -->
			</div><!-- /.padding-md -->
		</div><!-- /main-container -->

       <!-- Edit user modal -->


        <div class="modal fade" id="orderDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Order Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('updateorder') ?>" id="edituserform">
												{{ csrf_field() }}
												<div class="form-group">
													<label>Product Name</label>
													<input type="text" class="form-control input-sm" id = "product_id" name="product_id" placeholder="First Name">
												</div><!-- /form-group -->
												<div class="form-group">
													<label>Quantity ordered</label>
													<input type="text" class="form-control input-sm" id="quantity_ordered" name="quantity_ordered" placeholder="Last Name">
												</div><!-- /form-group -->

												<div class="form-group">
													<label>Quality Delivered</label>
												<input type="text" class="form-control input-sm" id="quantity_delivered" name="quantity_delivered" >
												</div>
											
												
												
												<input type="hidden" id="order_detail_id" name="order_detail_id">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="edituserbtn" class="btn btn-success btn-sm pull-right"> <i>Approve</i>  </button>
											</form>
						</div>
						</div>
					</div>
        </div>


        	<!-- pop confirm modal -->
			<!-- disapprove comments field -->
			<div class="modal fade" id="disapproveCommentsModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add Comments</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form role="form" county="post" action="<?php echo URL::route('disapproveorder') ?>" id="edituserform">
												{{ csrf_field() }}
											
												<div class="form-group">
													<label>Comments</label>
												<input type="textarea" class="form-control input-sm" id="comments" name="comments" >
												</div>
											
												
												
												<input type="hidden" id="order_detail_id1" name="order_detail_id">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
												<button type="submit" id="edituserbtn1" class="btn btn-success btn-sm pull-right"> <i>Submit</i>  </button>
											</form>
						</div>
						</div>
					</div>
        </div>

	<div class="custom-popup light width-100" id="lightCustomModal">
		<div class="padding-md">
			<h4 class="m-top-none"> Do you want to continue? </h4>
		</div>

		<div class="text-center">
			<a href="#" class="btn btn-success m-right-sm lightCustomModal_close btndeleteuser-confirm">Confirm</a>
			<a href="#" class="btn btn-danger lightCustomModal_close">Cancel</a>
		</div>
	</div>
	<!-- pop confirm modal -->
        	
@endsection
@section('scripts')

<script src="/assets/js/datatables.js"></script>
<script src="/assets/js/user.js"></script>
<script src="/assets/js/order.js"></script>
<script type="text/javascript">
	$('.category').select2();
</script>

@endsection
